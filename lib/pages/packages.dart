import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';

// HTTP
import 'package:http/http.dart' as http;
import 'package:purchases_flutter/purchases_flutter.dart';
import 'dart:async';
import 'dart:convert';

// Globals
import '../utils/globals.dart' as globals;

import '../components/myappbar.dart';
import 'package:shared_preferences/shared_preferences.dart';
//
import 'package:url_launcher/url_launcher.dart';

class PackageInner {
  final String id;
  final String name;
  final String description;
  final double price;
  final String icon;
  final int priority;

  PackageInner(
      {required this.id,
      required this.name,
      required this.description,
      required this.price,
      required this.icon,
      required this.priority});

  factory PackageInner.fromJson(Map<String, dynamic> json) {
    return PackageInner(
      id: json['id'],
      name: json['name'],
      description: json['description'],
      price: json['price'],
      icon: json['icon'],
      priority: json['priority'],
    );
  } //end factory

} //end package class

class Packages extends StatefulWidget {
  @override
  _PackagesState createState() => _PackagesState();
}

class _PackagesState extends State<Packages> {
  //Future<Package> packages;
  List data = List.empty(growable: true);
  bool _visibleLoading = true;
  bool _visibleError = false;
  bool _isLogout = false;
  String _message = "";
  late Offerings _offerings;
  bool _isPro = false;
  GlobalKey<ScaffoldState> _packagesScaffold =
      GlobalKey<ScaffoldState>(debugLabel: '_packagesScaffold');

  late int packageId;
  String content = "";

  @override
  void initState() {
    super.initState();
    getPackages();
    initPlatformState();
    fetchData();
  }

  Future<void> initPlatformState() async {
    await Purchases.setDebugLogsEnabled(true);
    await Purchases.setup("gHuonXfvjUHRgbGvdygGvtsMFSOWMOZP");
  }

  Future<void> fetchData() async {
    late Offerings offerings;
    try {
      print("SELF TUTOR DEBUG: Retriving Offerings");
      offerings = await Purchases.getOfferings();
      //offerings.current.
      //Package package = offerings.current!.availablePackages.first;
      PurchaserInfo purchaserInfo = await Purchases.getPurchaserInfo();
      if (purchaserInfo.entitlements.all["tester"] != null &&
          purchaserInfo.entitlements.all["tester"]!.isActive) {
        debugPrint("SELFTUTOR DEBUG : PRO ACCES GRANTED ALREADY");
        _isPro = true;
      } else {
        debugPrint(
            "SELFTUTOR DEBUG : THIS USER HAS NOT PRO ACCESS FOR SOME REASON");
      }
    } on PlatformException catch (e) {
      print(e);
    }

    if (!mounted) return;

    setState(() {
      _offerings = offerings;
      print("Here are the offerings");
      print(offerings.current.toString());
      print("END of the offerings");
    });
  }

  Future<String> getPackages() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString("apiToken") as String;
    setState(() {
      packageId = preferences.getInt('packageId') as int;
    });
    String url = globals.baseUrl;

    await http.get(Uri.parse(url + "packages"),
        headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        setState(() {
          print("error doing the call");
          _visibleError = true;
          _visibleLoading = false;
          _message = 'Hubo un error con el servicio, intenta más tarde';
        });
        if (statusCode == 401) {
          setState(() {
            _message =
                'Hubo un error con la autorización de tu usuario, \nintena deslogearte y volver a iniciar sesión';
            _visibleError = true;
            _isLogout = true;
            _visibleLoading = false;
          });
        }
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          data = result;

          if (data.length == 0) {
            print("No results, no error");
          } else {
            print("Erease error");
            print("DESCRIPTION");
            print(result[0]['description']);
            content = result[0]['description'];
          }
          _visibleError = false;
          _visibleLoading = false;
          _message = "";
          print(data.length);
          print(result);
        });
      }
    });

    return "success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(title: Text("Paquetes")),
      key: _packagesScaffold,
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(15.0, 25.0, 15.0, 15.0),
                  child: Text('Desde aquí puedes consultar nuestros paquetes'),
                ),
                _offerings != null
                    //"${_offerings.current.sixMonth.product.price} ${_offerings.current.sixMonth.product.currencyCode}")
                    ? makeProductCard(
                        _offerings.current!.sixMonth!.product.title,
                        _offerings.current!.sixMonth!.product.description,
                        _offerings.current!.sixMonth!.product.priceString)
                    : Row(children: [
                        Text('Cargando paquetes desde las tiendas...')
                      ]),
              ],
            ),
            /* _offerings != null
                ? Column(
                    children: [
                      Text("Paquetes desde la tienda"),
                      Text(
                        "Identificador: ${_offerings.current.sixMonth}",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16.0,
                            color: Colors.white),
                      ),
                      Text(
                        "Six Month: ${_offerings.current.sixMonth.identifier}",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16.0,
                            color: Colors.white),
                      ),
                      Text(
                        "offeringIdentifier: ${_offerings.current.sixMonth.offeringIdentifier}",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16.0,
                            color: Colors.white),
                      ),
                      Text(
                        "packageType: ${_offerings.current.sixMonth.packageType}",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16.0,
                            color: Colors.white),
                      ),
                      Text(
                        "Product: ${_offerings.current.sixMonth.product.identifier}",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16.0,
                            color: Colors.white),
                      ),
                      Text(
                        "Description: ${_offerings.current.sixMonth.product.description}",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16.0,
                            color: Colors.white),
                      ),
                      Text(
                        "Price: ${_offerings.current.sixMonth.product.price} - ${_offerings.current.sixMonth.product.currencyCode}",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16.0,
                            color: Colors.white),
                      ),
                      Text(
                        "Title: ${_offerings.current.sixMonth.product.title}",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16.0,
                            color: Colors.white),
                      ),
                      FlatButton(
                          color: Colors.white,
                          onPressed: () async {
                            try {
                              PurchaserInfo purchaserInfo =
                                  await Purchases.purchasePackage(
                                      _offerings.current.sixMonth);

                              if (purchaserInfo
                                  .entitlements.all["tester"].isActive) {
                                print("ST - Purchase Made");
                              } else {
                                print(
                                    "ST - ERROR something went wrong making the purchases");
                              }
                            } on PlatformException catch (e) {
                              var errorCode =
                                  PurchasesErrorHelper.getErrorCode(e);
                              if (errorCode !=
                                  PurchasesErrorCode.purchaseCancelledError) {
                                //showError(e);
                                print("ST - ERROR - ${e.toString()}");
                              }
                            }
                          },
                          child: Text("¡Comprar!"))
                    ],
                  )
                : Row(), */
            //=======================================
            // Error Column
            //=======================================
            _visibleError
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          _message,
                        ),
                      ),
                      _isLogout
                          ? Container(
                              padding: EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: FlatButton(
                                      onPressed: () {
                                        _makeLogout();
                                      },
                                      color: Colors.red,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.0, vertical: 15.0),
                                      child: Text(
                                        "Volver a Iniciar Sesión".toUpperCase(),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 16.0,
                                            color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Offstage(),
                    ],
                  )
                : Row(),
            //=======================================
            // Loading Column
            //=======================================
            _visibleLoading
                ? Column(
                    children: <Widget>[
                      SizedBox(
                        height: 20.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          CircularProgressIndicator(
                            backgroundColor: Colors.white,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              "Cargando...",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  letterSpacing: 0.5),
                            ),
                          ),
                        ],
                      ),
                    ],
                  )
                : Row(),
            //=======================================
            // End Loading Column
            //=======================================
            /* Flexible(
              child: ListView.builder(
                itemCount: data == null ? 0 : data.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    elevation: 3.0,
                    margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                    color: Color.fromRGBO(25, 48, 80, 0.1),
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          //leading: Text("${data[index]['id']}"),
                          title: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              "${data[index]['name']}".toUpperCase(),
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          subtitle: Container(
                            child: Padding(
                              padding: const EdgeInsets.all(0.0),
                              child: Html(
                                data: data[index]['description'],
                              ),
                            ),
                          ),
                          //Text("${data[index]['description']}"),
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                                child: FlatButton(
                                    color: Colors.white,
                                    onPressed: () {},
                                    child: Text("\$${data[index]['price']}"))),
                          ],
                        )
                      ],
                    ),
                  );
                },
              ),
            ), */
            Container(
              padding: EdgeInsets.fromLTRB(5.0, 17.5, 5.0, 5.0),
              decoration: BoxDecoration(
                color: Color.fromRGBO(25, 48, 80, 1),
              ),
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0, bottom: 4.0),
                      child: Text("Para más información consulta:"),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      _launchURL('http://selftutor.mx/terminos-y-condiciones');
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Terminos y Condiciones',
                        style: TextStyle(decoration: TextDecoration.underline),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      _launchURL('http://selftutor.mx/aviso-de-privacidad');
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 5.0, 8.0, 8.0),
                      child: Text(
                        'Aviso de Privacidad',
                        style: TextStyle(decoration: TextDecoration.underline),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
            )
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Card makeProductCard(String productName, String description, String price) {
    return Card(
      elevation: 3.0,
      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
      color: Color.fromRGBO(25, 48, 80, 0.5),
      child: Column(
        children: <Widget>[
          ListTile(
            //leading: Text("${data[index]['id']}"),
            title: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: [
                  Text(
                    productName,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 18.0),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 3.0),
                    child: Text(
                      '$price',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  _isPro || packageId == 1 || packageId == 2
                      ? Container(
                          color: Colors.white,
                          width: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              '[Paquete adquirido]',
                              style: TextStyle(color: Colors.black),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        )
                      : Offstage(),
                ],
              ),
            ),
            subtitle: Container(
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Divider(
                      color: Colors.white54,
                    ),
                    Text("Suscripción por: Seis Meses",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.5,
                        )),
                    Divider(
                      color: Colors.white54,
                    ),
                    Html(
                      data: description,
                    ),
                    Html(
                      data: content,
                    ),
                  ],
                ),
              ),
            ),
            //Text("${data[index]['description']}"),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: FlatButton(
                  color: Colors.white,
                  onPressed: () async {
                    if (_isPro || packageId == 2 || packageId == 1) {
                      showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text('Felicidades'),
                              content: Text('¡Ya eres Chrysós!'),
                              actions: <Widget>[
                                FlatButton(
                                  onPressed: () => Navigator.pop(context),
                                  child: Text('Aceptar'),
                                ),
                              ],
                            ),
                          ) ??
                          false;
                    } else {
                      try {
                        //pack. myPackage = _offerings.current!.sixMonth as Package;
                        PurchaserInfo purchaserInfo =
                            await Purchases.purchasePackage(
                                _offerings.current!.sixMonth as Package);

                        if (purchaserInfo
                            .entitlements.all["Chrysos"]!.isActive) {
                          print("ST - Purchase Made");
                          _postUserUpdate();
                          showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                  title: Text('Felicidades'),
                                  content:
                                      Text('¡Paquete adquirido con éxito!'),
                                  actions: <Widget>[
                                    FlatButton(
                                      onPressed: () => Navigator.pop(context),
                                      child: Text('Aceptar'),
                                    ),
                                  ],
                                ),
                              ) ??
                              false;
                        } else {
                          print(
                              "ST - ERROR something went wrong making the purchases");
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text(
                                  "Hubo un error al adquirir tu paquete")));
                        }
                      } on PlatformException catch (e) {
                        var errorCode = PurchasesErrorHelper.getErrorCode(e);
                        if (errorCode !=
                            PurchasesErrorCode.purchaseCancelledError) {
                          //showError(e);
                          print("ST - ERROR - ${e.toString()}");
                        }
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text("Se canceló la compra")));
                      }
                    }
                  },
                  child: Text(
                    "Suscríbete",
                    style: TextStyle(fontSize: 18.0),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _makeLogout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool cleared = await preferences.clear();
    await Purchases.reset();

    if (cleared) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('splash', (Route<dynamic> route) => false);
    } else {
      print("Algo malo está pasando...");
    }
  }

  Future<bool> _postUserUpdate() async {
    bool send = true;

    SharedPreferences preferences = await SharedPreferences.getInstance();

    int userId = preferences.getInt('userId') as int;
    String token = preferences.getString("apiToken") as String;
    String urlPostUser = globals.baseUrl + "user/changepkg";
    print('URL $urlPostUser');
    print("Making the body request...");
    var body = new Map<String, dynamic>();
    body['user_id'] = userId.toString();
    body['package_id'] = "1";
    print("Body request...");
    print(body);

    if (userId != 1 && send) {
      await http
          .post(Uri.parse(urlPostUser),
              headers: {'api-token': token}, body: body)
          .then((http.Response response) async {
        print("Intento de Postear = ${response.body}");
        final int statusCode = response.statusCode;

        print("Status del Server $statusCode");

        if (statusCode < 200 || statusCode > 400) {
          setState(() {
            debugPrint(
                "Hubo un problema al actualizar el paquete STATUS CODE $statusCode");
            return;
          });
        } else {
          var result = json.decode(response.body);

          debugPrint("Paquete actualizado con éxito STATUS CODE $statusCode");
        }
      });
    }
    return false;
  }
}

import 'package:flutter/material.dart';
import 'package:purchases_flutter/purchases_flutter.dart';
import '../components/myappbar.dart';
// HTTP
import 'package:http/http.dart' as http;
import '../friends/friend_profile.dart';
import '../friends/friend_search.dart';
import '../friends/pending_friends.dart';
import '../pages/pending_battles.dart';
import 'dart:convert';
// Globals
import '../utils/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

class UserFriends extends StatefulWidget {
  @override
  _UserFriendsState createState() => _UserFriendsState();
}

class _UserFriendsState extends State<UserFriends> {
  bool _isLoading = true;
  bool _visibleError = false;
  bool _isLogout = false;
  String _message = "";
  List data = List.empty(growable: true);
  int userId = 0;

  //=======================================
  //
  // GET all the friends of the current user
  //
  //=======================================
  Future<String> _getFriends() async {
    String url = globals.baseUrl;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getInt('userId') as int;
    String token = preferences.getString('apiToken') as String;

    await http.get(Uri.parse(url + "user/$userId/friends"),
        headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        setState(() {
          _visibleError = true;
          _isLoading = false;
          _message = "Hubo un error en el servicio, intenta más tarde";
        });
        if (statusCode == 401) {
          setState(() {
            _message =
                'Hubo un error con la autorización de tu usuario, intena deslogearte y volver a iniciar sesión';
            _visibleError = true;
            _isLoading = false;
            _isLogout = true;
          });
        }
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          data = result;

          data.forEach((element) {
            print("Friend - ${element['id']}");
          });
          List<String> friendsStrings =
              data.map((i) => i['id'].toString()).toList();
          preferences.setStringList('friends', friendsStrings);
          print("ID de amigos actuales desde pantalla amigos");
          print(friendsStrings);

          if (data.length == 0) {
            print("No results, no error");
            setState(() {
              _visibleError = false;
              _message = "Aún no tienes amigos, añade algunos";
            });
          } else {
            print("Erease error");
          }
          _isLoading = false;
          print(data.length);
          print(result);
        });
      }
    });

    return "success";
  }

  @override
  void initState() {
    _getFriends();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(),
      body: _isLoading
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(
                    backgroundColor: Colors.white,
                  ),
                  SizedBox(
                    height: 25.0,
                  ),
                  Text('Estamos cargando...'),
                ],
              ),
            )
          : _visibleError
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        _message,
                        textAlign: TextAlign.center,
                      ),
                      _isLogout
                          ? Container(
                              padding: EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: FlatButton(
                                      onPressed: () {
                                        _makeLogout();
                                      },
                                      color: Colors.red,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.0, vertical: 15.0),
                                      child: Text(
                                        "Volver a Iniciar Sesión".toUpperCase(),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 16.0,
                                            color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Offstage(),
                    ],
                  ),
                )
              : Column(
                  children: <Widget>[
                    //===========================
                    // display the data
                    //===========================
                    Flexible(
                      child: ListView.builder(
                        itemCount: data == null ? 0 : data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Card(
                            elevation: 8.0,
                            margin: EdgeInsets.symmetric(
                                horizontal: 10.0, vertical: 6.0),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Theme.of(context).primaryColor,
                              ),
                              child: ListTile(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 0.0),
                                leading: Container(
                                    padding: EdgeInsets.only(right: 12.0),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            right: BorderSide(
                                                width: 1.0,
                                                color: Colors.white24))),
                                    child: Image(
                                      image: AssetImage(
                                          'images/${data[index]['avatar']}.png'),
                                    )),
                                title: Text(
                                  "${data[index]['username']} | ${data[index]['first_name']}",
                                  style: TextStyle(color: Colors.white),
                                ),
                                subtitle: Row(
                                  children: <Widget>[
                                    Column(
                                      children: [
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(9)),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 3.0,
                                                      horizontal: 12.0),
                                              child: Text(
                                                "${data[index]['global_score']}",
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.w700),
                                              ),
                                            )),
                                      ],
                                    ),
                                  ],
                                ),
                                trailing: Image(
                                  image: AssetImage(
                                      'images/${data[index]['badge']['icon']}'),
                                  width: 35.0,
                                ),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => FriendsProfile(
                                                userId: data[index]['id'],
                                              )));
                                },
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        TextButton(
                          //color: Color.fromRGBO(78, 124, 176, 1),
                          style: TextButton.styleFrom(
                            primary: Colors.white,
                            backgroundColor: Color.fromRGBO(78, 124, 176, 1),
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => PendingBattles()));
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10.0, vertical: 15.0),
                            child: Text(
                              "Retos Pendientes",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        TextButton(
                          //color: Color.fromRGBO(78, 124, 176, 1),
                          style: TextButton.styleFrom(
                            primary: Colors.white,
                            backgroundColor: Color.fromRGBO(78, 124, 176, 1),
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => PendingFriends()));
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10.0, vertical: 15.0),
                            child: Text(
                              "Solicitudes Pendientes",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        TextButton(
                          //color: Color.fromRGBO(78, 124, 176, 1),
                          style: TextButton.styleFrom(
                            primary: Colors.white,
                            backgroundColor: Color.fromRGBO(78, 124, 176, 1),
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FriendSearch()));
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10.0, vertical: 15.0),
                            child: Text(
                              "Buscar / Añadir Amigo",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
      // bottomNavigationBar: userId != 1
      //     ? _visibleError
      //         ? Offstage()
      //         : Column(
      //             mainAxisAlignment: MainAxisAlignment.end,
      //             crossAxisAlignment: CrossAxisAlignment.stretch,
      //             children: [
      //               TextButton(
      //                 //color: Color.fromRGBO(78, 124, 176, 1),
      //                 style: TextButton.styleFrom(
      //                   primary: Colors.white,
      //                   backgroundColor: Color.fromRGBO(78, 124, 176, 1),
      //                 ),
      //                 onPressed: () {
      //                   Navigator.push(
      //                       context,
      //                       MaterialPageRoute(
      //                           builder: (context) => PendingFriends()));
      //                 },
      //                 child: Padding(
      //                   padding: const EdgeInsets.symmetric(
      //                       horizontal: 10.0, vertical: 15.0),
      //                   child: Text(
      //                     "Solicitudes Pendientes",
      //                     textAlign: TextAlign.center,
      //                     style: TextStyle(
      //                         fontSize: 16.0,
      //                         fontWeight: FontWeight.w600,
      //                         color: Colors.white),
      //                   ),
      //                 ),
      //               ),
      //               SizedBox(
      //                 height: 5.0,
      //               ),
      //               TextButton(
      //                 //color: Color.fromRGBO(78, 124, 176, 1),
      //                 style: TextButton.styleFrom(
      //                   primary: Colors.white,
      //                   backgroundColor: Color.fromRGBO(78, 124, 176, 1),
      //                 ),
      //                 onPressed: () {
      //                   Navigator.push(
      //                       context,
      //                       MaterialPageRoute(
      //                           builder: (context) => FriendSearch()));
      //                 },
      //                 child: Padding(
      //                   padding: const EdgeInsets.symmetric(
      //                       horizontal: 10.0, vertical: 15.0),
      //                   child: Text(
      //                     "Buscar / Añadir Amigo",
      //                     textAlign: TextAlign.center,
      //                     style: TextStyle(
      //                         fontSize: 16.0,
      //                         fontWeight: FontWeight.w600,
      //                         color: Colors.white),
      //                   ),
      //                 ),
      //               ),
      //             ],
      //           )
      //     : Offstage(),
    );
  }

  _makeLogout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool cleared = await preferences.clear();
    await Purchases.reset();

    if (cleared) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('splash', (Route<dynamic> route) => false);
    } else {
      print("Algo malo está pasando...");
    }
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import '../helpers/examen_midterm.dart';
import '../utils/globals.dart' as globals;
// HTTP
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class ExamQuestion extends StatefulWidget {
  final int examId;
  final int moduleId;
  final int questionId;
  final bool correct;
  final score;
  final bool start;
  final bool end;
  final int totalQuestions;
  final int totalModules;
  final int doneModules;

  const ExamQuestion(
      {required this.examId,
      required this.moduleId,
      required this.questionId,
      required this.correct,
      required this.score,
      required this.start,
      required this.end,
      required this.totalQuestions,
      required this.totalModules,
      required this.doneModules});

  @override
  _ExamQuestionState createState() => _ExamQuestionState();
}

class _ExamQuestionState extends State<ExamQuestion> {
  List data = List.empty(growable: true);
  late List<dynamic> answers;
  bool _visibleLoading = true;
  bool _visibleError = false;
  String _message = "";
  bool _chosed0 = false;
  bool _chosed1 = false;
  bool _chosed2 = false;
  bool _chosed3 = false;
  bool _end = false;
  bool _correct = false;
  late Timer _timer;
  double _currentPercentage = 0.0;
  late List<bool> chosed;
  ValueNotifier _timeNotifier = ValueNotifier<String>('');
  //ValueNotifier _QuestionNotifier = ValueNotifier<String>('');
  //Html myHtml = My;
  ValueNotifier _htmlNotifier = ValueNotifier<Html>(Html(data: ''));

  /// ================================================
  /// Lets get the List filled with the questions
  /// This one is for each module asking in the module
  /// instructions for its questiosn
  /// ================================================
  Future<String> _getQuestionFromList() async {
    // From the module instructions gets the 0 as first question

    SharedPreferences preferences = await SharedPreferences.getInstance();

    int index = 0;
    List<String> moduleIds =
        preferences.getStringList('modulesIds') as List<String>;
    index = moduleIds.indexOf(widget.moduleId.toString());

    List<String> questionsLists =
        preferences.getStringList('modulesQuestions') as List<String>;

    //List<String> questions = json.decode(questionsLists[index]) as List<String>;
    List questions = json.decode(questionsLists[index]);

    Map<String, dynamic> question = Map<String, dynamic>();
    print("The question from the list string is");
    print(questions[widget.questionId]);

    question = json.decode(questions[widget.questionId]);

    print("Some transformed question is");
    print(question['question']);
    print(question['answers']);
    print(question['answers'].runtimeType);

    _htmlNotifier.value = Html(data: question['question'], style: {
      "*": Style(color: Colors.black, fontSize: FontSize(18)),
      "p": Style(color: Colors.black, fontSize: FontSize(18)),
      "table": Style(
        textOverflow: TextOverflow.clip,
      ),
    });

    answers = question['answers'];
    //answers = json.decode(json.encode(question['answers'][0]));
    chosed = List<bool>.filled(answers.length, false);

    setState(() {
      _visibleLoading = false;
    });

    return "success";
  }

  /// ================================================
  /// Lets get the List filled with the questions
  /// This one is for each module asking in the module
  /// instructions for its questiosn
  /// ================================================
  Future<String> _getQuestion() async {
    // From the module instructions gets the 0 as first question

    SharedPreferences preferences = await SharedPreferences.getInstance();

    List<String> questions =
        preferences.getStringList('examQuestions') as List<String>;

    Map<String, dynamic> question = Map<String, dynamic>();
    print("The question from the list string is");
    print(questions[widget.questionId]);

    question = json.decode(questions[widget.questionId]);

    print("Some transformed question is");
    print(question['question']);
    print(question['answers']);
    print(question['answers'].runtimeType);

    _htmlNotifier.value = Html(data: question['question'], style: {
      "*": Style(color: Colors.black, fontSize: FontSize(18)),
      "p": Style(color: Colors.black, fontSize: FontSize(18)),
      "table": Style(
        textOverflow: TextOverflow.clip,
      ),
    });

    answers = question['answers'];
    //answers = json.decode(json.encode(question['answers'][0]));
    chosed = List<bool>.filled(answers.length, false);

    setState(() {
      _visibleLoading = false;
    });

    return "success";
  }

  //=======================================
  //
  // GET all the Questions of the quiz
  //
  //=======================================
  Future<String> _getExam() async {
    String url = globals.baseUrl;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString('apiToken') as String;

    await http.get(Uri.parse(url + "module/${widget.moduleId}/questions"),
        headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        setState(() {
          print("error doing the call");
          _visibleError = true;
          _visibleLoading = false;
          _message = "Error al conectarse con el servidor";
        });
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          print("SF - result $result code error ${result[0]}");

          if (result[0] != null && result[0] == -1) {
            //print("EL MODULO YA NO EXISTEEEEEEEE");
            //revisar si el siguiente módulo existe, mandarlo allá
            //si no exite mandarlo al home
          }
          data = result;

          //_QuestionNotifier.value = data[widget.questionId]['question'];
          _htmlNotifier.value =
              Html(data: data[widget.questionId]['question'], style: {
            "*": Style(color: Colors.black, fontSize: FontSize(18)),
            "p": Style(color: Colors.black, fontSize: FontSize(18)),
            "table": Style(
              textOverflow: TextOverflow.clip,
            ),
          });

          answers = data[widget.questionId]['answers'];
          print('answers');
          print(data[widget.questionId]['answers']);
          chosed = List<bool>.filled(answers.length, false);

          if (data.length == 0) {
            print("No results, no error");
            _visibleError = true;
            _visibleLoading = false;
            _message = "No hay preguntas para este módulo";
          } else {
            print("Erease error");
          }
          _visibleLoading = false;
          print("SF - Total Questions = ${data.length}");
          //print(result);
        });
        print("SF - Question index on array ${widget.questionId}");
        //print(data[widget.questionId - 1]['question']);
        print("Total De RESPUESTAS = ${answers.length}");
        print("RESPUESTAS :");
        //print(answers);
      }
    });

    return "success";
  }

  void _startTime() {
    const oneSecond = Duration(seconds: 1);
    _timer = Timer.periodic(oneSecond, (timer) {
      _verifyTime();
    });
  }

  _verifyTime() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    var finalDate = DateTime.parse(pref.getString('finalExamDate') as String);

    var currentDate = DateTime.now();

    if (currentDate.isAfter(finalDate)) {
      /*print('Se acabó el tiempo');*/
      _timer.cancel();
      if (_timer.isActive == false) {
        // UNICAMENTE ENVIAR A RESULTADOS DE MÓDULO,
        // ESA PANTALLA SABRÁ DESPUÉS SI ES FINAL DE EXAMEN O NO
        List<String> modulesIds =
            pref.getStringList('modulesIds') as List<String>;
        // the next index of the array is the next module Id we should get
        int nextModuleId;
        if ((widget.doneModules + 1) < modulesIds.length) {
          nextModuleId = int.parse(modulesIds[widget.doneModules + 1]);
        } else {
          nextModuleId = int.parse(modulesIds.last) + 1;
        }

        return Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => ExamMidTerm(
                      examId: widget.examId, // the current exam
                      score: widget.score, // the current score
                      totalQuestions:
                          widget.totalQuestions, // the total of questions
                      doneModules:
                          widget.doneModules, // the current made modules
                      nextModuleId:
                          nextModuleId, // the next module to load if any
                      totalModules:
                          widget.totalModules, // the many modules of the exam
                    )));
      }
    } else {
      /*print('Aún tienes tiempo');*/
      /*Duration dif = finalDate.difference(currentDate);
      //print('Diff te queda(n) ${dif.inMinutes} minuto(s)');
      setState(() {
        //remaining = dif.inMinutes.toString() + ":" + dif.inSeconds.toString();
        _remainingTime =
            dif.toString().substring(0, (dif.toString().length - 7));
      });*/
    }
    Duration dif = finalDate.difference(currentDate);
    _timeNotifier.value =
        dif.toString().substring(0, (dif.toString().length - 7));
  }

  @override
  void initState() {
    _currentPercentage =
        ((widget.questionId + 1) * 100 / widget.totalQuestions) / 100;
    //_getExam();
    //_getQuestion();
    _getQuestionFromList();
    _startTime();
    _setCurrentQuestion();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer.cancel();
    }
    super.dispose();
  }

  _setCurrentQuestion() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setInt('currentQuestion', widget.questionId);
    preferences.setInt('score', widget.score);

    /*List<String> modulesIds = preferences.getStringList('modulesIds');
    int nextModuleId = int.parse(modulesIds[widget.countCurrentModule]);
    preferences.setInt('currentModule', nextModuleId);*/
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return await (showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Container(
                    width: double.infinity,
                    child: Text(
                      '¡Alerta!',
                      style: TextStyle(color: Colors.red),
                    )),
                content: Container(
                  height: 125,
                  child: Column(
                    children: [
                      Text(
                        '¿Realmente quires salir del Módulo?',
                        style: TextStyle(fontSize: 20.0),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                          'Te contará como un intento y no lo podrás recuperar')
                    ],
                  ),
                ),
                actions: [
                  Column(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        child: FlatButton(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 12.5),
                          color: Color.fromRGBO(78, 124, 176, 1),
                          onPressed: () {
                            Navigator.of(context).pop(false);
                            return;
                          },
                          child: Text('Continuar'.toUpperCase(),
                              style: TextStyle(color: Colors.white)),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        child: FlatButton(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 12.5),
                          //color: Color.fromRGBO(78, 124, 176, 1),
                          color: Colors.red,
                          onPressed: () async {
                            SharedPreferences preferences =
                                await SharedPreferences.getInstance();
                            List<String> modulesIds = preferences
                                .getStringList('modulesIds') as List<String>;
                            print(modulesIds);
                            print(widget.doneModules);
                            // the next index of the array is the next module Id we should get
                            int nextModuleId;
                            if ((widget.doneModules + 1) < modulesIds.length) {
                              nextModuleId =
                                  int.parse(modulesIds[widget.doneModules + 1]);
                            } else {
                              nextModuleId = int.parse(modulesIds.last) + 1;
                            }
                            //int nextModuleId = int.parse(modulesIds[widget.doneModules + 1]);
                            preferences.setInt(
                                'arrayPosition', widget.doneModules);

                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ExamMidTerm(
                                          examId: widget.examId,
                                          score: widget.score,
                                          totalQuestions: widget.totalQuestions,
                                          doneModules: widget.doneModules,
                                          nextModuleId: nextModuleId,
                                          totalModules: widget.totalModules,
                                        )));
                            return;
                          },
                          child: Text(
                            'Salir'.toUpperCase(),
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )) ??
            false;
      },
      child: Scaffold(
          body: _visibleLoading
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Cargando el examen..."),
                      SizedBox(
                        height: 25.0,
                      ),
                      CircularProgressIndicator(
                        backgroundColor: Colors.white,
                      )
                    ],
                  ),
                )
              : SafeArea(
                  child: _visibleError
                      ? Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(_message),
                            ],
                          ),
                        )
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            /* LOAD THE QUESTION WHEN IS READY */
                            SizedBox(
                              height: 15.0,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.85,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text("Examen / Módulo ${widget.moduleId} / " +
                                      (widget.questionId + 1).toString()),
                                  Column(
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 4.0),
                                        child: Text("Restante:"),
                                      ),
                                      //Text(_remainingTime)
                                      ValueListenableBuilder(
                                          valueListenable: _timeNotifier,
                                          builder: (_, value, __) =>
                                              Text(value.toString())),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.85,
                              child: SizedBox(
                                height: 15.0,
                                child: LinearProgressIndicator(
                                  backgroundColor: Colors.white12,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Color.fromRGBO(78, 124, 176, 1)),
                                  value: _currentPercentage,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Flexible(
                              child: ListView.builder(
                                itemCount: data == null ? 0 : 1,
                                itemBuilder: (BuildContext context, int index) {
                                  return Container(
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          decoration: BoxDecoration(
                                              //color: Colors.amber,
                                              borderRadius:
                                                  BorderRadius.circular(15)),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.90,
                                          child: Material(
                                            elevation: 2.0,
                                            //color: Colors.greenAccent,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(15)),
                                            child: Column(
                                              children: <Widget>[
                                                SizedBox(
                                                  height: 10.0,
                                                ),
                                                Container(
                                                  alignment: Alignment.center,
                                                  padding: EdgeInsets.all(10.0),
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.95,
                                                  decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15),
                                                    /*border: Border(
                                          bottom: BorderSide(width: 1.0, color: Colors.grey[200])
                                        )*/
                                                  ),
                                                  //child: Text("$data[0][question]", style: TextStyle(color: Colors.black),)
                                                  // child: Html(
                                                  //   data:
                                                  //       _QuestionNotifier.value,
                                                  //   style: {
                                                  //     "*": Style(
                                                  //         color: Colors.black,
                                                  //         fontSize:
                                                  //             FontSize(18)),
                                                  //     "p": Style(
                                                  //         color: Colors.black,
                                                  //         fontSize:
                                                  //             FontSize(18)),
                                                  //     "table": Style(
                                                  //       textOverflow:
                                                  //           TextOverflow.clip,
                                                  //     ),
                                                  //   },
                                                  // ),
                                                  child: _htmlNotifier.value,
                                                ),
                                                Divider(
                                                  color: Colors.white,
                                                  thickness: 2.5,
                                                ),
                                                SizedBox(height: 5.0),
                                              ],
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 10.0),
                                        answers != null
                                            ? Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.9,
                                                child: _buildQuestions(
                                                    context, answers))
                                            : Row(),
                                        Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.95,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Expanded(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: (widget
                                                              .totalQuestions ==
                                                          (widget.questionId +
                                                              1))
                                                      ? RaisedButton(
                                                          color: Color.fromRGBO(
                                                              78, 124, 176, 1),
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  vertical:
                                                                      15.0,
                                                                  horizontal:
                                                                      10.0),
                                                          child: Text(
                                                            "Finalizar",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 16,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500),
                                                          ),
                                                          onPressed: () {
                                                            nextQuestion();
                                                          },
                                                        )
                                                      : RaisedButton(
                                                          //color: Color.fromRGBO(25, 48, 80, 0.7),
                                                          color: Color.fromRGBO(
                                                              78, 124, 176, 1),
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  vertical:
                                                                      15.0,
                                                                  horizontal:
                                                                      10.0),
                                                          child: Text(
                                                            "Siguiente",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 16,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500),
                                                          ),
                                                          onPressed: () {
                                                            nextQuestion();
                                                          },
                                                        ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                )),
    );
  }

  Widget _buildQuestions(BuildContext context, List strings) {
    List<Widget> list = List<Widget>.empty(growable: true);

    for (var i = 0; i < strings.length; i++) {
      list.add(
        Row(
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 5),
                child: Material(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)),
                  color: chosed[i] ? Colors.blue[100] : Colors.white,
                  child: InkWell(
                    onTap: () {
                      markQuestion2(i);
                      int isCorrect = -1;
                      strings[i]['is_correct'] is int
                          ? isCorrect = strings[i]['is_correct']
                          : isCorrect = int.parse(strings[i]['is_correct']);
                      //changeQuestion( int.parse(strings[i]['is_correct']));
                      changeQuestion(isCorrect);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 17.5, horizontal: 12.0),
                      child: Text(
                        "${strings[i]['value']}",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14.5,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
    return Column(children: list);
  }

  markQuestion2(int index) {
    setState(() {
      chosed = List<bool>.filled(answers.length, false);
      chosed[index] = true;
    });
    print(chosed);
  }

  nextQuestion() async {
    // add to the score
    int score = widget.score;
    print("Data Length: ${data.length}");
    print("Question ID - Position on array: ${widget.questionId}");

    if (widget.totalQuestions == (widget.questionId + 1)) {
      _end = true;
    }
    if (_correct) {
      score = score + 1;
    }

    //make sure to cancell the timer before changin questions
    if (_timer != null && _timer.isActive) {
      _timer.cancel();
    }

    if (!_end) {
      return Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => ExamQuestion(
                    examId: widget.examId,
                    correct: _correct,
                    end: _end, // still not the end
                    score: score,
                    questionId:
                        widget.questionId + 1, // lets get the next question
                    start: false,
                    totalQuestions: widget.totalQuestions,
                    doneModules: widget
                        .doneModules, // still the same amount of done modules
                    moduleId: widget
                        .moduleId, // Not the end so lets keep calling the same module
                    totalModules: widget.totalModules, // this one never changes
                  )));
    } else {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      List<String> modulesIds =
          preferences.getStringList('modulesIds') as List<String>;
      print('Modules IDS - Array Positions');
      print(modulesIds);
      print("Modules IDS LENGTH ${modulesIds.length}");
      print(widget.doneModules);
      print("Done Modules so far ${widget.doneModules}");
      // the next index of the array is the next module Id we should get
      // int nextModuleId;
      // if (modulesIds.length == (widget.doneModules + 1)) {
      //   nextModuleId = modulesIds.length;
      // } else {
      //   nextModuleId = int.parse(modulesIds[widget.doneModules + 1]);
      // }
      int nextModuleId;
      if ((widget.doneModules + 1) < modulesIds.length) {
        nextModuleId = int.parse(modulesIds[widget.doneModules + 1]);
      } else {
        nextModuleId = int.parse(modulesIds.last) + 1;
      }
      preferences.setInt('arrayPosition', widget.doneModules);

      return Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ExamMidTerm(
                    examId: widget.examId,
                    score: score,
                    totalQuestions: widget.totalQuestions,
                    doneModules:
                        widget.doneModules, // send the next desiareble index
                    //nextModuleId: widget.moduleId + 1,
                    nextModuleId: nextModuleId,
                    totalModules: widget.totalModules,
                  )));
    }
  }

  changeQuestion(int isCorrect) {
    // here will go with the flow and
    // add to the score
    int score = widget.score;
    print("${data.length} == " + (widget.questionId + 1).toString());
    if (data.length == (widget.questionId + 1)) {
      _end = true;
    }
    if (isCorrect == 1) {
      score = score + 1;
      _correct = true;
    } else {
      _correct = false;
    }

    print("---------");
    print('Question number = ${widget.questionId}');
    print('Is_correct = $isCorrect');
    print('Top Correct = $_correct');
    print('End = $_end');
    print('Score = $score');
    print("---------");
  }

  markQuestion(int index) {
    setState(() {
      switch (index) {
        case 0:
          if (_chosed0 == false) {
            _chosed0 = !_chosed0;
            _chosed1 = false;
            _chosed2 = false;
            _chosed3 = false;
          }
          break;
        case 1:
          if (_chosed1 == false) {
            _chosed0 = false;
            _chosed1 = !_chosed1;
            _chosed2 = false;
            _chosed3 = false;
          }
          break;
        case 2:
          if (_chosed2 == false) {
            _chosed0 = false;
            _chosed1 = false;
            _chosed2 = !_chosed2;
            _chosed3 = false;
          }
          break;
        case 3:
          if (_chosed3 == false) {
            _chosed0 = false;
            _chosed1 = false;
            _chosed2 = false;
            _chosed3 = !_chosed3;
          }
          break;
        default:
      }
    });
  }
}

import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import '../components/myappbar.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
// Globals
import '../utils/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

class LogQuizzes extends StatefulWidget {
  @override
  _LogQuizzesState createState() => _LogQuizzesState();
}

class _LogQuizzesState extends State<LogQuizzes> {
  bool _isLoading = false;
  bool _error = false;
  String _message = "";
  String _url = "";

  List data = List.empty(growable: true);

  // Get the books function
  Future<String> _getLogQuizzes() async {
    setState(() {
      _isLoading = true;
      _error = false;
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString('apiToken') as String;
    int userId = preferences.getInt('userId') as int;

    _url = globals.baseUrl + "user/$userId/quizzes";
    print(_url);

    http.get(Uri.parse(_url), headers: {'api-token': token}).then(
        (http.Response response) async {
      final int statusCode = response.statusCode;
      print("Status del Server $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        setState(() {
          _isLoading = false;
          _error = true;
          _message = "Hubo un error al procesar la búsqueda";
        });
        throw new Exception("Error while fetching data");
      } else {
        // The server was reached and responded
        setState(() {
          _isLoading = false;
          data = json.decode(response.body);
        });
        print(response.body);
      }
    });

    return "success";
  }

  @override
  void initState() {
    initializeDateFormatting('es_MX', null);
    _getLogQuizzes();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(
        title: Text("Resultados"),
      ),
      body: Center(
        child: Column(children: [
          _error
              ? Row(
                  children: [Text(_message)],
                )
              : Row(),
          _isLoading
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 25.0,
                        ),
                        CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        Text("Cargando resultados..."),
                      ],
                    )
                  ],
                )
              : Flexible(
                  child: ListView.builder(
                      itemCount: data == null ? 0 : data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                          elevation: 8.0,
                          margin: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 6.0),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                            ),
                            child: ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 0.0),
                              /*leading: Container(
                          padding: EdgeInsets.only(right: 12.0),
                          decoration: BoxDecoration(
                            border: Border(
                              right: BorderSide(
                                width: 1.0, color: Colors.white24
                              )
                            )
                          ),
                          child: Icon(
                            Icons.star
                          )
                        ),*/
                              title: Text(
                                "Puntaje : ${data[index]['pivot']['score']}",
                                style: TextStyle(color: Colors.white),
                              ),
                              subtitle: Row(
                                children: <Widget>[
                                  Column(
                                    children: [
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(9)),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 3.0,
                                                horizontal: 12.0),
                                            child: Text(
                                              "" +
                                                  DateFormat.yMMMMd('es_MX')
                                                      .format(DateTime.parse(
                                                          data[index]['pivot']
                                                              ['created_at'])),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w700),
                                            ),
                                          )),
                                    ],
                                  ),
                                ],
                              ),
                              trailing: Icon(Icons.star),
                              onTap: () {
                                //
                              },
                            ),
                          ),
                        );
                      }))
        ]),
      ),
    );
  }
}

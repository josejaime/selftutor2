import 'package:flutter/material.dart';
import 'package:purchases_flutter/purchases_flutter.dart';
import '../components/myappbar.dart';
import 'package:shared_preferences/shared_preferences.dart';
// Globals
import '../utils/globals.dart' as globals;
// HTTP
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class UsersAchievements extends StatefulWidget {
  @override
  _UsersAchievementsState createState() => _UsersAchievementsState();
}

class _UsersAchievementsState extends State<UsersAchievements> {
  bool _isLoading = true;
  bool _visibleError = false;
  bool _isLogout = false;
  String _message = "";
  List data = List.empty(growable: true);

  Future<String> _getAchievements() async {
    String url = globals.baseUrl;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int userId = preferences.getInt('userId') as int;
    String token = preferences.getString('apiToken') as String;

    await http.get(Uri.parse(url + "user/$userId/achievements"),
        headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        setState(() {
          _visibleError = true;
          _isLoading = false;
          _message = "Hubo un error en el servicio, intenta más tarde";
        });
        if (statusCode == 401) {
          setState(() {
            _message =
                'Hubo un error con la autorización de tu usuario, intena deslogearte y volver a iniciar sesión';
            _visibleError = true;
            _isLoading = false;
            _isLogout = true;
          });
        }
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          data = result;

          if (data.length == 0) {
            print("No results, no error");
            setState(() {
              _visibleError = true;
              _message =
                  "Aún no tienes logros, sigue prácticando para obtenerlos";
            });
          } else {
            print("Erease error");
          }
          _isLoading = false;
          print(data.length);
          print(result);
        });
      }
    });

    return "success";
  }

  @override
  void initState() {
    _getAchievements();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(
        title: Text('Tus Logros'),
      ),
      body: _isLoading
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(
                    backgroundColor: Colors.white,
                  ),
                  SizedBox(
                    height: 25.0,
                  ),
                  Text('Estamos cargando...'),
                ],
              ),
            )
          : _visibleError
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        _message,
                        textAlign: TextAlign.center,
                      ),
                      _isLogout
                          ? Container(
                              padding: EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: FlatButton(
                                      onPressed: () {
                                        _makeLogout();
                                      },
                                      color: Colors.red,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.0, vertical: 15.0),
                                      child: Text(
                                        "Volver a Iniciar Sesión".toUpperCase(),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 16.0,
                                            color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Offstage(),
                    ],
                  ),
                )
              : ListView.builder(
                  itemCount: data == null ? 0 : data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                      elevation: 8.0,
                      margin:
                          EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                      child: Container(
                        decoration: BoxDecoration(
                          //color: Theme.of(context).primaryColor,
                          color: Color.fromRGBO(56, 90, 123, 1),
                        ),
                        child: ListTile(
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 5.0),
                          leading: Container(
                            padding: EdgeInsets.only(right: 12.0),
                            decoration: BoxDecoration(
                                border: Border(
                                    right: BorderSide(
                                        width: 1.0, color: Colors.white24))),
                            child: Icon(Icons.star, color: Colors.white),
                          ),
                          title: Text(
                            data[index]['name'],
                            style: TextStyle(color: Colors.white),
                          ),
                          subtitle: Row(
                            children: <Widget>[
                              Expanded(
                                flex: 4,
                                child: Padding(
                                    padding: EdgeInsets.only(left: 0.0),
                                    child: Text(data[index]['description'],
                                        style:
                                            TextStyle(color: Colors.white54))),
                              ),
                            ],
                          ),
                          onTap: () {},
                        ),
                      ),
                    );
                  },
                  /*children: <Widget>[
          Card(
            elevation: 8.0,
            margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            child: Container(
              decoration: BoxDecoration(
                //color: Theme.of(context).primaryColor,
                color: Color.fromRGBO(56, 90, 123, 1),
              ),
              child: ListTile(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                leading: Container(
                  padding: EdgeInsets.only(right: 12.0),
                  decoration: BoxDecoration(
                      border: Border(
                        right: BorderSide(
                          width: 1.0, color: Colors.white24))),
                  child: Icon(Icons.star, color: Colors.white),
                ),
                title: Text(
                  "Primer 10",
                  style: TextStyle(color: Colors.white),
                ),
                subtitle: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 4,
                      child: Padding(
                        padding: EdgeInsets.only(left: 0.0),
                        child: Text('Obten una puntuación perfecta en un quiz',
                            style: TextStyle(color: Colors.white54))),
                    ),
                  ],
                ),
                onTap: () {},
              ),
            ),
          ),
          Card(
            elevation: 4.0,
            margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            child: Container(
              decoration: BoxDecoration(
                //color: Theme.of(context).primaryColor,
                color: Color.fromRGBO(56, 90, 123, 0.6),
              ),
              child: ListTile(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                leading: Container(
                  padding: EdgeInsets.only(right: 12.0),
                  decoration: BoxDecoration(
                      border: Border(
                        right: BorderSide(
                            width: 1.0, color: Colors.white24))),
                  child: Icon(Icons.star_border, color: Colors.white24),
                ),
                title: Text(
                  "1000 Quizes",
                  style: TextStyle(color: Colors.white30),
                ),
                subtitle: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 4,
                      child: Padding(
                        padding: EdgeInsets.only(left: 0.0),
                        child: Text('Contesta 100 quizes de culaquier lección',
                            style: TextStyle(color: Colors.white30))),
                    ),
                  ],
                ),
                onTap: () {},
              ),
            ),
          ),
        ],*/
                ),
    );
  }

  _makeLogout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool cleared = await preferences.clear();
    await Purchases.reset();

    if (cleared) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('splash', (Route<dynamic> route) => false);
    } else {
      print("Algo malo está pasando...");
    }
  }
}

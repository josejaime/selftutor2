import 'package:flutter/material.dart';

class Soon extends StatefulWidget {
  @override
  _SoonState createState() => _SoonState();
}

class _SoonState extends State<Soon> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 16.0),
              child: Image(
                width: 125.0,
                image: AssetImage('images/logo.png')
              ),
            ),
            Padding(
              padding: const EdgeInsets.only( bottom: 4.0),
              child: Text("¡Estamos en desarrollo!", style: TextStyle(fontSize: 15.0, color: Colors.white),),
            ),
            Text("Te esperamos pronto", style: TextStyle(fontSize: 15.0, color: Colors.white),),
          ],
        ),
      ),
    );
  }
}
import 'package:flutter/material.dart';

// HTTP
import 'package:http/http.dart' as http;
import '../components/myappbar.dart';
import 'dart:async';
import 'dart:convert';

// Globals
import '../utils/globals.dart' as globals;

import '../pages/content.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Lesson extends StatefulWidget {
  // Constructor
  final id;
  final String? name;
  final String? portrait; // The general subject photo

  Lesson({
    this.id,
    this.name,
    this.portrait,
  });

  @override
  _LessonState createState() => _LessonState();
}

class _LessonState extends State<Lesson> {
  List data = List.empty(growable: true);
  bool _visibleLoading = true;
  bool _visibleError = false;
  String _errorMessage = "";

  @override
  void initState() {
    super.initState();
    _getLessons();
  }

  //=======================================
  //
  // GET all the lessson for the subject
  //
  //=======================================
  Future<String> _getLessons() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString("apiToken") as String;
    String url = globals.baseUrl;

    await http.get(Uri.parse(url + "subject/${widget.id}/lessons"),
        headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        setState(() {
          _visibleError = true;
          _visibleLoading = false;
          _errorMessage = "Hubo un error en el servicio, intenta más tarde";
        });
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          print(result);
          data = result;

          if (data.length == 0) {
            print("No results, no error");
            setState(() {
              _visibleError = true;
              _errorMessage = "No hay lecciones en esta sección por el momento";
            });
          } else {
            print("Erease error");
          }
          _visibleLoading = false;
          print(data.length);
          print(result);
        });
      }
    });

    return "success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(title: Text('')),
      body: Center(
        child: Column(
          children: <Widget>[
            Material(
              elevation: 8.0,
              color: Colors.transparent,
              child: Stack(
                children: <Widget>[
                  /*Container(
                    padding: EdgeInsets.only(left: 10.0),
                    height: MediaQuery.of(context).size.height * 0.25,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("images/${widget.portrait}",),
                        fit: BoxFit.contain,
                      ),
                    )
                  ),*/
                  Container(
                    height: MediaQuery.of(context).size.height * 0.25,
                    padding: EdgeInsets.all(40.0),
                    width: MediaQuery.of(context).size.width,
                    decoration:
                        BoxDecoration(color: Color.fromRGBO(25, 48, 80, 0.85)),
                    child: Center(
                        //child: Text('${widget.name}', style: Theme.of(context).textTheme.headline3,),
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 0.0),
                        Icon(
                          Icons.check,
                          color: Colors.white,
                        ),
                        Container(
                          width: 90.0,
                          child: Divider(
                            color: Colors.white,
                            height: 10.0,
                          ),
                        ),
                        //SizedBox(height: 00.0),
                        Text(
                          '${widget.name}',
                          style: TextStyle(color: Colors.white, fontSize: 40.5),
                        ),
                        //SizedBox(height: 30.0),
                      ],
                    )),
                  ),
                  /*Positioned(
                    left: 8.0,
                    top: 20.0,
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(Icons.arrow_back, color: Colors.white),
                    ),
                  )*/
                ],
              ),
            ),
            /*Material(
              elevation: 0.0,
              child: Container(
                color: Color.fromRGBO(64, 80, 97, 1),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    //crossAxisAlignment: CrossAxisAlignment.center,

                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8.0, 15.0, 8.0,0),
                            child: Container(
                              height: 85,
                              width: 85,
                              decoration: BoxDecoration(
                                //color: const Color(0xff7c94b6),
                                image: DecorationImage(
                                  image: ExactAssetImage('images/${widget.portrait}'),
                                  fit: BoxFit.contain,
                                ),
                              ),

                              //child: Text("Aquí debe haber algo"),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),*/
            /*Material(
              elevation: 1.0,
              child: Container(
                color: Color.fromRGBO(64, 80, 97, 1),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Text("${widget.name}", style: TextStyle( color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18.0, letterSpacing: 0.5),),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),*/
            //=======================================
            // Error Column
            //=======================================
            _visibleError
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(_errorMessage),
                      )
                    ],
                  )
                : Offstage(),
            //=======================================
            // Loading Column
            //=======================================
            _visibleLoading
                ? Column(
                    children: <Widget>[
                      SizedBox(
                        height: 25.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.white,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "Cargando lecciones...",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    letterSpacing: 0.5),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 25.0,
                      ),
                    ],
                  )
                : Row(),
            //=======================================
            // End Loading Column
            //=======================================
            /*Padding(
              padding: const EdgeInsets.fromLTRB(8.0,8.0,8.0,18.0),
              child: Column(
                children: <Widget>[
                  Text("${widget.name}", style: TextStyle( fontWeight: FontWeight.w700, fontSize: 18.0),),
                ],
              ),
            ),*/
            //===========================
            // display the data
            //===========================
            Flexible(
              child: ListView.builder(
                itemCount: data == null ? 0 : data.length,
                itemBuilder: (BuildContext context, int index) {
                  double difficulty = 0.0;
                  data[index]['difficulty'] is double
                      ? difficulty = data[index]['difficulty']
                      : difficulty =
                          double.parse(data[index]['difficulty'].toString());

                  return Card(
                    elevation: 8.0,
                    margin:
                        EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor.withOpacity(0.99),
                        //color: Colors.white70
                      ),
                      child: ListTile(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 0.0),
                        title: Text(
                          data[index]['name'],
                          style: TextStyle(color: Colors.white),
                        ),
                        subtitle: Row(
                          children: <Widget>[
                            Expanded(
                                flex: 1,
                                child: Container(
                                  //tag: 'hero',
                                  child: LinearProgressIndicator(
                                      backgroundColor:
                                          Color.fromRGBO(209, 224, 224, 0.2),
                                      value: difficulty,
                                      valueColor:
                                          AlwaysStoppedAnimation(Colors.white)),
                                  height: 2.0,
                                )),
                            Expanded(
                              flex: 3,
                              child: Padding(
                                  padding: EdgeInsets.only(left: 10.0),
                                  child: Text('',
                                      style: TextStyle(color: Colors.white54))),
                            ),
                          ],
                        ),
                        trailing: Icon(Icons.keyboard_arrow_right,
                            color: Colors.white, size: 30.0),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Content(
                                      id: data[index]['id'],
                                      name: data[index]['name'],
                                      content: data[index]['content'],
                                      subjectId: widget.id,
                                      subjectPortrait:
                                          widget.portrait as String,
                                      subjectName: widget.name as String)));
                        },
                      ),
                    ),
                  );
                  /*Container(
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(color: Colors.blueGrey, width: 1.0)
                      )
                    ),
                    child: ListTile(
                      onTap: () {
                        print('clicked = ${data[index]['id']}');
                        Navigator.push(context, MaterialPageRoute( builder: (context) => Content(
                          id: data[index]['id'],
                          name: data[index]['name'],
                          content: data[index]['content'],
                        )));
                      },
                      title: Text("${data[index]['name']}"),
                    ),
                  );*/
                },
              ),
            ),
            /*Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  FlatButton(
                    onPressed: (){},
                    color: Colors.blueGrey,
                    child: Text("Empezar", style: TextStyle( color: Colors.white, letterSpacing: 1.0, fontWeight: FontWeight.w800),),
                  )  
                ],
              ),
            ),*/
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:purchases_flutter/purchases_flutter.dart';
import '../components/myappbar.dart';

// HTTP
import 'package:http/http.dart' as http;
import '../pages/user_update.dart';
import 'dart:async';
import 'dart:convert';

// Globals
import '../utils/globals.dart' as globals;

import '../pages/packages.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserProfile extends StatefulWidget {
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  var profile;
  String profileImage = "";
  int userId = 0;
  GlobalKey<ScaffoldState> _scaffoldKey =
      GlobalKey<ScaffoldState>(debugLabel: "HOLS");
  bool _visibleLoading = true;
  bool _visibleError = false;
  bool _isLogout = false;
  String _message = "";

  @override
  initState() {
    super.initState();
    _getProfile();
  }

  /*========================================================*/
  /* Lets call the profile from the server */
  /* Now just call the ID = 1              */
  /*========================================================*/
  Future<String> _getProfile() async {
    //===============================================
    // Lets get the user in shared preferences
    //===============================================
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userId = (prefs.getInt('userId') ?? 1);
    String token = prefs.getString('apiToken') as String;
    print("The user id is = $userId");

    String url = globals.baseUrl;

    await http.get(Uri.parse(url + "user/$userId"),
        headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        setState(() {
          _message = 'Hubo un error conectando con el servidor';
          _visibleError = true;
          _visibleLoading = false;
        });

        if (statusCode == 401) {
          setState(() {
            _message =
                'Hubo un error con la autorización de tu usuario, intena deslogearte y volver a iniciar sesión';
            _visibleError = true;
            _visibleLoading = false;
            _isLogout = true;
          });
        }

        throw new Exception("Error while fetching data");
      } else {
        String avatar = prefs.getString('avatar') as String;
        profileImage = avatar + ".png";
        setState(() {
          var result = json.decode(response.body);
          profile = result;

          /*if (profile.length == 0) {
            print("No results, no error");
          }else {
            print("Erease error");
          }*/
          //print(response.body);
          //print(profile.length);
          print(profile);
        });
      }
    });

    return "success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: MyCustomAppBar(
        title: Text('Tu cuenta'),
        actions: [
          IconButton(
            onPressed: () {
              if (userId != null && userId != 1) {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => UpdateUser()));
              } else {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text("Debes crear una cuenta primero"),
                  ),
                );
              }
            },
            icon: Icon(Icons.settings),
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Material(
                elevation: 3.0,
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(56, 90, 123, 1),
                      border: Border(
                          bottom: BorderSide(
                        color: Colors.white,
                      ))),
                  child: profile != null
                      ? Row(
                          children: <Widget>[
                            Expanded(
                                flex: 1,
                                child: Center(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(50.0),
                                    child: Image(
                                      image:
                                          AssetImage('images/' + profileImage),
                                      width: 120.0,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                )),
                            Expanded(
                                child: Material(
                              color: Colors.white,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    profile["first_name"] +
                                        ' ' +
                                        profile["last_name"],
                                    style: TextStyle(
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black),
                                    textAlign: TextAlign.center,
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  Container(
                                      padding: EdgeInsets.all(8.0),
                                      decoration: BoxDecoration(
                                          color: Color.fromRGBO(25, 48, 80, 1),
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Padding(
                                        padding: const EdgeInsets.all(6.5),
                                        child: Text(
                                          'Score = ${profile['global_score']}',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      )),
                                ],
                              ),
                            )),
                          ],
                        )
                      : _visibleError
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  _message,
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            )
                          : Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                CircularProgressIndicator(
                                  backgroundColor: Colors.white,
                                ),
                              ],
                            ),
                ),
              ),
            ],
          ),
          profile != null
              ? Flexible(
                  child: ListView(
                    children: <Widget>[
                      profile != null
                          ? Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      8.0, 18.0, 8.0, 8.0),
                                  child: Text("Información personal"),
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Card(
                                            elevation: 1.0,
                                            child: Column(
                                              children: <Widget>[
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 8.0,
                                                      vertical: 15.0),
                                                  child: Column(
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.person,
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColor,
                                                            size: 30.0,
                                                          ),
                                                          Expanded(
                                                              child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 10.0),
                                                            child: Text(
                                                              profile[
                                                                  "username"],
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .bodyText1,
                                                            ),
                                                          ))
                                                        ],
                                                      ),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.mail,
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColor,
                                                            size: 30.0,
                                                          ),
                                                          Expanded(
                                                              child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 10.0),
                                                            child: Text(
                                                              profile['email'],
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .bodyText1,
                                                            ),
                                                          ))
                                                        ],
                                                      ),
                                                      profile['prepa_name'] !=
                                                              null
                                                          ? Row(
                                                              children: <
                                                                  Widget>[
                                                                Icon(
                                                                  Icons.school,
                                                                  color: Theme.of(
                                                                          context)
                                                                      .primaryColor,
                                                                  size: 30.0,
                                                                ),
                                                                Expanded(
                                                                    child:
                                                                        Padding(
                                                                  padding: const EdgeInsets
                                                                          .only(
                                                                      left:
                                                                          10.0),
                                                                  child: Text(
                                                                    profile[
                                                                        'prepa_name'],
                                                                    style: Theme.of(
                                                                            context)
                                                                        .textTheme
                                                                        .bodyText1,
                                                                  ),
                                                                ))
                                                              ],
                                                            )
                                                          : Row(),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.score,
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColor,
                                                            size: 30.0,
                                                          ),
                                                          Expanded(
                                                              child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 10.0),
                                                            child: Text(
                                                              "Promedio: ${profile['prepa_score']}",
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .bodyText1,
                                                            ),
                                                          ))
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            )),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text("Carrera"),
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Packages()));
                                          },
                                          child: Card(
                                              elevation: 1.0,
                                              child: Column(
                                                children: <Widget>[
                                                  Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 8.0,
                                                        vertical: 15.0),
                                                    child: Column(
                                                      children: <Widget>[
                                                        Row(
                                                          children: <Widget>[
                                                            Icon(
                                                              Icons.school,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColor,
                                                              //color: Colors.white,
                                                              size: 30.0,
                                                            ),
                                                            Expanded(
                                                                child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left:
                                                                          10.0),
                                                              child: Text(
                                                                '${profile['career']['name']}',
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .bodyText1,
                                                              ),
                                                            ))
                                                          ],
                                                        ),
                                                        Row(
                                                          children: <Widget>[
                                                            Icon(
                                                              Icons
                                                                  .radio_button_checked,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColor,
                                                              //color: Colors.white,
                                                              size: 30.0,
                                                            ),
                                                            Expanded(
                                                                child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left:
                                                                          10.0),
                                                              child: Text(
                                                                'Puntaje requerido: ${profile['career']['admission']}',
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .bodyText1,
                                                              ),
                                                            ))
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              )),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text("Paquete"),
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Packages()));
                                          },
                                          child: Card(
                                              elevation: 1.0,
                                              child: Column(
                                                children: <Widget>[
                                                  Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 8.0,
                                                        vertical: 15.0),
                                                    child: Column(
                                                      children: <Widget>[
                                                        /*Row(
                                            children: <Widget>[
                                              Icon(
                                                Icons.payment, color: Theme.of(context).primaryColor,
                                                size: 30.0,
                                              ),
                                              Expanded(
                                                child: Padding(
                                                  padding: const EdgeInsets.only(left: 10.0),
                                                  child: Text('Método de Pago (pendiente)', style: Theme.of(context).textTheme.bodyText1,),
                                                )
                                              )
                                            ],
                                          ),*/
                                                        Row(
                                                          children: <Widget>[
                                                            Icon(
                                                              Icons
                                                                  .check_box_outline_blank,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColor,
                                                              size: 30.0,
                                                            ),
                                                            Expanded(
                                                                child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left:
                                                                          10.0),
                                                              child: Text(
                                                                '${profile['package']['name']}',
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .bodyText1,
                                                              ),
                                                            ))
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              )),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text("Rango"),
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Card(
                                            elevation: 1.0,
                                            child: Column(
                                              children: <Widget>[
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 8.0,
                                                      vertical: 15.0),
                                                  child: Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                          flex: 1,
                                                          child: Center(
                                                            child: ClipRRect(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          0.0),
                                                              child: Image(
                                                                image: AssetImage(
                                                                    'images/' +
                                                                        profile['badge']
                                                                            [
                                                                            'icon']),
                                                                width: 105.0,
                                                                fit:
                                                                    BoxFit.fill,
                                                              ),
                                                            ),
                                                          )),
                                                      Expanded(
                                                          flex: 2,
                                                          child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 8.0),
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              children: <
                                                                  Widget>[
                                                                Text(
                                                                  profile['badge']
                                                                      ['name'],
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          17.0,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600,
                                                                      color: Colors
                                                                          .black),
                                                                ),
                                                                Text(
                                                                  profile['badge']
                                                                      [
                                                                      'description'],
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          12.5,
                                                                      color: Colors
                                                                          .black),
                                                                )
                                                              ],
                                                            ),
                                                          )),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            )),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            )
                          : Center(
                              child: Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 25.0,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CircularProgressIndicator(
                                    backgroundColor: Colors.white,
                                  ),
                                ),
                                Text('Cargando...'),
                                SizedBox(
                                  height: 25.0,
                                ),
                              ],
                            )),
                    ],
                  ),
                )
              : _visibleError
                  ? Column(
                      children: [
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          _message,
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        _isLogout
                            ? Container(
                                padding: EdgeInsets.all(20),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: FlatButton(
                                        onPressed: () {
                                          _makeLogout();
                                        },
                                        color: Colors.red,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0, vertical: 15.0),
                                        child: Text(
                                          "Volver a Iniciar Sesión"
                                              .toUpperCase(),
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16.0,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Offstage(),
                      ],
                    )
                  : Offstage(),
        ],
      ),
    );
  }

  _makeLogout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool cleared = await preferences.clear();
    await Purchases.reset();

    if (cleared) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('splash', (Route<dynamic> route) => false);
    } else {
      print("Algo malo está pasando...");
    }
  }
}

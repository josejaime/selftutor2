//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:purchases_flutter/purchases_flutter.dart';
import '../helpers/exams_list.dart';
import '../components/loading.dart';
import '../components/myappbar.dart';
import '../helpers/select_avatar.dart';
import '../loggin/logout.dart';
import '../pages/achievements.dart';
import '../pages/friends.dart';
import '../pages/packages.dart';
import '../pages/pending_battles.dart';
import '../pages/profile.dart';
import '../pages/subjects.dart';

// HTTP
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

// Globals
import '../utils/globals.dart' as globals;

import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var _profile;
  List friends = List.empty(growable: true);
  List friendsPending = List.empty(growable: true);
  List ourFriendsPending = List.empty(growable: true);
  bool _showError = false;
  String _message = "";
  String _avatar = "0";
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  //final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool _isLogout = false;
  late int userId;

  //=========================================================
  //
  // Update the user avatar, this will be a post
  //
  //=========================================================
  Future<String> _getCurrentUser() async {
    /*var bytes = utf8.encode("password"); // data being hashed
    var digest = sha512.convert(bytes);

    print("Digest as bytes: ${digest.bytes}");
    print("Digest as hex string: $digest");*/

    //===============================================
    // Lets get the user in shared preferences
    // youll get here only after setting this number
    //===============================================
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userId = (prefs.getInt('userId') ?? 1);
    String token = prefs.getString('apiToken') as String;
    print("The user id is = $userId");
    print("The token is = $token");

    String url = globals.baseUrl;

    //===============================================
    // Lets get the user info from the server
    //===============================================
    await http.get(Uri.parse(url + "user/$userId"),
        headers: {'api-token': token}).then((http.Response response) async {
      final int statusCode = response.statusCode;
      print("Get user info Status Code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        setState(() {
          _showError = true;
          _message = "Error al conseguir tu usuario, intenta más tarde";
        });

        if (statusCode == 401) {
          setState(() {
            _message =
                'Hubo un error con la autorización de tu usuario, intena deslogearte y volver a iniciar sesión';
            _showError = true;
            //_visibleLoading = false;
            _isLogout = true;
          });
        }

        print("Get user error doing the call");
        throw new Exception("Error while fetching profile");
      } else {
        //===============================================
        // Get Friends list to save it
        //===============================================
        await http.get(Uri.parse(url + "user/$userId/friends"),
            headers: {'api-token': token}).then((http.Response response) async {
          final int statusCode = response.statusCode;
          print("Get Friends Status Code: $statusCode");

          if (statusCode < 200 || statusCode > 400) {
            print("error doing the call for friends");
            throw new Exception("Error while fetching data");
          } else {
            friends = json.decode(response.body);
            /*friends.forEach((element) {
              print("Frined - ${element['id']}");
            });*/
            List<String> friendsStrings =
                friends.map((i) => i['id'].toString()).toList();
            prefs.setStringList('friends', friendsStrings);
            print("ID de amigos actuales");
            print(friendsStrings);
          }
        });
        //===============================================
        // Get Pending Friends list to save it
        //===============================================
        await http.get(Uri.parse(url + "user/$userId/pendingrequests"),
            headers: {'api-token': token}).then((http.Response response) async {
          final int statusCode = response.statusCode;
          print("Get Pending Friends status code: $statusCode");

          if (statusCode < 200 || statusCode > 400) {
            print("error doing the call for pending friends");
            throw new Exception("Error while fetching data");
          } else {
            friendsPending = json.decode(response.body);
            //print(friends.toString());
            /*friendsPending.forEach((element) {
              print("Frined Pending - ${element['id']}");
            });*/
            List<String> friendsPendingStrings =
                friendsPending.map((i) => i['id'].toString()).toList();
            prefs.setStringList('friendsPending', friendsPendingStrings);
            print("ID de amigos pendientes");
            print(friendsPendingStrings);
          }
        });
        //===============================================
        // Get Friends Sent Request list to save it
        //===============================================
        await http.get(Uri.parse(url + "user/$userId/onwaitfriends"),
            headers: {'api-token': token}).then((http.Response response) async {
          final int statusCode = response.statusCode;
          print("Get Friends Sent Request status code: $statusCode");

          if (statusCode < 200 || statusCode > 400) {
            print("error doing the call for friends");
            throw new Exception("Error while fetching data");
          } else {
            ourFriendsPending = json.decode(response.body);
            //print(friends.toString());
            /*ourFriendsPending.forEach((element) {
              print("Our Frined Pending - ${element['id']}");
            });*/
            List<String> ourFriendsPendingStrings =
                ourFriendsPending.map((i) => i['id'].toString()).toList();
            prefs.setStringList('ourFriendsPending', ourFriendsPendingStrings);
            print("ID de nuestros amigos pendientes");
            print(ourFriendsPendingStrings);
          }
        });

        setState(() {
          var result = json.decode(response.body);

          _profile = result;

          if (_profile.length != 0) {
            //=======================================
            // USER INFO
            //=======================================
            prefs.setString('username', _profile['username']);
            prefs.setString('name', _profile['first_name']);
            prefs.setString('email', _profile['email']);
            prefs.setString('lastName', _profile['last_name']);
            prefs.setString('avatar', _profile['avatar'].toString());
            _avatar = _profile['avatar'].toString();
            //=======================================
            // USER BADGE
            //=======================================
            prefs.setInt('badgeId', _profile['badge']['id']);
            prefs.setString('badgeIcon', _profile['badge']['icon']);
            //=======================================
            // USER HIGH SCHOOL
            //=======================================
            prefs.setString('prepa', _profile['prepa_name']);
            _profile['prepa_score'] is int
                ? prefs.setInt('promedio', _profile['prepa_score'])
                : prefs.setInt('promedio', int.parse(_profile['prepa_score']));
            //prefs.setInt('promedio', int.parse(_profile['prepa_score'].toString()));
            //=======================================
            // USER CAREER
            //=======================================
            prefs.setInt('careerId', _profile['career']['id']);
            prefs.setString('careerName', _profile['career']['name']);
            _profile['career']['admision'] is int
                ? prefs.setInt(
                    'careerAdmission', _profile['career']['admission'])
                : prefs.setInt('careerAdmission',
                    int.parse(_profile['career']['admission'].toString()));
            //prefs.setInt('careerAdmission', int.parse(_profile['career']['admission'].toString()));
            //=======================================
            // USER Package
            //=======================================
            prefs.setInt('packageId', _profile['package']['id']);

            print("Perfil de usuario");
            print(result);

            initPlatformState(_profile['username']);
          } else {
            print("El perfil de usuario está vacío");
          }
        });
      }
    });

    return "success";
  }

  @override
  void initState() {
    /* _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        setState(() {
          print("Message $message on Message");
          //_message ='$message';
        });
      },
      onLaunch: (message) async {
        setState(() {
          print("Message $message on Launch");
          //_message = '$message';
        });
      },
      onResume: (message) async {
        setState(() {
          print("Message $message on Resume");
          //_message = '$message';
        });
      },
    );
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      setState(() {
        print("The token is $token");
      });
    }); */
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    _getCurrentUser();
    super.initState();
  }

  Future<void> initPlatformState(String appId) async {
    await Purchases.setDebugLogsEnabled(true);
    await Purchases.setup("gHuonXfvjUHRgbGvdygGvtsMFSOWMOZP");
    await Purchases.identify(appId);
    checkUserStatus();
  }

  Future<void> checkUserStatus() async {
    try {
      PurchaserInfo purchaserInfo = await Purchases.getPurchaserInfo();

      if (_profile['package']['id'] == 2) {
        debugPrint("ST DEBUG - USER HAVE DASHBOARD PRO");
      } else {
        if (purchaserInfo.entitlements.all["Chrysos"] != null &&
            purchaserInfo.entitlements.all["Chrysos"]!.isActive) {
          print("SELFTUTOR DEBUG : GRANT PRO ACCES");
          if (_profile['id'] != null &&
              _profile['id'] != 1 &&
              _profile['package']['id'] != 1) {
            debugPrint(
                'ST DEBUG - User in the server has not the privileges UPDATEEEE');
            _postUserUpdate(1);
          } else {
            debugPrint(
                'ST DEBUG - User Has The Correct Privileges in the server DO NOTHING');
          }
        } else {
          print("SELFTUTOR DEBUG : THIS USER HAS NOT PRO ACCESS PURCHASED");
          if (_profile['id'] != null &&
              _profile['id'] != 1 &&
              _profile['package']['id'] != 4) {
            _postUserUpdate(4);
          }
        }
      }
    } on PlatformException catch (e) {
      print("SELFTUTOR DEBUG : SOMETHING WENT BAD ${e.message}");
    }
  }

  @override
  void reassemble() {
    super.reassemble();
  }

  @override
  Widget build(BuildContext context) {
    //checkUserStatus();
    return Scaffold(
      key: _scaffoldKey,
      appBar: MyCustomAppBar(title: Text('BIENVENIDO')),
      drawer: _showError
          ? Offstage()
          : Drawer(
              child: ListView(
                children: <Widget>[
                  UserAccountsDrawerHeader(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                          image: ExactAssetImage('images/logo.png'),
                        ),
                        //color: Color.fromRGBO(125, 167, 217, 1),
                        color: Colors.white70),
                    accountName: Text(
                      "",
                      style: TextStyle(fontWeight: FontWeight.w800),
                    ),
                    accountEmail: Text(""),
                  ),
                  ListTile(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Subjects()));
                    },
                    title: Text("Asignaturas"),
                    leading: Icon(Icons.assignment),
                  ),
                  ListTile(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => UsersAchievements()));
                    },
                    title: Text("Logros"),
                    leading: Icon(Icons.check),
                  ),
                  ListTile(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => UserProfile()));
                    },
                    title: Text("Perfil"),
                    leading: Icon(Icons.person),
                  ),
                  ListTile(
                    onTap: () {
                      Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SelectAvatar()))
                          .then((value) async {
                        if (value != null) {
                          SharedPreferences preferences =
                              await SharedPreferences.getInstance();
                          preferences.setString('avatar', value.toString());
                          setState(() {
                            _avatar = value.toString();
                          });
                        }
                      });
                    },
                    title: Text("Selecciona Avatar"),
                    leading: Icon(Icons.edit),
                  ),
                  ListTile(
                    onTap: () {
                      if (_profile['id'] != null && _profile['id'] != 1) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Packages()));
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content:
                                Text('Tienes que crear una cuenta primero'),
                          ),
                        );
                      }
                    },
                    title: Text("Paquetes"),
                    leading: Icon(Icons.check_box_outline_blank),
                  ),
                  Divider(),
                  ListTile(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => LogoutUser()));
                    },
                    title: Text("Salir"),
                    leading: Icon(Icons.exit_to_app),
                  ),
                ],
              ),
            ),
      body: _profile != null
          ? Center(
              child: ListView(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    SizedBox(
                      height: 15.0,
                    ),
                    Container(
                      height: 125,
                      width: 155,
                      decoration: BoxDecoration(
                        //color: const Color(0xff7c94b6),
                        image: DecorationImage(
                          image: ExactAssetImage('images/logo.png'),
                          fit: BoxFit.contain,
                        ),
                      ),
                      //child: Text("Aquí debe haber algo"),
                    ),
                  ],
                ),
                GridView.count(
                  crossAxisCount: 2,
                  primary: false,
                  // external paddings
                  padding: const EdgeInsets.all(15),
                  // horizontal space
                  crossAxisSpacing: 10,
                  // vertical space
                  mainAxisSpacing: 10,
                  shrinkWrap: true,

                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Subjects()));
                      },
                      child: Material(
                        elevation: 4.0,
                        color: Color.fromRGBO(90, 56, 123, 1),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(0.0),
                            //color: Color.fromRGBO(90, 56, 123, 1),
                            color: Color.fromRGBO(56, 90, 123, 1),
                          ),
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                // Icon(Icons.assignment, size: 48.0, color: Colors.white,),
                                //IconButton(icon: FaIcon(FontAwesomeIcons.chess), onPressed: () {  },),
                                Image(
                                    width: 70.0,
                                    image: ExactAssetImage(
                                        'images/menu_icons/asignaturas.png')),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    "Asignaturas",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w800,
                                        fontSize: 16.0,
                                        letterSpacing: 1),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => UsersAchievements()));
                      },
                      child: Material(
                        elevation: 4.0,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(0.0),
                            //color: Color.fromRGBO(98, 96, 150, 1),
                            color: Color.fromRGBO(56, 90, 123, 1),
                          ),
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                //Icon(Icons.check, size: 48.0, color: Colors.white,),
                                Image(
                                    width: 70.0,
                                    image: ExactAssetImage(
                                        'images/menu_icons/logros.png')),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    "Logros",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w800,
                                        fontSize: 16.0,
                                        letterSpacing: 1),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        if (_profile['id'] != null && _profile['id'] != 1) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ExamsList()));
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content:
                                  Text('Tienes que crear una cuenta primero'),
                            ),
                          );
                        }
                      },
                      child: Material(
                        elevation: 4.0,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(0.0),
                            //color: Color.fromRGBO(64, 80, 97, 1),
                            color: Color.fromRGBO(56, 90, 123, 1),
                          ),
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                //Icon(Icons.golf_course, size: 48.0, color: Colors.white,),
                                Image(
                                    width: 70.0,
                                    image: ExactAssetImage(
                                        'images/menu_icons/examen.png')),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    "Exámenes",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w800,
                                        fontSize: 16.0,
                                        letterSpacing: 1),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        if (_profile['id'] != null && _profile['id'] != 1) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => UserFriends()));
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content:
                                  Text('Tienes que crear una cuenta primero'),
                            ),
                          );
                        }
                      },
                      child: Material(
                        elevation: 4.0,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(0.0),
                            color: Color.fromRGBO(56, 90, 123, 1),
                          ),
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image(
                                    width: 70.0,
                                    image: ExactAssetImage(
                                        'images/menu_icons/amigos.png')),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    "Amigos",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w800,
                                        fontSize: 16.0,
                                        letterSpacing: 1),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => UserProfile()));
                      },
                      child: Material(
                        elevation: 4.0,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(0.0),
                            color: Color.fromRGBO(78, 124, 176, 1),
                            //color: Color.fromRGBO(56, 90, 123, 1),
                          ),
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                //Icon(Icons.person, size: 48.0, color: Colors.white,),
                                Image(
                                    width: 65,
                                    image: AssetImage('images/$_avatar.png')),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    _profile['first_name'],
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w800,
                                        fontSize: 16.0,
                                        letterSpacing: 1),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      borderRadius: BorderRadius.circular(18.0),
                      onTap: () {
                        if (_profile['id'] != null && _profile['id'] != 1) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Packages()));
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content:
                                  Text('Tienes que crear una cuenta primero'),
                            ),
                          );
                        }
                      },
                      child: Material(
                        elevation: 4.0,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(0.0),
                            //color: Color.fromRGBO(81, 78, 176, 1),
                            color: Color.fromRGBO(56, 90, 123, 1),
                          ),
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                // Icon(Icons.check_box_outline_blank, size: 48.0, color: Colors.white,),
                                /* FaIcon(
                                  FontAwesomeIcons.boxes,
                                  color: Colors.white,
                                  size: 45.0,
                                ), */
                                Image(
                                    width: 70.0,
                                    image: ExactAssetImage(
                                        'images/menu_icons/paquetes.png')),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    "Paquetes",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w800,
                                        fontSize: 16.0,
                                        letterSpacing: 1),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ))
          : _showError
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        _message,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      _isLogout
                          ? Container(
                              padding: EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: FlatButton(
                                      onPressed: () {
                                        _makeLogout();
                                      },
                                      color: Colors.red,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.0, vertical: 15.0),
                                      child: Text(
                                        "Salir de la cuenta".toUpperCase(),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 16.0,
                                            color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Offstage(),
                    ],
                  ),
                )
              : Center(
                  child: LoadingMessage(
                  message: "Estamos cargando tu usuario...",
                )),
    );
  }

  _makeLogout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool cleared = await preferences.clear();
    await Purchases.reset();

    if (cleared) {
      //SystemChannels.platform.invokeMethod('SystemNavigator.pop');
      Navigator.of(context)
          .pushNamedAndRemoveUntil('splash', (Route<dynamic> route) => false);
    } else {
      print("Algo malo está pasando...");
    }
  }

  Future<bool> _postUserUpdate(int packageId) async {
    bool send = true;

    SharedPreferences preferences = await SharedPreferences.getInstance();

    int userId = preferences.getInt('userId') as int;
    String token = preferences.getString("apiToken") as String;
    String urlPostUser = globals.baseUrl + "user/changepkg";
    print('URL $urlPostUser');
    print("Making the body request...");
    var body = new Map<String, dynamic>();
    body['user_id'] = userId.toString();
    body['package_id'] = packageId.toString();
    print("Body request...");
    print(body);

    if (userId != 1 && send) {
      await http
          .post(Uri.parse(urlPostUser),
              headers: {'api-token': token}, body: body)
          .then((http.Response response) async {
        print("Intento de Postear = ${response.body}");
        final int statusCode = response.statusCode;

        print("Status del Server $statusCode");

        if (statusCode < 200 || statusCode > 400) {
          setState(() {
            debugPrint(
                "Hubo un problema al actualizar el paquete STATUS CODE $statusCode");
            return;
          });
        } else {
          var result = json.decode(response.body);

          debugPrint("Paquete actualizado con éxito STATUS CODE $statusCode");
        }
      });
    }
    return false;
  }
}

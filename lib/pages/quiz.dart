import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

// HTTP
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import '../helpers/quiz_end.dart';

// Globals
import '../utils/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

class QuizQuestion extends StatefulWidget {
  // constructor
  final quizId;
  final questionId;
  final bool correct;
  final score;
  final bool start;
  final bool end;
  final int totalQuestions;

  QuizQuestion({
    this.quizId,
    this.questionId,
    required this.correct,
    this.score,
    required this.start,
    required this.end,
    required this.totalQuestions,
  });

  @override
  _QuizQuestionState createState() => _QuizQuestionState();
}

class _QuizQuestionState extends State<QuizQuestion> {
  List data = List.empty(growable: true);
  List answers = List.empty(growable: true);
  bool _visibleLoading = true;
  bool _visibleError = false;
  bool chosed0 = false;
  bool chosed1 = false;
  bool chosed2 = false;
  bool chosed3 = false;
  bool answered = false;
  bool correct = false;
  bool end = false;
  int totalQuestions = -1;
  late Timer _timer;
  String remaining = "";
  List<bool> chosed = List<bool>.empty(growable: true);
  ValueNotifier _timerNotifier = ValueNotifier<String>('hola');
  ValueNotifier _htmlNotifier = ValueNotifier<Html>(Html(data: ''));

  @override
  void initState() {
    _getQuiz();
    _startTimer();
    _setCurrentQuestion();
    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    _timerNotifier.dispose();
    super.dispose();
  }

  _setCurrentQuestion() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setInt('currentQuestion', widget.questionId);
    preferences.setInt('score', widget.score);
  }

  // void _startTimer(timeleft - diff? or the both dates?? make the diff and
  // display diff.inminutes and diff.inSeconds)
  void _startTimer() {
    const oneSecond = Duration(seconds: 1);
    _timer = Timer.periodic(oneSecond, (timer) {
      // remainingTime = remainingTime - 1;
      // verify each second if the time has been reached
      _verifyTime();
    });
  }

  //==========================================================
  // Function
  // Check if the time for the quiz has been passed or no
  //==========================================================
  _verifyTime() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    var finalDate = DateTime.parse(pref.getString('finalDate') as String);

    var currentDate = DateTime.now();

    //print('current score ${widget.score}');

    if (currentDate.isAfter(finalDate)) {
      print('SELFTUTOR DEBUG: Se acabó el tiempo');
      _timer.cancel();
      if (_timer.isActive == false) {
        print('Score a enviar ${widget.score}');
        print('Total de preguntas ${widget.totalQuestions}');
        print('ID del quiz ${widget.quizId}');
        return Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => QuizEnd(
                      score: widget.score,
                      totalQuestions: widget.totalQuestions,
                      quizId: widget.quizId,
                      isEnded: true,
                    )));
      }
    } else {
      /*print('Aún tienes tiempo');
      print('Initital Date : $initialDate');
      print('Final Date : $finalDate');
      print('Current Date : $currentDate');*/
      Duration dif = finalDate.difference(currentDate);
      //print('Diff te queda(n) ${dif.inMinutes} minuto(s)');
      //setState(() {
      //remaining = dif.inMinutes.toString() + ":" + dif.inSeconds.toString();
      //remaining = dif.toString().substring(0, (dif.toString().length - 7));
      // });
    }
    //setState(() {
    Duration dif = finalDate.difference(currentDate);
    //remaining = dif.inMinutes.toString() + ":" + dif.inSeconds.toString();
    _timerNotifier.value =
        dif.toString().substring(0, (dif.toString().length - 7));
    //});
  }

  //=======================================
  //
  // GET all the Questions of the quiz
  //
  //=======================================
  Future<String> _getQuiz() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString("apiToken") as String;
    String url = globals.baseUrl;

    await http.get(Uri.parse(url + "quiz/${widget.quizId}/questions"),
        headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        setState(() {
          print("error doing the call");
          _visibleError = true;
          _visibleLoading = false;
        });
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          data = result;

          _htmlNotifier.value = Html(
            data: data[(widget.questionId - 1)]['question'],
            style: {
              "*": Style(color: Colors.white, fontSize: FontSize(16)),
              "p": Style(color: Colors.white, fontSize: FontSize(18)),
            },
          );
          answers = data[widget.questionId - 1]['answers'];

          chosed = List<bool>.filled(answers.length, false);

          if (data.length == 0) {
            print("No results, no error");
          } else {
            print("Erease error");
          }
          _visibleLoading = false;
          print("Total Quiz Questions ${data.length}");
          print("Obteniendo index ${widget.questionId - 1}");
          print(data[widget.questionId - 1]['question']);
          print("Total De RESPUESTAS = ${answers.length}");
          print("RESPUESTAS :");
          print(answers);

          //print("Total De RESPUESTAS = ${data[widget.questionId - 1]['answers']}");
        });
      }
    });

    return "success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: _onWillPop,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              /*Padding(
                padding: const EdgeInsets.all(15.0),
                child: Text("Contesta la siguiente pregunta", textAlign: TextAlign.center, style: TextStyle( fontWeight: FontWeight.w800, fontSize: 16.0),),
              ),*/
              //=======================================
              // Error Column
              //=======================================
              _visibleError
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                              "¡Hubo un error en el servicio, intenta más tarde!"),
                        )
                      ],
                    )
                  : Offstage(),
              //=======================================
              // Loading Column
              //=======================================
              _visibleLoading
                  ? Center(
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 50.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: CircularProgressIndicator(
                                  backgroundColor: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    "Cargando...",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 0.5),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  : Row(),
              //=======================================
              // End Loading Column
              //=======================================
              Flexible(
                child: ListView.builder(
                  itemCount: data.isEmpty ? 0 : 1,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              color: Color.fromRGBO(25, 48, 80, 0.8),
                            ),
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                minHeight:
                                    MediaQuery.of(context).size.height * 0.35,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 15.0, left: 25.0),
                                        child: CircleAvatar(
                                          backgroundColor: Colors.white70,
                                          child: Text("${widget.questionId}"),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 15.0, right: 25.0),
                                        child: ValueListenableBuilder(
                                          valueListenable: _timerNotifier,
                                          builder: (_, value, __) => Text(
                                            value.toString(),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  /* Row(
                                    children: [
                                      ValueListenableBuilder(
                                        valueListenable: _timerNotifier,
                                        builder: (_, value, __) => Container(
                                          child: Text(value.toString()),
                                        ),
                                      )
                                    ],
                                  ), */
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 25.0),
                                    child: _htmlNotifier.value,
                                  ),
                                  //QuestionContent(data: data, widget: widget),
                                ],
                              ),
                            ),
                          ),
                          answers != null
                              ? getTextWidgets(context, answers)
                              : Row(),
                          SizedBox(height: 8.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: ((data.length) == widget.questionId)
                                      ? RaisedButton(
                                          //color: Color.fromRGBO(25, 48, 80, 0.7),
                                          color:
                                              Color.fromRGBO(78, 124, 176, 1),
                                          child: Text(
                                            "Finalizar",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          onPressed: () {
                                            nextQuestion();
                                          },
                                        )
                                      : RaisedButton(
                                          //color: Color.fromRGBO(25, 48, 80, 0.7),
                                          color:
                                              Color.fromRGBO(78, 124, 176, 1),
                                          padding: EdgeInsets.symmetric(
                                              vertical: 15.0, horizontal: 1.0),
                                          child: Text(
                                            "Siguiente",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          onPressed: () {
                                            nextQuestion();
                                          },
                                        ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getTextWidgets(BuildContext context, List strings) {
    List<Widget> list = List<Widget>.empty(growable: true);
    List<String> vowels = ['a', 'b', 'c', 'd'];

    for (var i = 0; i < strings.length; i++) {
      list.add(Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        child: Material(
          borderRadius: BorderRadius.circular(10.0),
          color: chosed[i] ? Colors.lightBlue : Colors.white,
          child: Row(
            children: <Widget>[
              Expanded(
                child: FlatButton(
                  child: Text('${strings[i]['value']}'),
                  onPressed: () {
                    print('answer choosed');
                    int isCorrect = -1;
                    strings[i]['is_correct'] is int
                        ? isCorrect = strings[i]['is_correct']
                        : isCorrect = int.parse(strings[i]['is_correct']);

                    changeQuestion(isCorrect);
                    //markQuestion(i);
                    markQuestion2(i);
                  },
                ),
              ),
            ],
          ),
        ),
      ));
    }
    //print("Lista de falsos :");
    //print(chosed.length);
    return Column(children: list);
  }

  markQuestion2(int index) {
    setState(() {
      chosed = List<bool>.filled(answers.length, false);
      chosed[index] = true;
    });
    print(chosed);
  }

  nextQuestion() {
    // add to the score
    int score = widget.score;
    print("Data Length: ${data.length}");
    print("Question ID: ${widget.questionId}");

    if (data.length == widget.questionId) {
      end = true;
      totalQuestions = data.length;
    }
    if (correct) {
      score = score + 1;
    }

    if (!end) {
      _timer.cancel();
      //return Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => QuizQuestion(
      return Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => QuizQuestion(
                    quizId: widget.quizId,
                    questionId: widget.questionId + 1,
                    start: false,
                    correct: correct,
                    end: end,
                    score: score,
                    totalQuestions: widget.totalQuestions,
                  )));
    } else {
      _timer.cancel();
      return Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => QuizEnd(
                    score: score,
                    totalQuestions: widget.totalQuestions,
                    quizId: widget.quizId,
                    isEnded: true,
                  )));
    }
  }

  changeQuestion(int isCorrect) {
    // here will go with the flow and
    // add to the score
    int score = widget.score;

    if (data.length == widget.questionId) {
      end = true;
      totalQuestions = data.length;
    }
    if (isCorrect == 1) {
      score = score + 1;
      correct = true;
    } else {
      correct = false;
    }

    print("---------");
    print('Question number = ${widget.questionId}');
    print('Is_correct = $isCorrect');
    print('Top Correct = $correct');
    print('End = $end');
    print('Score = $score');
    print("---------");
  }

  markQuestion(int index) {
    setState(() {
      switch (index) {
        case 0:
          chosed0 = !chosed0;
          chosed1 = false;
          chosed2 = false;
          chosed3 = false;
          break;
        case 1:
          chosed0 = false;
          chosed1 = !chosed1;
          chosed2 = false;
          chosed3 = false;
          break;
        case 2:
          chosed0 = false;
          chosed1 = false;
          chosed2 = !chosed2;
          chosed3 = false;
          break;
        case 3:
          chosed0 = false;
          chosed1 = false;
          chosed2 = false;
          chosed3 = !chosed3;
          break;
        default:
      }
    });
  }

  Future<bool> _onWillPop() async {
    return (await showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            content: Text(
              '¿Realmente quires salir del Quiz?',
              style: TextStyle(fontSize: 20.0),
            ),
            actions: [
              Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: TextButton(
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Color.fromRGBO(78, 124, 176, 1),
                      ),
                      /* padding: EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 12.5),
                      color: Color.fromRGBO(78, 124, 176, 1), */
                      onPressed: () => Navigator.of(context).pop(false),
                      child: Text('No, Continuar'.toUpperCase(),
                          style: TextStyle(color: Colors.white)),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: TextButton(
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.red,
                      ),
                      // Also cancel the timer here.
                      onPressed: () {
                        _timer.cancel();
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => QuizEnd(
                                      score: widget.score,
                                      totalQuestions: widget.totalQuestions,
                                      quizId: widget.quizId,
                                      isEnded: true,
                                    )));
                      },
                      child: Text(
                        'Sí, Salir'.toUpperCase(),
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        )) ??
        false;
  }
}

class QuestionContent extends StatelessWidget {
  const QuestionContent({
    Key? key,
    required this.data,
    required this.widget,
  }) : super(key: key);

  final List data;
  final QuizQuestion widget;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
          child: Html(
            data: data[(widget.questionId - 1)]['question'],
            style: {
              "*": Style(color: Colors.white, fontSize: FontSize(16)),
              "p": Style(color: Colors.white, fontSize: FontSize(18)),
            },
          ),
        ),
      ],
    );
  }
}

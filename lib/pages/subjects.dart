import 'package:flutter/material.dart';
import 'package:purchases_flutter/purchases_flutter.dart';
import '../components/myappbar.dart';
import '../helpers/exams_list.dart';
import '../pages/lesson.dart';

// HTTP
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

// Globals
import '../utils/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

//===========================================================
// Hacer display de las asignaturas en la app
// como por ejemplo (redaccion, matematicas, ingles)
//===========================================================
class Subjects extends StatefulWidget {
  @override
  _SubjectsState createState() => _SubjectsState();
}

class _SubjectsState extends State<Subjects> {
  List data = List.empty(growable: true);
  bool _visibleLoading = true;
  bool _visibleError = false;
  bool _isLogout = false;
  String _message = "";
  int _packageId = 0;
  int userId = 0;

  @override
  void initState() {
    //_getData();
    _getSubjects();
    super.initState();
  }

  Future<String> _getSubjects() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString('apiToken') as String;
    _packageId = preferences.getInt('packageId') as int;
    userId = preferences.getInt('userId') as int;

    String url = globals.baseUrl;
    await http.get(Uri.parse(url + "subjects"),
        headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");

        setState(() {
          _message = 'Hubo un error conectando con el servidor';
          _visibleError = true;
          _visibleLoading = false;
        });

        if (statusCode == 401) {
          setState(() {
            _message =
                'Hubo un error con la autorización de tu usuario, intena deslogearte y volver a iniciar sesión';
            _visibleError = true;
            _visibleLoading = false;
            _isLogout = true;
          });
        }

        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          data = result;

          if (data.length == 0) {
            print("No results, no error");

            setState(() {
              _visibleError = true;
              _message = 'Hubo un error con los datos, intenta más tarde';
            });
          } else {
            print("Erease error");
          }

          _visibleLoading = false;
          print(data.length);
          print(result);
        });
      }
    });

    return "success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(
        title: Text('Asignaturas'),
      ),
      body: _visibleLoading
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Estamos cargando las asignaturas...'),
                  SizedBox(
                    height: 25.0,
                  ),
                  CircularProgressIndicator(
                    backgroundColor: Colors.white,
                  ),
                ],
              ),
            )
          : Center(
              child: _visibleError
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          _message,
                          textAlign: TextAlign.center,
                        ),
                        _isLogout
                            ? Container(
                                padding: EdgeInsets.all(20),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: FlatButton(
                                        onPressed: () {
                                          _makeLogout();
                                        },
                                        color: Colors.red,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0, vertical: 15.0),
                                        child: Text(
                                          "Volver a Iniciar Sesión"
                                              .toUpperCase(),
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16.0,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Offstage(),
                      ],
                    )
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 125,
                          width: 155,
                          decoration: BoxDecoration(
                            //color: const Color(0xff7c94b6),
                            image: DecorationImage(
                              image: ExactAssetImage('images/logo.png'),
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            "Consulta nuestras asignaturas",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.w800, fontSize: 16.0),
                          ),
                        ),
                        Flexible(
                            child: GridView.builder(
                          // external paddings
                          padding: const EdgeInsets.all(10),
                          itemCount: data == null ? 0 : data.length,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            mainAxisSpacing: 10.0,
                            crossAxisSpacing: 10.0,
                          ),
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              borderRadius: BorderRadius.circular(0.0),
                              onTap: () {
                                print("selected ${data[index]['id']}");
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => Lesson(
                                        id: data[index]['id'],
                                        name: data[index]['name'],
                                        portrait: data[index]['portrait'],
                                      ),
                                      settings: RouteSettings(name: "/lessons"),
                                    ));
                              },
                              child: Material(
                                elevation: 4.0,
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(0.0),
                                    color: Color.fromRGBO(56, 90, 123, 1),
                                  ),
                                  child: Center(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        //Icon(Icons.assignment, size: 48.0, color: Colors.white,),
                                        /*Image(
                                  width: 35.0,
                                  image: index == 1 ? AssetImage('images/maths.png') : AssetImage('images/literature.png') , 
                                ),*/
                                        getIcon(data[index]['portrait']),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 12.5),
                                          child: Text(
                                            data[index]['name'],
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16.0,
                                                letterSpacing: 1),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        )),
                      ],
                    ),
            ),
      bottomNavigationBar: _visibleLoading
          ? Offstage()
          : !_visibleError
              ? Material(
                  color: Color.fromRGBO(78, 124, 176, 1),
                  //color: Colors.white,
                  child: InkWell(
                    onTap: () {
                      if (userId != 1) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ExamsList()));
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 15.0, horizontal: 10.0),
                      child: Text(
                        userId == 1
                            ? 'Crea una cuenta para ver los exámenes'
                            : "¡Exámenes!".toUpperCase(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0),
                      ),
                    ),
                  ),
                )
              : Offstage(),
    );
  }

  Widget getIcon(String index) {
    if (index == "school") {
      return Icon(
        Icons.school,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "book") {
      return Icon(
        Icons.book,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "note") {
      return Icon(
        Icons.note,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "library_books") {
      return Icon(
        Icons.library_books,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "import_contacts") {
      return Icon(
        Icons.import_contacts,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "photo_album") {
      return Icon(
        Icons.photo_album,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "collections_bookmark") {
      return Icon(
        Icons.collections_bookmark,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "local_library") {
      return Icon(
        Icons.local_library,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "score") {
      return Icon(
        Icons.score,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "date_range") {
      return Icon(
        Icons.date_range,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "category") {
      return Icon(
        Icons.category,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "functions") {
      return Icon(
        Icons.functions,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "account_balance") {
      return Icon(
        Icons.account_balance,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "art_track") {
      return Icon(
        Icons.art_track,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "description") {
      return Icon(
        Icons.description,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "donut_large") {
      return Icon(
        Icons.donut_large,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "g_translate") {
      return Icon(
        Icons.g_translate,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "language") {
      return Icon(
        Icons.language,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "offline_bolt") {
      return Icon(
        Icons.offline_bolt,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "chrome_reader_mode") {
      return Icon(
        Icons.chrome_reader_mode,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "font_download") {
      return Icon(
        Icons.font_download,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "blur_circular") {
      return Icon(
        Icons.blur_circular,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "plus_one") {
      return Icon(
        Icons.plus_one,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "business") {
      return Icon(
        Icons.business,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "lightbulb_outline") {
      return Icon(
        Icons.lightbulb_outline,
        color: Colors.white,
        size: 36.0,
      );
    }
    if (index == "explore") {
      return Icon(
        Icons.explore,
        color: Colors.white,
        size: 36.0,
      );
    }
    //return FaIcon(FontAwesomeIcons.helicopter);
    return Icon(
      Icons.book,
      color: Colors.white,
      size: 36.0,
    );
  }

  _makeLogout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool cleared = await preferences.clear();
    await Purchases.reset();

    if (cleared) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('splash', (Route<dynamic> route) => false);
    } else {
      print("Algo malo está pasando...");
    }
  }
}

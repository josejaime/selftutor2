import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
// HTTP
import 'package:http/http.dart' as http;
import '../components/myappbar.dart';
import '../helpers/quiz_instructions.dart';
import 'dart:async';

// Globals
import '../utils/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

class Content extends StatefulWidget {
  // Constructor
  final id;
  final String name;
  final String content;
  final int subjectId;
  final String subjectPortrait;
  final String subjectName;

  Content({
    this.id, // The lesson id
    required this.name, // the lesson name
    required this.content, // the lesson file name
    required this.subjectId, // the subject id
    required this.subjectPortrait,
    required this.subjectName,
  });

  @override
  _ContentState createState() => _ContentState();
}

class _ContentState extends State<Content> {
  var data;
  bool _visibleLoading = true;
  bool _visibleError = false;

  @override
  void initState() {
    super.initState();
    _getLessons();
  }

  //=======================================
  //
  // GET all the lessson for the subject
  //
  //=======================================
  Future<String> _getLessons() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString("apiToken") as String;

    String url = globals.baseUrl;

    await http.get(Uri.parse(url + "lesson/${widget.id}"),
        headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        setState(() {
          print("error doing the call");
          _visibleError = true;
          _visibleLoading = false;
        });
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          data = result;

          if (data.length == 0) {
            print("No results, no error");
          } else {
            print("Erease error");
          }
          _visibleLoading = false;
        });

        print("DATA IS :");
        print("Lección " + data.toString());
      }
    });

    return "success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyCustomAppBar(
        title: Text("${widget.name}"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            //=======================================
            // Error Column
            //=======================================
            _visibleError
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 25.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "¡Hubo un error en el servicio, intenta más tarde!",
                          style: TextStyle(color: Colors.black),
                        ),
                      )
                    ],
                  )
                : Row(),
            //=======================================
            // Loading Column
            //=======================================
            _visibleLoading
                ? Column(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          SizedBox(
                            height: 25.0,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: CircularProgressIndicator(),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "Cargando...",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    letterSpacing: 0.5,
                                    color: Colors.black),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  )
                : Row(),
            //=======================================
            // End Loading Column
            //=======================================
            //===========================
            // display the data
            //===========================
            Flexible(
              child: ListView.builder(
                itemCount: data == null ? 0 : 1,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: Colors.blueGrey, width: 1.0))),
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          title: Html(
                              data: data['content'] != ""
                                  ? data['content']
                                  : '<p>No hay contenido</p>',
                              style: {
                                "p": Style(
                                  color: Colors.black,
                                  textAlign: TextAlign.justify,
                                ),
                                "*": Style(
                                  color: Colors.black,
                                ),
                              }),
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                decoration: BoxDecoration(
                                  //color: Color.fromRGBO(25, 48, 80, 0.7)
                                  color: Color.fromRGBO(78, 124, 176, 1),
                                ),
                                child: data['content'] != ""
                                    ? FlatButton(
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text("¡Haz el Quiz!",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16.0)),
                                        ),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      QuizInstructions(
                                                        lessonId: widget.id,
                                                        nameLesson: widget.name,
                                                        subjectId:
                                                            widget.subjectId,
                                                        subjectName:
                                                            widget.subjectName,
                                                        subjectPortrait: widget
                                                            .subjectPortrait,
                                                      )));
                                        },
                                      )
                                    : Text("HOLA"),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),

      /*bottomNavigationBar: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(25, 48, 80, 0.7)
        ),
        child: FlatButton(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Siguiente Lección", style: TextStyle( color: Colors.white, fontSize: 16.0 )),
          ), 
          onPressed: () {
            Navigator.push(context, MaterialPageRoute( builder: (context) => QuizInstructions(
              lessonId: widget.id,
              nameLesson: widget.name,
            )));
          },
        ),
      ),*/
    );
  }
}

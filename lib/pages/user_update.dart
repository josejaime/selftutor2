import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
// HTTP
import 'package:http/http.dart' as http;
import '../components/myappbar.dart';
import 'dart:async';
import 'dart:convert';

// Globals
import '../utils/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

class UpdateUser extends StatefulWidget {
  @override
  _UpdateUserState createState() => _UpdateUserState();
}

class _UpdateUserState extends State<UpdateUser> {
  var profile;
  String profileImage = "";
  String _message = "";
  bool _showMessage = false;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _firstName = TextEditingController();
  TextEditingController _lastName = TextEditingController();
  TextEditingController _prepaName = TextEditingController();
  TextEditingController _prepaScore = TextEditingController();
  TextEditingController _password = TextEditingController();
  bool sending = false;

  @override
  initState() {
    _getProfile();
    super.initState();
  }

  /*========================================================*/
  /* Lets call the profile from the server */
  /* Now just call the ID = 1              */
  /*========================================================*/
  Future<String> _getProfile() async {
    //===============================================
    // Lets get the user in shared preferences
    //===============================================
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int userId = (prefs.getInt('userId') ?? 1);
    String token = prefs.getString('apiToken') as String;
    print("The user id is = $userId");

    String url = globals.baseUrl;

    await http.get(Uri.parse(url + "user/$userId"),
        headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        throw new Exception("Error while fetching data");
      } else {
        String avatar = prefs.getString('avatar') as String;
        profileImage = "$avatar.png";

        setState(() {
          var result = json.decode(response.body);
          profile = result;
          print(profile['username']);
          _firstName.text = profile['first_name'].toString();
          _lastName.text = profile['last_name'].toString();
          _prepaScore.text = profile['prepa_score'].toString();
          _prepaName.text = profile['prepa_name'].toString();
        });

        print(profile);
      }
    });

    return "success";
  }

  Future<bool> _postUserUpdate() async {
    bool send = true;

    setState(() {
      _message = "";
      _showMessage = false;
      sending = true;
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();

    int userId = preferences.getInt('userId') as int;
    String token = preferences.getString("apiToken") as String;
    String urlPostUser = globals.baseUrl + "user/$userId";
    print('URL $urlPostUser');
    print("Making the body request...");
    var body = new Map<String, dynamic>();
    //body['id'] = userId.toString();
    body['first_name'] = _firstName.text.toString();
    body['_method'] = 'PUT';
    body['last_name'] = _lastName.text.toString();
    body['prepa_name'] = _prepaName.text.toString();
    body['email'] = profile['email'].toString();
    body['avatar'] = profile['avatar'].toString();
    body['career_id'] = profile['career_id'].toString();
    body['prepa_score'] = _prepaScore.text.toString();
    print("Body request...");
    print(body);

    if (_password.text != "" && _password.text.length > 5) {
      var bytes = utf8.encode(_password.text); // data being hashed
      var digest = sha512.convert(bytes);
      body['password'] = digest.toString();
    }

    if (userId != 1 && send) {
      await http
          .post(Uri.parse(urlPostUser),
              headers: {'api-token': token}, body: body)
          .then((http.Response response) async {
        print("Intento de Postear = ${response.body}");
        final int statusCode = response.statusCode;

        print("Status del Server $statusCode");

        if (statusCode < 200 || statusCode > 400) {
          setState(() {
            sending = false;
            _showMessage = true;
            _message = "Hubo un error con el servidor";
            print(
                "Lo sentimos, hubo un error al procesar tu búsqueda, intenta más tarde STATUS CODE $statusCode");
          });
          throw new Exception("Error while fetching data");
        } else {
          var result = json.decode(response.body);

          if (result.length > 0) {
            if (result[0] != -1) {
              setState(() {
                sending = false;
                _message = '';
                profile = result;
              });
              return Navigator.of(context).pushNamedAndRemoveUntil(
                  'home', (Route<dynamic> route) => false);
            } else {
              setState(() {
                sending = false;
                _message = result[2];
              });
            }
          } else {
            setState(() {
              sending = false;
              _showMessage = true;
              _message = "Hubo un error en la respuesta";
            });
          }

          setState(() {
            sending = false;
          });
        }
      });
    }
    setState(() {
      sending = false;
    });
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(
        title: Text('Tu cuenta'),
      ),
      body: Column(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Material(
                elevation: 3.0,
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(56, 90, 123, 1),
                      border: Border(
                          bottom: BorderSide(
                        color: Colors.white,
                      ))),
                  child: profile != null
                      ? Row(
                          children: <Widget>[
                            Expanded(
                                flex: 1,
                                child: Center(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(50.0),
                                    child: Image(
                                      image:
                                          AssetImage('images/' + profileImage),
                                      width: 120.0,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                )),
                            Expanded(
                                child: Material(
                              color: Colors.white,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    profile["first_name"] +
                                        ' ' +
                                        profile["last_name"],
                                    style: TextStyle(
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black),
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(8.0),
                                    decoration: BoxDecoration(
                                        color: Color.fromRGBO(25, 48, 80, 1),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(6.5),
                                      child: Text(
                                        'Score = ${profile['global_score']}',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )),
                          ],
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircularProgressIndicator(
                              backgroundColor: Colors.white,
                            ),
                          ],
                        ),
                ),
              ),
            ],
          ),
          Flexible(
            child: ListView(
              children: <Widget>[
                profile != null
                    ? Column(
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(8.0, 18.0, 8.0, 8.0),
                            child: Text("Información personal"),
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Form(
                                  key: _formKey,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Card(
                                        elevation: 1.0,
                                        child: Column(
                                          children: <Widget>[
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 8.0,
                                                      vertical: 15.0),
                                              child: Column(
                                                children: <Widget>[
                                                  Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                        child: TextFormField(
                                                          controller:
                                                              _firstName,
                                                          decoration:
                                                              _myInputDecoration(
                                                                  profile[
                                                                      'first_name'],
                                                                  Icons.person),
                                                          onFieldSubmitted:
                                                              (String value) {},
                                                          validator: (value) {
                                                            if (value!
                                                                .isEmpty) {
                                                              return "* Debes ingresar una búsqueda";
                                                            }
                                                            if (value.length <
                                                                3) {
                                                              return "* Intenta con mínimo 3 caracteres";
                                                            }

                                                            return null;
                                                          },
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 8.0,
                                                  ),
                                                  Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                        child: TextFormField(
                                                          controller: _lastName,
                                                          decoration:
                                                              _myInputDecoration(
                                                                  profile['last_name']
                                                                      .toString(),
                                                                  Icons.score),
                                                          onFieldSubmitted:
                                                              (String value) {},
                                                          validator: (value) {
                                                            if (value!
                                                                .isEmpty) {
                                                              return "* Debes ingresar una búsqueda";
                                                            }
                                                            if (value.length <
                                                                3) {
                                                              return "* Intenta con mínimo 3 caracteres";
                                                            }

                                                            return null;
                                                          },
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 8.0,
                                                  ),
                                                  profile['prepa_name'] != null
                                                      ? Row(
                                                          children: <Widget>[
                                                            Expanded(
                                                              child:
                                                                  TextFormField(
                                                                controller:
                                                                    _prepaName,
                                                                decoration: _myInputDecoration(
                                                                    profile[
                                                                        'prepa_name'],
                                                                    Icons
                                                                        .school),
                                                                onFieldSubmitted:
                                                                    (String
                                                                        value) {},
                                                                validator:
                                                                    (value) {
                                                                  if (value!
                                                                      .isEmpty) {
                                                                    return "* Debes ingresar una búsqueda";
                                                                  }
                                                                  if (value
                                                                          .length <
                                                                      3) {
                                                                    return "* Intenta con mínimo 3 caracteres";
                                                                  }

                                                                  return null;
                                                                },
                                                              ),
                                                            ),
                                                          ],
                                                        )
                                                      : Row(),
                                                  SizedBox(
                                                    height: 8.0,
                                                  ),
                                                  Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                        child: TextFormField(
                                                          controller:
                                                              _prepaScore,
                                                          decoration:
                                                              _myInputDecoration(
                                                                  profile['prepa_score']
                                                                      .toString(),
                                                                  Icons.score),
                                                          onFieldSubmitted:
                                                              (String value) {},
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          validator: (value) {
                                                            if (value!
                                                                .isEmpty) {
                                                              return "* Debes ingresar una búsqueda";
                                                            }
                                                            if (value.length <
                                                                2) {
                                                              return "* Intenta con mínimo 3 caracteres";
                                                            }

                                                            return null;
                                                          },
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Divider(
                                                    color: Colors.black,
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets
                                                            .fromLTRB(
                                                        8.0, 4.0, 8.0, 8.0),
                                                    child: Text(
                                                      "¿Renovar Contraseña?",
                                                      style: TextStyle(
                                                          color: Colors.black),
                                                    ),
                                                  ),
                                                  Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                        child: TextFormField(
                                                          controller: _password,
                                                          decoration:
                                                              _myInputDecoration(
                                                                  'Nueva contraseña',
                                                                  Icons
                                                                      .security),
                                                          onFieldSubmitted:
                                                              (String value) {},
                                                          keyboardType:
                                                              TextInputType
                                                                  .visiblePassword,
                                                          obscureText: true,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        )),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8.0, vertical: 15.0),
                                  child: FlatButton(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.0, vertical: 15.0),
                                      color: Color.fromRGBO(78, 124, 176, 1),
                                      onPressed: !sending
                                          ? () {
                                              print("Hello");
                                              if (_formKey.currentState!
                                                  .validate()) {
                                                print(
                                                    "All good with the update form");
                                                setState(() {
                                                  sending = true;
                                                });
                                                _postUserUpdate();
                                              } else {
                                                print(
                                                    "There are erros on the update form");
                                              }
                                            }
                                          : () {
                                              print("Sending user post data");
                                            },
                                      child: Text(
                                        sending
                                            ? "Cargando...".toUpperCase()
                                            : "Actualizar".toUpperCase(),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.w500),
                                      )),
                                ),
                              )
                            ],
                          ),
                        ],
                      )
                    : Center(
                        child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 25.0,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.white,
                            ),
                          ),
                          Text('Cargando...'),
                          SizedBox(
                            height: 25.0,
                          ),
                        ],
                      )),
              ],
            ),
          ),
        ],
      ),
    );
  }

  InputDecoration _myInputDecoration(String hint, IconData iconData) {
    return InputDecoration(
        filled: true,
        fillColor: Colors.white,
        isDense: true,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        enabledBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(0.0)),
          borderSide: const BorderSide(
              color: Color.fromRGBO(78, 124, 176, 1), width: 1.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(0.0)),
          borderSide: BorderSide(
            color: Colors.black,
            width: 1.0,
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          borderSide: BorderSide(
            width: 1.0,
          ),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          borderSide: BorderSide(
            color: Colors.red,
            width: 2.0,
          ),
        ),
        errorStyle: TextStyle(color: Color.fromRGBO(78, 124, 176, 1)),
        hintText: hint,
        prefixIcon: IconButton(
            icon: Icon(
              iconData,
              color: Colors.black,
            ),
            onPressed: () {
              /*if (_formController.currentState.validate() ) {
            //_getFriendsSearch();
            FocusScope.of(context).requestFocus(FocusNode());
          } else {
            print("Not ready for search");
          }*/
            }));
  }
}

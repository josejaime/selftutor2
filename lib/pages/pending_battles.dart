import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:purchases_flutter/purchases_flutter.dart';
import '../components/myappbar.dart';
import '../friends/battle_instructions.dart';
import 'package:shared_preferences/shared_preferences.dart';
// HTTP
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import '../utils/globals.dart' as globals;

class PendingBattles extends StatefulWidget {
  @override
  _PendingBattlesState createState() => _PendingBattlesState();
}

class _PendingBattlesState extends State<PendingBattles> {
  bool _isLoading = true;
  bool _showMessage = false;
  bool _isLogout = false;
  List _data = List.empty(growable: true);
  String _message = "";

  Future<String> _getPendingBattles() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString("apiToken") as String;
    int userId = preferences.getInt('userId') as int;

    String url = globals.baseUrl + "challenges/$userId/todo";

    await http.get(Uri.parse(url), headers: {'api-token': token}).then(
        (http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        setState(() {
          print("error doing the call");
          _showMessage = true;
          _message = "Error en el servidor, intenta más tarde";
          _isLoading = false;
        });
        if (statusCode == 401) {
          setState(() {
            _message =
                'Hubo un error con la autorización de tu usuario, intena deslogearte y volver a iniciar sesión';
            _showMessage = true;
            _isLoading = false;
            _isLogout = true;
          });
        }
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);

          if (result.length > 0) {
            if (result[0] != -1) {
              print("Result =" + result.length.toString());

              _data = result;
              print("Data =" + _data.length.toString());

              if (_data.length == 0) {
                print("No results, no error");
                _showMessage = true;
                _message = "No tienes retos pendientes";
              } else {
                print("Erease error");
                _showMessage = false;
              }
              _isLoading = false;

              print(result);
            } else {
              setState(() {
                _isLoading = false;
                _showMessage = true;
                _message = result[2];
                //TODO: What append here
                //_data = null;
              });
            }
          } else {
            setState(() {
              _isLoading = false;
              _showMessage = true;
              _message = "No tienes retos pendientes";
              _data = result;
            });
          }
        });
      }
    });

    return "success";
  }

  @override
  void initState() {
    _getPendingBattles();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MyCustomAppBar(
          title: Text("Retos Pendientes"),
        ),
        body: _isLoading
            ? Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(
                        backgroundColor: Colors.white,
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Text("Cargando retos..."),
                    ]),
              )
            : _showMessage
                ? Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            _message,
                            textAlign: TextAlign.center,
                          ),
                          _isLogout
                              ? Container(
                                  padding: EdgeInsets.all(20),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: FlatButton(
                                          onPressed: () {
                                            _makeLogout();
                                          },
                                          color: Colors.red,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10.0, vertical: 15.0),
                                          child: Text(
                                            "Volver a Iniciar Sesión"
                                                .toUpperCase(),
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16.0,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : Offstage(),
                        ]),
                  )
                : Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(children: [
                        Flexible(
                            child: ListView.builder(
                          itemCount: _data == null ? 0 : _data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                              elevation: 8.0,
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(4)),
                                child: ListTile(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 20.0, vertical: 0.0),
                                  leading: Container(
                                    padding: EdgeInsets.only(right: 12.0),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            right: BorderSide(
                                                width: 1.0,
                                                color: Colors.black38))),
                                    // 1 - You been challenged
                                    child: FaIcon(
                                      FontAwesomeIcons.clock,
                                      size: 45.0,
                                    ),
                                  ),
                                  title: Text(
                                    "${_data[index]['friend']['first_name']} te desafío...",
                                    style: TextStyle(color: Colors.black),
                                  ),
                                  subtitle: Row(
                                    children: <Widget>[
                                      Column(
                                        children: [
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Container(
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      78, 124, 176, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(9)),
                                              child: Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      vertical: 3.0,
                                                      horizontal: 12.0),
                                                  child: Text(
                                                    "Responde el desafío...",
                                                    style: TextStyle(
                                                        color: Colors.black),
                                                  ))),
                                        ],
                                      ),
                                    ],
                                  ),
                                  trailing: Offstage(),
                                  onTap: () async {
                                    SharedPreferences preferences =
                                        await SharedPreferences.getInstance();
                                    int userId =
                                        preferences.getInt('userId') as int;
                                    String userAvatar = preferences
                                        .getString('avatar') as String;
                                    String name =
                                        preferences.getString('name') as String;
                                    String contenderName =
                                        _data[index]['friend']['username'];
                                    userAvatar = "" + userAvatar + ".png";
                                    String contenderAvatar =
                                        "${_data[index]['friend']['avatar']}.png";

                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              BattleInstrucionts(
                                            userId: userId,
                                            userAvatar: userAvatar,
                                            contenderId: _data[index]['friend']
                                                ['id'], //widget.userId
                                            contenderAvatar: contenderAvatar,
                                            userName: name,
                                            contenderName: contenderName,
                                            response: true,
                                            challengeId: _data[index]['id'],
                                          ),
                                        ));
                                  },
                                ),
                              ),
                            );
                          },
                        )),
                      ]),
                    ),
                  ));
  }

  _makeLogout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool cleared = await preferences.clear();
    await Purchases.reset();

    if (cleared) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('splash', (Route<dynamic> route) => false);
    } else {
      print("Algo malo está pasando...");
    }
  }
}

import 'package:flutter/material.dart';
import '../loggin/register.dart';
import '../pages/home.dart';
import '../utils/globals.dart' as globals;
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:crypto/crypto.dart';

import 'package:shared_preferences/shared_preferences.dart';

class LogginUser extends StatefulWidget {
  @override
  _LogginUserState createState() => _LogginUserState();
}

class _LogginUserState extends State<LogginUser> {
  final _formController = GlobalKey<FormState>();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _passController = TextEditingController();
  bool _isLoading = false;
  bool _showMessage = false;
  String _message = "";
  var _data;

  _makeLogin() async {
    String url = globals.baseUrl + "login";
    print("empezando llamada");
    print(url);

    setState(() {
      _isLoading = true;
      _showMessage = false;
      _message = "Cargando...";
    });

    Map body = Map<String, dynamic>();
    body['username'] = _nameController.text;
    // MAKE THE PASS
    //print('password: ${_passController.text}');
    var bytes = utf8.encode(_passController.text); // data being hashed
    var digest = sha512.convert(bytes);
    body['password'] = digest.toString();

    print(body);

    await http
        .post(Uri.parse(url), body: body)
        .then((http.Response response) async {
      //==================================
      // get the Status Code
      // these part may be useful later
      //==================================
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        setState(() {
          //_showMessage = true;
          _isLoading = false;
          _showMessage = true;
          _message = "Hubo un error con el servidor";
          print(
              "Lo sentimos, hubo un error al procesar tu búsqueda, intenta más tarde STATUS CODE $statusCode");
        });
        throw new Exception("Error while fetching data");
      } else {
        if (response.bodyBytes.isEmpty) {
          setState(() {
            _isLoading = false;
            _showMessage = true;
            _message = "Tus credenciales no son correctas, intenta nuevamente";
          });
        } else {
          var _result = json.decode(response.body);
          _data = _result;
          SharedPreferences preferences = await SharedPreferences.getInstance();
          await preferences.setString("apiToken", _data['api_token']);
          await preferences.setBool(
              'loged', true); // LETS GET TO THE LOGED ALREADY
          await preferences.setInt(
              'userId', _data['id']); // LETS GET ALSO THE ID
          setState(() {
            _isLoading = false;
            _message = "";
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => Home()));
          });
        }
      }
    }).timeout(Duration(seconds: 60), onTimeout: () {
      setState(() {
        _isLoading = false;
        _message =
            "El servicio no está disponible por el momento, intenta nuevamente";
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                height: 125,
                width: 155,
                decoration: BoxDecoration(
                  //color: const Color(0xff7c94b6),
                  image: DecorationImage(
                    image: ExactAssetImage('images/logo.png'),
                    fit: BoxFit.contain,
                  ),
                ),
                //child: Text("Aquí debe haber algo"),
              ),
              SizedBox(
                height: 15.0,
              ),
              Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  padding: EdgeInsets.all(25.0),
                  decoration: BoxDecoration(
                      color: Colors.grey.withAlpha(50),
                      borderRadius: BorderRadius.circular(5)),
                  child: Column(children: [
                    SizedBox(
                      height: 15.0,
                    ),
                    Form(
                        key: _formController,
                        child: Column(children: [
                          Row(
                            children: [
                              Text(
                                "Usuario",
                                textAlign: TextAlign.left,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          TextFormField(
                            controller: _nameController,
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                enabledBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  borderSide: const BorderSide(
                                      color: Color.fromRGBO(78, 124, 176, 1),
                                      width: 1.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  borderSide: BorderSide(
                                    color: Color.fromRGBO(78, 124, 176, 1),
                                    width: 1.0,
                                  ),
                                ),
                                errorBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  borderSide: BorderSide(
                                    width: 1.0,
                                  ),
                                ),
                                focusedErrorBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  borderSide: BorderSide(
                                    color: Colors.red,
                                    width: 2.0,
                                  ),
                                ),
                                errorStyle: TextStyle(color: Colors.red),
                                hintText: "Ingresa usuario",
                                prefixIcon: IconButton(
                                    icon: Icon(
                                      Icons.person,
                                      color: Colors.black54,
                                    ),
                                    onPressed: () {
                                      if (_formController.currentState!
                                          .validate()) {
                                        //_getFriendsSearch();
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                      } else {
                                        print("Not ready for search");
                                      }
                                    })),
                            onFieldSubmitted: (String value) {
                              if (_formController.currentState!.validate()) {
                                //_getFriendsSearch();
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                              }
                            },
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "* Debes ingresar un usuario";
                              }
                              if (value.length < 3) {
                                return "* Intenta con mínimo 3 caracteres";
                              }

                              return null;
                            },
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Row(
                            children: [
                              Text(
                                "Constraseña",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 14,
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          TextFormField(
                            controller: _passController,
                            obscureText: true,
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                enabledBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  borderSide: const BorderSide(
                                      color: Color.fromRGBO(78, 124, 176, 1),
                                      width: 1.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  borderSide: BorderSide(
                                    color: Color.fromRGBO(78, 124, 176, 1),
                                    width: 1.0,
                                  ),
                                ),
                                errorBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  borderSide: BorderSide(
                                    width: 1.0,
                                  ),
                                ),
                                focusedErrorBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  borderSide: BorderSide(
                                    color: Colors.red,
                                    width: 2.0,
                                  ),
                                ),
                                errorStyle: TextStyle(color: Colors.red),
                                hintText: "Ingresa Contraseña",
                                prefixIcon: IconButton(
                                    icon: Icon(
                                      Icons.verified_user,
                                      color: Colors.black54,
                                    ),
                                    onPressed: () {
                                      if (_formController.currentState!
                                          .validate()) {
                                        //_getFriendsSearch();
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                      } else {
                                        print("Not ready for search");
                                      }
                                    })),
                            onFieldSubmitted: (String value) {
                              if (_formController.currentState!.validate()) {
                                //_getFriendsSearch();
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                              }
                            },
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "* Debes ingresar una contraseña";
                              }
                              if (value.length < 6) {
                                return "* Intenta con mínimo 6 caracteres";
                              }

                              return null;
                            },
                          ),
                        ])),
                    SizedBox(
                      height: 15.0,
                    ),
                  ])),
              SizedBox(
                height: 15.0,
              ),
              _isLoading
                  ? Column(
                      children: [
                        SizedBox(
                          height: 5.0,
                        ),
                        CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text("Cargando..."),
                      ],
                    )
                  : Row(),
              _showMessage
                  ? Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("* " + _message),
                        ),
                      ],
                    )
                  : Row(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15.0, vertical: 10.0),
                      child: FlatButton(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 15.0),
                          color: Color.fromRGBO(78, 124, 176, 1),
                          onPressed: () {
                            if (_formController.currentState!.validate()) {
                              print("VALIDA");
                              if (_isLoading == false) {
                                _makeLogin();
                              } else {
                                print("Already sending login");
                              }
                            } else {
                              print("NO VÁLIDO");
                              setState(() {
                                //_formMessage = "Te falta rellenar algo, revísalo";
                              });
                            }
                          },
                          child: Text(
                            'Entrar'.toUpperCase(),
                            style:
                                TextStyle(color: Colors.white, fontSize: 16.0),
                          )),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 15.0,
              ),
              Text(
                "ó",
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 13.0,
                    fontStyle: FontStyle.italic),
              ),
              SizedBox(
                height: 15.0,
              ),
              GestureDetector(
                  onTap: () {
                    print("Register...");
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RegisterUser()));
                  },
                  child: Text(
                    "¿No tienes una cuenta? Regístrate",
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 13.0,
                        fontStyle: FontStyle.italic),
                  ))
            ]),
          ),
        ),
      ),
    );
  }
}

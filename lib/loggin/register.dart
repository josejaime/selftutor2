import 'package:flutter/material.dart';
import '../pages/home.dart';
import '../utils/globals.dart' as globals;
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crypto/crypto.dart';

class RegisterUser extends StatefulWidget {
  @override
  _RegisterUserState createState() => _RegisterUserState();
}

class _RegisterUserState extends State<RegisterUser> {
  final _formController = GlobalKey<FormState>();
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _prepaNameController = TextEditingController();
  TextEditingController _prepaScoreController = TextEditingController();
  TextEditingController _passController = TextEditingController();
  String _formMessage = "";
  late Timer timer;

  Career _currentCareer =
      Career(id: -1, name: "Selecciona una carrera", admission: "123");
  String hint = "Selecciona una tienda";

  //
  List _data = List.empty(growable: true);
  List<Career> listOfCareer = List<Career>.empty(growable: true);
  //
  bool _isLoading = false;

  Future<String> _getCareers() async {
    // lets call the url
    String url = globals.baseUrl + "careers";
    print("empezando llamada");
    print(url);

    var response =
        await http.get(Uri.parse(url)).then((http.Response response) {
      setState(() {
        //_showMessage = false;
        //_message = "";
      });
      //==================================
      // get the Status Code
      // these part may be useful later
      //==================================
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        setState(() {
          //_showMessage = true;
          //_message = "Hubo un error con el servidor";
          print(
              "Lo sentimos, hubo un error al procesar tu búsqueda, intenta más tarde STATUS CODE $statusCode");
        });
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var _result = json.decode(response.body);
          _data = _result;

          if (_data.length == 0) {
            print('La búsqueda no generó resultados');
          } else {
            print("Here nothing wrong");
            //here earese the circle
          }
          print("El tamaño de los datos: ${_data.length}");
          print(_data[0]);

          final items = _result.cast<Map<String, dynamic>>();

          listOfCareer = items.map<Career>((json) {
            return Career.fromJson(json);
          }).toList();

          _currentCareer = listOfCareer[0];
        });
      }
    });

    return response;
  }

  _registerUser() async {
    setState(() {
      _isLoading = true;
      _formMessage = "";
    });

    String url = globals.baseUrl + "signup";

    var body = new Map<String, dynamic>();

    body['username'] = _usernameController.text;
    body['first_name'] = _nameController.text;
    body['last_name'] = _lastNameController.text;
    body['email'] = _emailController.text;
    body['avatar'] = "0";
    body['prepa_name'] = _prepaNameController.text;
    body['prepa_score'] = _prepaScoreController.text;
    body['career_id'] = _currentCareer.id.toString();

    // MAKE THE PASS
    var bytes = utf8.encode(_passController.text); // data being hashed
    var digest = sha512.convert(bytes);
    body['password'] = digest.toString();

    print("Digest as bytes: ${digest.bytes}");
    print("Digest as hex string: $digest");

    print("body to post:");
    print(body);

    http.post(Uri.parse(url), body: body).then((http.Response response) async {
      final int statusCode = response.statusCode;
      print(statusCode);
      print("The result is = " + response.body);
      var result = json.decode(response.body);
      //int code = int.tryParse(result[0]);
      int code = result[0] ?? 1;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        setState(() {
          _isLoading = false;
        });
        throw new Exception("Error while fetching data");
      }

      if (code != null && code < 0) {
        // Error

        setState(() {
          print("THERE WAS AN ERROR CODE $code");
          _isLoading = false;
        });

        showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Text('Hubo un problema con el registro'),
                content: Text(result[2]),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () => Navigator.pop(context),
                    child: Text('Aceptar'),
                  ),
                ],
              ),
            ) ??
            false;
      } else {
        print("REGISTED USER SUCCESS");
        var profile = result;
        print(profile);
        print("USERNAME = " + profile['username']);
        SharedPreferences preferences = await SharedPreferences.getInstance();
        await preferences.setString("apiToken", profile['api_token']);
        await preferences.setBool(
            'loged', true); // LETS GET TO THE LOGED ALREADY
        await preferences.setInt(
            'userId', profile['id']); // LETS GET ALSO THE ID

        Timer(Duration(milliseconds: 600), () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Home()));
        });
        setState(() {
          _isLoading = false;
        });
      }
      print("CODE ${code.toString()}");
      print(response.body);
      //Navigator.push(context, MaterialPageRoute( builder: (context) => Home()));
      //return User.fromJson(json.decode(response.body));

      return response;
    });
  }

  @override
  void initState() {
    _getCareers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Center(
        child: SingleChildScrollView(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Container(
              height: 125,
              width: 155,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: ExactAssetImage('images/logo.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            Container(
                width: MediaQuery.of(context).size.width * 0.85,
                padding: EdgeInsets.all(25.0),
                decoration: BoxDecoration(
                    color: Colors.grey.withAlpha(50),
                    borderRadius: BorderRadius.circular(5)),
                child: Column(children: [
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    "Registrate".toUpperCase(),
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    "Y comienza a aprender",
                    style: TextStyle(
                        color: Colors.white30,
                        fontSize: 12.0,
                        fontWeight: FontWeight.w400),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  _registerForm(context),
                  SizedBox(
                    height: 25.0,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(_formMessage),
                    ),
                  ),
                ])),
            SizedBox(
              height: 15.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15.0, vertical: 10.0),
                    child: FlatButton(
                        padding: EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 15.0),
                        color: _isLoading
                            ? Theme.of(context).primaryColor
                            : Color.fromRGBO(78, 124, 176, 1),
                        onPressed: () {
                          if (_formController.currentState!.validate()) {
                            print("VALIDA");
                            if (_isLoading == false) {
                              _registerUser();
                            } else {
                              print("Already Loading register form");
                            }
                          } else {
                            print("NO VÁLIDO");
                            setState(() {
                              _formMessage = "Te falta rellenar algo, revísalo";
                            });
                          }
                        },
                        child: Text(
                          _isLoading
                              ? '...Cargando...'.toUpperCase()
                              : 'Regístrate'.toUpperCase(),
                          style: TextStyle(color: Colors.white, fontSize: 16.0),
                        )),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 15.0,
            ),
            /*GestureDetector(
                onTap: (){
                  print("Register...");
                  Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterUser()));
                },
                child: Text("¿No tienes una cuenta? Regístrate", style: TextStyle( color: Colors.grey, fontSize: 12.0, fontStyle: FontStyle.italic),)
              )*/
          ]),
        ),
      ),
    ));
  }

  Widget _registerForm(BuildContext context) {
    return Form(
        key: _formController,
        child: Column(children: [
          Row(
            children: [
              Text(
                "Usuario",
                textAlign: TextAlign.left,
                style: TextStyle(color: Colors.white54),
              ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          TextFormField(
            controller: _usernameController,
            decoration: _myInputsDecoration("Ingresa un nombre de usuario"),
            validator: (value) {
              if (value!.isEmpty) {
                return "* Debes ingresar un nombre de usuario";
              }
              if (value.length < 3) {
                return "* Intenta con mínimo 3 caracteres";
              }
              Pattern pattern =
                  r'^(?=[a-zA-Z0-9._]{4,14}$)(?!.*[_.]{2})[^_.].*[^_.]$';
              RegExp regex = RegExp(pattern.toString());

              if (!regex.hasMatch(value)) {
                return "Asegurate de colocar un username válido";
              }
              return null;
            },
          ),
          _labelDivider(context, "Nombre"),
          TextFormField(
            controller: _nameController,
            keyboardType: TextInputType.name,
            decoration: _myInputsDecoration("Ingresa tu nombre"),
            validator: (value) {
              if (value!.isEmpty) {
                return "* Debes ingresar tu nombre";
              }
              if (value.length < 3) {
                return "* Intenta con mínimo 3 caracteres";
              }
              return null;
            },
          ),
          _labelDivider(context, "Apellidos"),
          TextFormField(
            controller: _lastNameController,
            keyboardType: TextInputType.name,
            decoration: _myInputsDecoration("Ingresa tus apellidos"),
            validator: (value) {
              if (value!.isEmpty) {
                return "* Debes ingresar tus apellidos";
              }
              if (value.length < 3) {
                return "* Intenta con mínimo 3 caracteres";
              }
              return null;
            },
          ),
          _labelDivider(context, "Email"),
          TextFormField(
            controller: _emailController,
            keyboardType: TextInputType.emailAddress,
            decoration: _myInputsDecoration("Ingresa tu email"),
            validator: (value) {
              if (value!.isEmpty) {
                return "* Debes ingresar tu email";
              }
              Pattern pattern =
                  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
              RegExp regex = new RegExp(pattern.toString());
              if (!regex.hasMatch(value)) {
                return "* Asegurate de colocar un email válido";
              }
              return null;
            },
          ),
          _labelDivider(context, "Preparatoria"),
          TextFormField(
            controller: _prepaNameController,
            decoration: _myInputsDecoration("Nombre de tu preparatoria"),
            validator: (value) {
              if (value!.isEmpty) {
                return "* Debes ingresar el nombre de tu prepa";
              }
              if (value.length < 3) {
                return "* Intenta con mínimo 3 caracteres";
              }
              return null;
            },
          ),
          _labelDivider(context, "Promedio"),
          TextFormField(
            controller: _prepaScoreController,
            keyboardType: TextInputType.number,
            decoration: _myInputsDecoration("Tu promedio actual"),
            validator: (value) {
              if (value!.isEmpty) {
                return "* Debes ingresar tu promedio";
              }
              print("TRYING TO PARSE INT ");
              print(int.tryParse(value));
              if (int.tryParse(value) == null) {
                return "Debes ingresar un número entero";
              }
              int valueScore = int.parse(value);
              if (valueScore < 0 || valueScore > 100) {
                return "Intenta con un número entre 0 y 100";
              }

              return null;
            },
          ),
          _labelDivider(context, "Selecciona tu carrera deseada"),
          listOfCareer != null
              ? DropdownButtonFormField<Career>(
                  items: listOfCareer
                      .map((career) => DropdownMenuItem(
                            child: Text("${career.name}"),
                            value: career,
                          ))
                      .toList(),
                  onChanged: (Career? newValue) {
                    setState(() {
                      _currentCareer = newValue as Career;
                    });
                  },
                  decoration: _myInputsDecoration("Carrera"),
                  hint: Text("Selecciona tu carrera deseada"),
                  isExpanded: true,
                  value: _currentCareer,
                )
              : Text('Cargando Carreras....'),
          _labelDivider(context, "Contraseña"),
          TextFormField(
            controller: _passController,
            obscureText: true,
            decoration: _myInputsDecoration("Contraseña"),
            validator: (value) {
              if (value!.isEmpty) {
                return "* Debes ingresar una contraseña";
              }
              if (value.length < 6) {
                return "* Intenta con mínimo 6 caracteres";
              }
              if (value.length > 8) {
                return "* Intenta con menos de 6 caracteres";
              }
              return null;
            },
          ),
        ]));
  }

  InputDecoration _myInputsDecoration(String hint) {
    return InputDecoration(
      filled: true,
      isDense: true,
      fillColor: Colors.white,
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      enabledBorder: const OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        borderSide: const BorderSide(
            color: Color.fromRGBO(78, 124, 176, 1), width: 1.0),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        borderSide: BorderSide(
          color: Color.fromRGBO(78, 124, 176, 1),
          width: 1.0,
        ),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        borderSide: BorderSide(
          width: 1.0,
        ),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        borderSide: BorderSide(
          color: Colors.red,
          width: 2.0,
        ),
      ),
      errorStyle: TextStyle(color: Colors.red, fontSize: 14.0),
      hintText: hint,
    );
  }

  Widget _labelDivider(BuildContext context, String text) {
    return Column(
      children: [
        SizedBox(
          height: 8.0,
        ),
        Row(
          children: [
            Text(text,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.white54,
                ))
          ],
        ),
        SizedBox(
          height: 10.0,
        ),
      ],
    );
  }
}

class Career {
  int id;
  String name;
  String admission;

  Career({required this.id, required this.name, required this.admission});

  factory Career.fromJson(Map<String, dynamic> json) {
    return Career(
      id: json['id'],
      name: json['name'],
      admission: json['admission'].toString(),
    );
  }
}

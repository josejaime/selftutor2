import 'package:flutter/material.dart';
import '../friends/pending_friends.dart';
import '../helpers/select_avatar.dart';
import '../loggin/logout.dart';
import '../pages/achievements.dart';
import '../pages/log_quizzes.dart';
import '../pages/packages.dart';
import '../pages/profile.dart';
import '../pages/soon.dart';
import '../pages/subjects.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyCustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: ExactAssetImage('images/logo.png'),
                ),
                //color: Color.fromRGBO(125, 167, 217, 1),
                color: Colors.white70),
            accountName: Text(
              "",
              style: TextStyle(fontWeight: FontWeight.w800),
            ),
            accountEmail: Text(""),
          ),
          ListTile(
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Subjects()));
            },
            title: Text("Asignaturas"),
            leading: Icon(Icons.assignment),
          ),
          ListTile(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => UsersAchievements()));
            },
            title: Text("Logros"),
            leading: Icon(Icons.check),
          ),
          ListTile(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LogQuizzes()));
            },
            title: Text("Quizes Realizados"),
            leading: Icon(Icons.dashboard),
          ),
          ListTile(
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Soon()));
            },
            title: Text("Retos"),
            leading: Icon(Icons.golf_course),
          ),
          ListTile(
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Soon()));
            },
            title: Text("Amigos"),
            leading: Icon(Icons.people),
          ),
          ListTile(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => UserProfile()));
            },
            title: Text("Perfil"),
            leading: Icon(Icons.person),
          ),
          ListTile(
            onTap: () {
              Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SelectAvatar()))
                  .then((value) async {
                if (value != null) {
                  SharedPreferences preferences =
                      await SharedPreferences.getInstance();
                  preferences.setString('avatar', value.toString());
                }
              });
            },
            title: Text("Selecciona Avatar"),
            leading: Icon(Icons.edit),
          ),
          ListTile(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PendingFriends()));
            },
            title: Text("Solicitudes de amistad"),
            leading: Icon(Icons.group_add),
          ),
          ListTile(
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Packages()));
            },
            title: Text("Paquetes"),
            leading: Icon(Icons.check_box_outline_blank),
          ),
          Divider(),
          ListTile(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LogoutUser()));
            },
            title: Text("Salir"),
            leading: Icon(Icons.exit_to_app),
          ),
        ],
      ),
    );
  }
}

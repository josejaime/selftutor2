import 'package:flutter/material.dart';

class MyCustomAppBar extends AppBar {
  MyCustomAppBar({Key? key, Widget? title, actions})
      : super(
            key: key,
            title: title,
            centerTitle: true,
            elevation: 3.0,
            actions: actions);
}

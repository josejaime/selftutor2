import 'package:flutter/material.dart';

class LoadingMessage extends StatelessWidget {
  final String message;

  LoadingMessage({
    required this.message,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CircularProgressIndicator(
          backgroundColor: Colors.white,
        ),
        SizedBox(
          height: 25.0,
        ),
        Text(message),
      ],
    );
  }
}

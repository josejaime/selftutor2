import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

// Globals
import '../utils/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

class BattleEnd extends StatefulWidget {
  //Constructor
  final int score;
  final int totalQuestions;
  final int contenderId;
  final int challengeId;
  final bool response;

  BattleEnd({
    required this.score,
    required this.totalQuestions,
    required this.contenderId,
    required this.challengeId,
    required this.response,
  });

  @override
  _BattleEndState createState() => _BattleEndState();
}

class _BattleEndState extends State<BattleEnd> {
  String _message = "";
  bool _showMessage = false;
  int finalScore = 0;
  late double valueScore;

  @override
  void initState() {
    finalScore = (widget.score * 100 / widget.totalQuestions).truncate();
    valueScore = widget.score * 100 / widget.totalQuestions / 100;
    _postScore();
    super.initState();
    //double temp = (widget.score * 100 / widget.totalQuestions).truncate();
  }

  //====================================================
  //
  // Lets make the score post for the user
  //
  //====================================================
  Future<bool> _postScore() async {
    String urlPostScore = globals.baseUrl + "challenge/log";

    setState(() {
      _message = "";
      _showMessage = false;
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();

    int userId = preferences.getInt('userId') as int;
    String token = preferences.getString('apiToken') as String;
    var body = new Map<String, dynamic>();

    if (widget.response) {
      body['user_id'] =
          widget.contenderId.toString(); // Who started the challenge
      body['friend_id'] = userId.toString(); // The challenged one
      body['score'] = finalScore.toString(); // Actual score
      body['challenge_id'] = widget.challengeId.toString();
    } else {
      body['user_id'] = userId.toString(); // Who started the challenge
      body['friend_id'] = widget.contenderId.toString(); // The challenged one
      body['score'] = finalScore.toString(); // Actual score
      body['challenge_id'] = widget.challengeId.toString();
    }

    print(body);

    Uri uriPostScore = Uri.parse(urlPostScore);

    http.post(uriPostScore, body: body, headers: {'api-token': token}).then(
        (http.Response response) async {
      final int statusCode = response.statusCode;
      print("Intento de Postear SCORE = ${response.body}");
      print("Status del Server $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        throw new Exception("Error while fetching data");
      }
      if (response.body != "-1") {
        print("Fue true");

        print(response.body);
        setState(() {
          _showMessage = true;
        });
      } else {
        print("Fue falso");
        setState(() {
          _showMessage = true;
          _message = "Intento fallido de registro";
        });
      }
    });
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(25.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "¡Felicidades completaste el Reto!".toUpperCase(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 21.0,
                      color: Colors.white),
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              /*Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  padding: EdgeInsets.all(75.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Column(
                    children: <Widget>[
                      Text("Calificación", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 21.0, color: Colors.black)),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        //child: Text("${widget.score}".toUpperCase(), textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 28.0, color: Colors.black),),
                        child: Text("$finalScore", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 38.0, color: Colors.black),),
                      ),
                    ],
                  )
                ),
              ),*/
              Container(
                  width: MediaQuery.of(context).size.width * 0.65,
                  padding: EdgeInsets.all(55.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Column(
                    children: <Widget>[
                      Text("Puntos",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 27.0,
                              color: Colors.black)),
                      SizedBox(
                        height: 20.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              SizedBox(
                                width: 115,
                                height: 115,
                                child: CircularProgressIndicator(
                                  value: valueScore,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Color.fromRGBO(78, 124, 176, 1)),
                                  backgroundColor: Colors.black12,
                                  strokeWidth: 7.5,
                                ),
                              ),
                              Container(
                                width: 115,
                                height: 115,
                                child: Center(
                                  child: Text(finalScore.toString(),
                                      style: TextStyle(
                                          fontSize: 37,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w600)),
                                ),
                              ),
                            ],
                          ),
                        ],
                      )
                    ],
                  )),
              SizedBox(
                height: 15.0,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.75,
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      /*Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border(
                              right: BorderSide( color: Colors.white38, style: BorderStyle.solid ),
                            ),
                          ),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Text("${widget.quizId}/6", style: TextStyle(fontSize: 26.0)),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric( horizontal: 10.0, vertical: 5.0),
                                child: Text("Módulos terminados", textAlign: TextAlign.center, style: TextStyle( color: Colors.white70),),
                              )
                            ],
                          ),
                        ),
                      ),*/
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Text(
                                  "${widget.score}/${widget.totalQuestions}",
                                  style: TextStyle(fontSize: 26.0)),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 5.0),
                              child: Text(
                                "Aciertos",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white70),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              /*Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("${widget.score}".toUpperCase(), textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 28.0, color: Colors.white),),
              ),*/
              Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15.0),
                  child: Text("Intenta mejorar tu score en cada oportunidad",
                      textAlign: TextAlign.center)),
              SizedBox(
                height: 10.0,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.75,
                child: FlatButton(
                  color: Color.fromRGBO(78, 124, 176, 1),
                  padding: EdgeInsets.symmetric(
                    horizontal: 10.0,
                    vertical: 15.0,
                  ),
                  onPressed: () async {
                    print("END");
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        'home', (Route<dynamic> route) => false);
                  },
                  child: Text(
                    "Regresar al Inicio",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 16.0),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

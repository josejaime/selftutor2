import 'package:flutter/material.dart';
import '../components/myappbar.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
// Globals
import '../utils/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

import 'friend_profile.dart';

class FriendSearch extends StatefulWidget {
  @override
  _FriendSearchState createState() => _FriendSearchState();
}

class _FriendSearchState extends State<FriendSearch> {
  final _formController = GlobalKey<FormState>();
  final _inputController = TextEditingController();
  bool _isLoading = false;
  bool _error = false;
  String _message = "";
  String _url = "";

  List data = List.empty(growable: true);

  // Get the books function
  Future<String> _getFriendsSearch() async {
    setState(() {
      _isLoading = true;
      _error = false;
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString('apiToken') as String;

    _url = globals.baseUrl + "users/search";
    print(_inputController.text);
    print(_url);

    var body = new Map<String, dynamic>();
    body['search'] = _inputController.text;
    Uri uri = Uri.parse(_url);

    http.post(uri, body: body, headers: {'api-token': token}).then(
        (http.Response response) async {
      final int statusCode = response.statusCode;
      print("Intento de Postear = ${body.toString()}");
      print("Status del Server $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        setState(() {
          _isLoading = false;
          _error = true;
          _message = "Hubo un error al procesar la búsqueda";
        });
        throw new Exception("Error while fetching data");
      } else {
        // The server was reached and responded
        setState(() {
          _isLoading = false;
          data = json.decode(response.body);
        });
        print(response.body);
      }
    });

    return "success";
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(
        title: Text("Busca Amigos"),
      ),
      body: Center(
        child: Column(children: [
          Form(
            key: _formController,
            child: TextFormField(
              controller: _inputController,
              decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(0.0)),
                    borderSide: const BorderSide(
                        color: Color.fromRGBO(78, 124, 176, 1), width: 1.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(0.0)),
                    borderSide: BorderSide(
                      color: Colors.black,
                      width: 1.0,
                    ),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    borderSide: BorderSide(
                      width: 1.0,
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    borderSide: BorderSide(
                      color: Colors.red,
                      width: 2.0,
                    ),
                  ),
                  errorStyle: TextStyle(color: Color.fromRGBO(78, 124, 176, 1)),
                  hintText: "Busca un amigo",
                  suffixIcon: IconButton(
                      icon: Icon(
                        Icons.search,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        if (_formController.currentState!.validate()) {
                          _getFriendsSearch();
                          FocusScope.of(context).requestFocus(FocusNode());
                        } else {
                          print("Not ready for search");
                        }
                      })),
              onFieldSubmitted: (String value) {
                if (_formController.currentState!.validate()) {
                  _getFriendsSearch();
                  FocusScope.of(context).requestFocus(FocusNode());
                } else {
                  print("Not ready for search");
                }
              },
              validator: (value) {
                if (value!.isEmpty) {
                  return "* Debes ingresar una búsqueda";
                }
                if (value.length < 3) {
                  return "* Intenta con mínimo 3 caracteres";
                }

                return null;
              },
            ),
          ),
          _error
              ? Row(
                  children: [Text(_message)],
                )
              : Row(),
          _isLoading
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 25.0,
                        ),
                        CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        Text("Cargando resultados..."),
                      ],
                    )
                  ],
                )
              : Flexible(
                  child: ListView.builder(
                      itemCount: data == null ? 0 : data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                          elevation: 8.0,
                          margin: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 6.0),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                            ),
                            child: ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 0.0),
                              leading: Container(
                                  padding: EdgeInsets.only(right: 12.0),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          right: BorderSide(
                                              width: 1.0,
                                              color: Colors.white24))),
                                  child: Image(
                                    image: AssetImage(
                                        'images/${data[index]['avatar']}.png'),
                                    //image: AssetImage('images/avatar_'+"0"+'.png'),
                                  )),
                              title: Text(
                                "${data[index]['username']} | ${data[index]['first_name']}",
                                style: TextStyle(color: Colors.white),
                              ),
                              subtitle: Row(
                                children: <Widget>[
                                  Column(
                                    children: [
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(9)),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 3.0,
                                                horizontal: 12.0),
                                            child: Text(
                                              "${data[index]['global_score']}",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w700),
                                            ),
                                          )),
                                    ],
                                  ),
                                ],
                              ),
                              trailing: Image(
                                image: AssetImage(
                                    'images/${data[index]['badge']['icon']}'),
                                width: 35.0,
                              ),
                              onTap: () {
                                int userId = -1;
                                data[index]['id'] is int
                                    ? userId = data[index]['id']
                                    : int.parse(data[index]['id']);

                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => FriendsProfile(
                                              userId: userId,
                                            )));
                              },
                            ),
                          ),
                        );
                      }))
        ]),
      ),
    );
  }
}

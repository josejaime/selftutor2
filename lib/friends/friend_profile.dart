import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../components/myappbar.dart';
// HTTP
import 'package:http/http.dart' as http;
import '../friends/battle_instructions.dart';
import 'dart:async';
import 'dart:convert';

// Globals
import '../utils/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

class FriendsProfile extends StatefulWidget {
  final int userId;

  FriendsProfile({
    required this.userId,
  });

  @override
  _FriendsProfileState createState() => _FriendsProfileState();
}

class _FriendsProfileState extends State<FriendsProfile> {
  bool _isLoading = true;
  bool _visibleError = false;
  bool _isWaiting = false;
  String _message = "";
  var _profile;
  late String profileImage;
  late String _userAvatar;
  List<int> _friends = List<int>.empty(growable: true);
  List<int> _friendsPending = List<int>.empty(growable: true);
  List<int> _ourFriendsPending = List<int>.empty(growable: true);
  List _history = List.empty(growable: true);
  int _friendStatus = 0; // 0 means no friends

  /*========================================================*/
  /* Lets call the profile from the server                  */
  /*                                                        */
  /*========================================================*/
  Future<String> _getFriend() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userAvatar = preferences.getString('avatar') as String;
    _userAvatar = "$userAvatar.png";
    String token = preferences.getString('apiToken') as String;

    String url = globals.baseUrl;
    url = url + "user/${widget.userId}";

    Uri uri = Uri.parse(url);

    await http.get(uri, headers: {'api-token': token}).then(
        (http.Response response) async {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        throw new Exception("Error while fetching data");
      } else {
        //==================================================
        // Lets get the preferences
        //==================================================
        SharedPreferences preferences = await SharedPreferences.getInstance();
        int userId = preferences.getInt('userId') as int;
        //==================================================
        // An retrive the both list
        //==================================================
        List<String> myList = (preferences.getStringList('friends') ??
            List.empty(growable: true));
        _friends = myList.map((e) => int.parse(e)).toList();
        print("Current ID Friends");
        print(_friends.toString());
        //==================================================
        // Now the pending friends request
        //==================================================
        List<String> myList2 = (preferences.getStringList('friendsPending') ??
            List.empty(growable: true));
        _friendsPending = myList2.map((e) => int.parse(e)).toList();
        print("Current ID Pending Friends");
        print(_friendsPending.toString());
        //==================================================
        // Now our sent friends request
        //==================================================
        List<String> myList3 =
            (preferences.getStringList('ourFriendsPending') ??
                List.empty(growable: true));
        _ourFriendsPending = myList3.map((e) => int.parse(e)).toList();
        print("OUR Current ID Pending Friends");
        print(_ourFriendsPending.toString());

        //==================================================
        // GET the history challenges whit this user
        //==================================================
        url = globals.baseUrl;
        url = url + "challenges/$userId/historywith/${widget.userId}/";
        Uri uri = Uri.parse(url);

        await http.get(uri, headers: {'api-token': token}).then(
            (http.Response response) async {
          final int statusCode = response.statusCode;
          print("status history challenge code : $statusCode");

          if (statusCode < 200 || statusCode > 400) {
            print("error doing the call");
            throw new Exception("Error while fetching data");
          } else {
            setState(() {
              var result = json.decode(response.body);
              print("History result ");
              print(result);

              if (result.length > 0) {
                if (result[0] != -1) {
                  _history = result;

                  for (var i in _history) {
                    if (i['friend_score'] == -1) {
                      _isWaiting = true;
                    }
                  }
                }
              }
              //print(_history[0]['my_score']);
            });
          }
        });

        setState(() {
          var result = json.decode(response.body);
          _profile = result;
          _isLoading = false;

          print("USER PROFILE ID ${_profile['id']}");

          if (_friends.contains(_profile['id'])) {
            _friendStatus = 1; // already friends
            print("FRIEND STATUS 1");
          }
          if (_friendsPending.contains(_profile['id'])) {
            _friendStatus = 2; // Pending friend
            print("FRIEND STATUS 2");
          }
          if (_ourFriendsPending.contains(_profile['id'])) {
            _friendStatus = 3; // OUR Pending friend - WAITING RESPONSE
            print("FRIEND STATUS 3");
          }
          //IF STAYS IN CERO WE ARE NO FRIENDS

          print(_profile['username']);
        });
      }
    });

    return "success";
  }

  /*========================================================*/
  /* Remove a friend */
  /*========================================================*/
  Future<String> _removeFriend() async {
    setState(() {
      _isLoading = true;
      _visibleError = false;
      _message = "";
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();
    int userId = preferences.getInt('userId') as int;
    String token = preferences.getString('apiToken') as String;
    Map body = Map<String, dynamic>();
    body['friend_id'] = widget.userId.toString();

    String url = globals.baseUrl;
    url = url + "user/$userId/removefriend";

    Uri uri = Uri.parse(url);

    await http.post(uri, body: body, headers: {'api-token': token}).then(
        (http.Response response) async {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        throw new Exception("Error while fetching data");
      } else {
        //====================================================
        // Here there is no need to check for status,
        // just remove it
        //====================================================
        List<String> myList = (preferences.getStringList('friends') ??
            List.empty(growable: true));
        _friends = myList.map((e) => int.parse(e)).toList();
        //====================================================
        // Lets remove our friend from the preferences
        //====================================================
        _friends.remove(widget.userId);
        //====================================================
        // Lets save the new list whit out our friend
        //====================================================
        List<String> myListOfStrings =
            _friends.map((i) => i.toString()).toList();
        preferences.setStringList('friends', myListOfStrings);

        print("Amigos Restantes");
        print(_friends.toString());

        setState(() {
          print(response.body);
          //Now we are not friends anymore
          _friendStatus = 0;
          _isLoading = false;
        });
      }
    });

    return "success";
  }

  /*========================================================*/
  /* Remove a friend */
  /*========================================================*/
  Future<String> _addFriend() async {
    /* MAKE SOMETHING TO MARK AS PENDENT*/

    setState(() {
      _isLoading = true;
      _visibleError = false;
      _message = "";
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();
    int userId = preferences.getInt('userId') as int;
    String token = preferences.getString('apiToken') as String;
    String url = globals.baseUrl + "user/$userId/addfriend";
    Map body = Map<String, dynamic>();
    body['friend_id'] = widget.userId.toString();

    print('url = $url');
    print('body = $body');

    Uri uri = Uri.parse(url);

    await http.post(uri, body: body, headers: {'api-token': token}).then(
        (http.Response response) async {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        throw new Exception("Error while fetching data");
      } else {
        print("THE Response addFriend = ${response.body}");

        if (int.parse(response.body) == 0) {
          // Fail or already a friend Request,
          // here you should never get because it woudn't be a button to access
          print("FAIL or Already a Friend REQUEST");
        } else if (int.parse(response.body) == 1) {
          // The friend request was made, that means you were not friends
          // you send it
          // and now you are waiting

          //==================================================
          // Now our sent request lets remote it from there
          // if neccesary
          //==================================================
          List<String> myList3 =
              (preferences.getStringList('ourFriendsPending') ??
                  List.empty(growable: true));
          _ourFriendsPending = myList3.map((e) => int.parse(e)).toList();
          _ourFriendsPending.add(widget.userId);
          //====================================================
          // Lets save the new pending list whit out our friend
          //====================================================
          List<String> myListOfStrings3 =
              _ourFriendsPending.map((i) => i.toString()).toList();
          preferences.setStringList('ourFriendsPending', myListOfStrings3);

          setState(() {
            _friendStatus = 3;
          });
        } else if (int.parse(response.body) == 2) {
          // There was a friend request already from the other person
          // so you are now full friends

          //====================================================
          // Lets add our friend to the preferences
          //====================================================
          List<String> myList = (preferences.getStringList('friends') ??
              List.empty(growable: true));
          _friends = myList.map((e) => int.parse(e)).toList();
          _friends.add(widget.userId);
          //====================================================
          // Lets save the new list whit our new friend
          //====================================================
          List<String> myListOfStrings =
              _friends.map((i) => i.toString()).toList();
          preferences.setStringList('friends', myListOfStrings);

          //====================================================
          // Lets remove the item from the pending if exist
          // other way it wont do anything
          //====================================================
          List<String> myList2 = (preferences.getStringList('friendsPending') ??
              List.empty(growable: true));
          _friendsPending = myList2.map((e) => int.parse(e)).toList();
          //====================================================
          // Lets remove our friend from the preferences
          //====================================================
          _friendsPending.remove(widget.userId);
          //====================================================
          // Lets save the new pending list whit out our friend
          //====================================================
          List<String> myListOfStrings2 =
              _friendsPending.map((i) => i.toString()).toList();
          preferences.setStringList('friendsPending', myListOfStrings2);

          //==================================================
          // Now our sent request lets remote it from there
          // if neccesary
          //==================================================
          List<String> myList3 =
              (preferences.getStringList('ourFriendsPending') ??
                  List.empty(growable: true));
          _ourFriendsPending = myList3.map((e) => int.parse(e)).toList();
          _ourFriendsPending.remove(widget.userId);
          //====================================================
          // Lets save the new pending list whit out our friend
          //====================================================
          List<String> myListOfStrings3 =
              _ourFriendsPending.map((i) => i.toString()).toList();
          preferences.setStringList('ourFriendsPending', myListOfStrings3);

          setState(() {
            _friendStatus = 1; // We are friends yeah
          });
        }

        setState(() {
          _isLoading = false;
        });
      }
    });

    return "success";
  }

  @override
  void initState() {
    _getFriend();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(),
      body: _isLoading
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(
                    backgroundColor: Colors.white,
                  ),
                  SizedBox(
                    height: 25.0,
                  ),
                  Text('Estamos cargando...'),
                ],
              ),
            )
          : _visibleError
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(_message),
                    ],
                  ),
                )
              : _profile != null
                  ? SingleChildScrollView(
                      child: Center(
                        child: Column(
                          children: [
                            Material(
                              elevation: 8.0,
                              child: Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.2,
                                decoration: BoxDecoration(
                                  color: Color.fromRGBO(56, 90, 123, 1),
                                ),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                        flex: 1,
                                        child: Center(
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(50.0),
                                            child: Image(
                                              //image: AssetImage('images/avatar_' + _profile['avatar'] + '.png'),
                                              image: AssetImage(
                                                  'images/${_profile['avatar']}.png'),
                                              width: 120.0,
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                        )),
                                    Expanded(
                                        child: Material(
                                      color: Colors.white,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            _profile["first_name"] +
                                                ' ' +
                                                _profile["last_name"],
                                            style: TextStyle(
                                                fontSize: 17.0,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.black),
                                          ),
                                          SizedBox(
                                            height: 5.0,
                                          ),
                                          Container(
                                              padding: EdgeInsets.all(8.0),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      25, 48, 80, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(6.5),
                                                child: Text(
                                                  'Score: ${_profile['global_score']}',
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                              )),
                                        ],
                                      ),
                                    )),
                                  ],
                                ),
                              ),
                            ),
                            Column(
                              children: [
                                SizedBox(
                                  height: 5.0,
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            8.0, 12.0, 8.0, 8.0),
                                        child: Card(
                                            elevation: 1.0,
                                            child: Column(
                                              children: <Widget>[
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 8.0,
                                                      vertical: 15.0),
                                                  child: Column(
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.person,
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColor,
                                                            size: 30.0,
                                                          ),
                                                          Expanded(
                                                              child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 10.0),
                                                            child: Text(
                                                              _profile[
                                                                  "username"],
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .bodyText1,
                                                            ),
                                                          ))
                                                        ],
                                                      ),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.mail,
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColor,
                                                            size: 30.0,
                                                          ),
                                                          Expanded(
                                                              child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 10.0),
                                                            child: Text(
                                                              _profile['email'],
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .bodyText1,
                                                            ),
                                                          ))
                                                        ],
                                                      ),
                                                      _profile['prepa_name'] !=
                                                              null
                                                          ? Row(
                                                              children: <
                                                                  Widget>[
                                                                Icon(
                                                                  Icons.school,
                                                                  color: Theme.of(
                                                                          context)
                                                                      .primaryColor,
                                                                  size: 30.0,
                                                                ),
                                                                Expanded(
                                                                    child:
                                                                        Padding(
                                                                  padding: const EdgeInsets
                                                                          .only(
                                                                      left:
                                                                          10.0),
                                                                  child: Text(
                                                                    _profile[
                                                                        'prepa_name'],
                                                                    style: Theme.of(
                                                                            context)
                                                                        .textTheme
                                                                        .bodyText1,
                                                                  ),
                                                                ))
                                                              ],
                                                            )
                                                          : Row(),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(Icons.score,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColor,
                                                              size: 30.0),
                                                          Expanded(
                                                              child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 10.0),
                                                            child: Text(
                                                              "Promedio: ${_profile['prepa_score']}",
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .bodyText1,
                                                            ),
                                                          ))
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            )),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            8.0, 8.0, 8.0, 8.0),
                                        child: Card(
                                            elevation: 1.0,
                                            child: Column(
                                              children: <Widget>[
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 8.0,
                                                      vertical: 15.0),
                                                  child: Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                          flex: 1,
                                                          child: Center(
                                                            child: ClipRRect(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          50.0),
                                                              child: Image(
                                                                image: AssetImage(
                                                                    'images/' +
                                                                        _profile['badge']
                                                                            [
                                                                            'icon']),
                                                                width: 120.0,
                                                                fit:
                                                                    BoxFit.fill,
                                                              ),
                                                            ),
                                                          )),
                                                      Expanded(
                                                          flex: 2,
                                                          child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 8.0),
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              children: <
                                                                  Widget>[
                                                                Text(
                                                                  _profile[
                                                                          'badge']
                                                                      ['name'],
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          17.0,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600,
                                                                      color: Colors
                                                                          .black),
                                                                ),
                                                                Text(
                                                                  _profile[
                                                                          'badge']
                                                                      [
                                                                      'description'],
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          12.5,
                                                                      color: Colors
                                                                          .black),
                                                                )
                                                              ],
                                                            ),
                                                          )),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            )),
                                      ),
                                    ),
                                  ],
                                ),
                                _friendStatus == 0 // No FRIENDS
                                    ? noFriends(context)
                                    : _friendStatus == 1 // FRIENDS
                                        ? alreadyFriendsContent2(context)
                                        : _friendStatus ==
                                                2 // PENDING FRIEND THEY SEND IT TO ME
                                            ? pendingFriends(context)
                                            : Padding(
                                                //WAITING FOR RESPONSE
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 14.0,
                                                        vertical: 10.0),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Expanded(
                                                      child: FlatButton(
                                                          onPressed: null,
                                                          color: Color.fromRGBO(
                                                              78, 124, 176, 1),
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      10.0,
                                                                  vertical:
                                                                      15.0),
                                                          child: Text(
                                                            "Esperando Respuesta"
                                                                .toUpperCase(),
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                                fontSize: 16.0,
                                                                color: Colors
                                                                    .white),
                                                          )),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                _history != null && _history.length != 0
                                    ? Column(
                                        children: [
                                          SizedBox(
                                            height: 25.0,
                                          ),
                                          Text(
                                            "Tus Retos".toUpperCase(),
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16.0,
                                                color: Colors.white),
                                          ),
                                          SizedBox(
                                            height: 25.0,
                                          ),
                                        ],
                                      )
                                    : Row(),
                                _history != null
                                    ? Column(
                                        children: _history
                                            .map((i) => Card(
                                                  elevation: 2.0,
                                                  margin: EdgeInsets.symmetric(
                                                      horizontal: 10.0,
                                                      vertical: 6.0),
                                                  child: i['started_by_me']
                                                      ? challengeByMe(
                                                          context, i)
                                                      : challengeByFriend(
                                                          context, i),
                                                  //Text("HIKA")
                                                ))
                                            .toList())
                                    : Row(),
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  : Offstage(),
    );
  }

  challengeByFriend(BuildContext context, i) {
    int myScore = 0;
    int friendScore = 0;

    i['my_score'] is int
        ? myScore = i['my_score']
        : myScore = int.parse(i['my_score']);
    i['friend_score'] is int
        ? friendScore = i['friend_score']
        : friendScore = int.parse(i['friend_score']);

    return Container(
      width: MediaQuery.of(context).size.width * 0.92,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(4)),
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 0.0),
        leading: Container(
            padding: EdgeInsets.only(right: 12.0),
            decoration: BoxDecoration(
                border: Border(
                    right: BorderSide(width: 1.0, color: Colors.black38))),
            // 1 - You been challenged
            child: friendScore == -1
                ? FaIcon(
                    FontAwesomeIcons.clock,
                    size: 45.0,
                  )
                : friendScore == myScore
                    ? FaIcon(
                        FontAwesomeIcons.solidHandshake,
                        size: 35.5,
                      )
                    : friendScore < myScore
                        ? FaIcon(
                            FontAwesomeIcons.sadTear,
                            size: 45.0,
                          )
                        : /*Image(
            image: AssetImage('images/'+_userAvatar),
          )*/
                        FaIcon(
                            FontAwesomeIcons.trophy,
                            size: 40.0,
                          )),
        title: friendScore == -1
            ? Text(
                "Te desafiaron",
                style: TextStyle(color: Colors.black),
              )
            : friendScore == myScore
                ? Text("Empate")
                : friendScore < myScore
                    ? Text(
                        "Suerte la próxima vez",
                        style: TextStyle(color: Colors.black),
                      )
                    : Text("Felicidades"),
        subtitle: Row(
          children: <Widget>[
            Column(
              children: [
                SizedBox(
                  height: 5,
                ),
                Container(
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(78, 124, 176, 1),
                        borderRadius: BorderRadius.circular(9)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 3.0, horizontal: 12.0),
                      child: friendScore == -1
                          ? Text(
                              "Responde el desafío...",
                              style: TextStyle(color: Colors.white),
                            )
                          : friendScore == myScore
                              ? Text("Empate",
                                  style: TextStyle(color: Colors.white))
                              : friendScore < myScore
                                  ? Text(
                                      "Perdiste este desafío",
                                      style: TextStyle(color: Colors.white),
                                    )
                                  : Text(
                                      "Ganaste",
                                      style: TextStyle(color: Colors.white),
                                    ),
                    )),
              ],
            ),
          ],
        ),
        trailing:
            friendScore > myScore ? FaIcon(FontAwesomeIcons.star) : Offstage(),
        onTap: () async {
          if (friendScore == -1) {
            SharedPreferences preferences =
                await SharedPreferences.getInstance();
            int userId = preferences.getInt('userId') as int;
            String userAvatar = preferences.getString('avatar') as String;
            String name = preferences.getString('name') as String;
            String contenderName = _profile['first_name'];
            userAvatar = userAvatar + ".png";
            String contenderAvatar = "${_profile['avatar']}.png";

            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => BattleInstrucionts(
                    userId: userId,
                    userAvatar: userAvatar,
                    contenderId: widget.userId,
                    contenderAvatar: contenderAvatar,
                    response: true,
                    userName: name,
                    contenderName: contenderName,
                    challengeId: i['id'],
                  ),
                ));
          }
        },
      ),
    );
  }

  challengeByMe(BuildContext context, i) {
    int myScore = 0;
    int friendScore = 0;

    i['my_score'] is int
        ? myScore = i['my_score']
        : myScore = int.parse(i['my_score']);
    i['friend_score'] is int
        ? friendScore = i['friend_score']
        : friendScore = int.parse(i['friend_score']);

    return Container(
      width: MediaQuery.of(context).size.width * 0.92,
      decoration: BoxDecoration(color: Colors.white),
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 0.0),
        leading: Column(
          children: [
            Container(
              padding: EdgeInsets.only(right: 12.0),
              decoration: BoxDecoration(
                  border: Border(
                      right: BorderSide(width: 1.0, color: Colors.black38))),
              // its me who started the battle
              child: friendScore == -1
                  ? FaIcon(
                      FontAwesomeIcons.clock,
                      size: 45.0,
                    )
                  : friendScore == myScore
                      ? FaIcon(
                          FontAwesomeIcons.solidHandshake,
                          size: 35.0,
                        )
                      : friendScore < myScore
                          ? /*Image(
                image: AssetImage('images/${i['friend']['avatar']}.png'),
                width: 45.0,
              )*/
                          FaIcon(
                              FontAwesomeIcons.trophy,
                              size: 40.0,
                            )
                          : FaIcon(
                              FontAwesomeIcons.sadTear,
                              size: 45.0,
                            ),
            ),
          ],
        ),
        title: friendScore == -1
            ? Text(
                "Esperando la respuesta...",
                style: TextStyle(color: Colors.black),
              )
            : friendScore == myScore
                ? Text("Empate")
                : friendScore < myScore
                    ? Text("Felicidades")
                    : Text(
                        "Suerte la próxima vez",
                        style: TextStyle(color: Colors.black),
                      ),
        subtitle: Row(
          children: <Widget>[
            Column(
              children: [
                SizedBox(
                  height: 5,
                ),
                Container(
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(78, 124, 176, 1),
                        borderRadius: BorderRadius.circular(9)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 3.0, horizontal: 12.0),
                      child: friendScore == -1
                          ? Text(
                              "...",
                              style: TextStyle(color: Colors.white),
                            )
                          : friendScore == myScore
                              ? Text("Empate",
                                  style: TextStyle(color: Colors.white))
                              : friendScore < myScore
                                  ? Text(
                                      "Ganaste",
                                      style: TextStyle(color: Colors.white),
                                    )
                                  : Text("Perdiste",
                                      style: TextStyle(color: Colors.white)),
                    )),
              ],
            ),
          ],
        ),
        trailing: friendScore == -1
            ? Offstage()
            : friendScore < myScore
                ? FaIcon(FontAwesomeIcons.star)
                : Offstage(),
        onTap: () {
          /*Navigator.push(context, MaterialPageRoute(builder: (context) => FriendsProfile(
            userId: data[index]['id'],
          )));*/
        },
      ),
    );
  }

  alreadyFriendsContent2(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _isWaiting
                  ? Container()
                  : Expanded(
                      child: FlatButton(
                          onPressed: () async {
                            SharedPreferences preferences =
                                await SharedPreferences.getInstance();
                            int userId = preferences.getInt('userId') as int;
                            String name =
                                preferences.getString('name') as String;
                            String contenderName = _profile['username'];
                            String userAvatar =
                                preferences.getString('avatar') as String;
                            userAvatar = "$userAvatar.png";
                            String contenderAvatar =
                                "${_profile['avatar']}.png";

                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => BattleInstrucionts(
                                    userId: userId,
                                    userAvatar: userAvatar,
                                    contenderId: widget.userId,
                                    contenderAvatar: contenderAvatar,
                                    response: false,
                                    userName: name,
                                    contenderName: contenderName,
                                    challengeId:
                                        0, // this is zero cause the next call will tell us the right challenge id
                                  ),
                                ));
                          },
                          color: Color.fromRGBO(78, 124, 176, 1),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 15.0),
                          child: Text(
                            "Retar".toUpperCase(),
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 16.0,
                                color: Colors.white),
                          )),
                    ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: FlatButton(
                    onPressed: () async {
                      bool response = await createAlertDialogRemoveFriend(
                          context, '¿Seguro quieres borrar este amigo?');
                      if (response == true) {
                        print("Borrar amigo");
                        _removeFriend();
                      }
                    },
                    color: Colors.red,
                    padding:
                        EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
                    child: Text(
                      "Borrar Amigo".toUpperCase(),
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 16.0,
                          color: Colors.white),
                    )),
              ),
            ],
          ),
        ),
      ],
    );
  }

  pendingFriends(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: FlatButton(
                    onPressed: () async {
                      bool response = await createAlertDialogAddFriend(
                          context, '¿Seguro quieres añadir este amigo?');
                      if (response == true) {
                        print("Aceptar amigo");
                        _addFriend();
                      } else {
                        print("No Aceptar amigo");
                      }
                    },
                    color: Color.fromRGBO(78, 124, 176, 1),
                    padding:
                        EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
                    child: Text(
                      "Confirmar Solicitud".toUpperCase(),
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 16.0,
                          color: Colors.white),
                    )),
              ),
            ],
          ),
        ),
      ],
    );
  }

  noFriends(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: FlatButton(
                onPressed: () async {
                  var response = await createAlertDialogAddFriend(
                      context, '¿Seguro quieres añadir este amigo?');
                  if (response == true) {
                    print("Añadir amigo");
                    _addFriend();
                  } else {
                    print("No Añadir amigo");
                  }
                },
                color: Color.fromRGBO(78, 124, 176, 1),
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
                child: Text(
                  "Añadir Amigo".toUpperCase(),
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16.0,
                      color: Colors.white),
                )),
          ),
        ],
      ),
    );
  }

  createAlertDialogAddFriend(BuildContext context, String textContent) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Alerta"),
            content: Text(textContent),
            actions: [
              MaterialButton(
                color: Color.fromRGBO(78, 124, 176, 1),
                onPressed: () {
                  _addFriend();
                  Navigator.of(context).pop(true);
                },
                child: Text("Sí"),
              ),
              MaterialButton(
                color: Colors.red,
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: Text("No"),
              ),
            ],
          );
        });
  }

  createAlertDialogRemoveFriend(BuildContext context, String textContent) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Alerta"),
            content: Text(textContent),
            actions: [
              MaterialButton(
                color: Color.fromRGBO(78, 124, 176, 1),
                onPressed: () {
                  _removeFriend();
                  Navigator.of(context).pop(true);
                },
                child: Text("Sí"),
              ),
              MaterialButton(
                color: Colors.red,
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: Text("No"),
              ),
            ],
          );
        });
  }
}

createAlertDialog(BuildContext context, String textContent) {
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Alerta"),
          content: Text(textContent),
          actions: [
            MaterialButton(
              color: Color.fromRGBO(78, 124, 176, 1),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
              child: Text("Sí"),
            ),
            MaterialButton(
              color: Colors.red,
              onPressed: () {
                Navigator.of(context).pop(false);
              },
              child: Text("No"),
            ),
          ],
        );
      });
}

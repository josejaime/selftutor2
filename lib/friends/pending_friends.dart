import 'package:flutter/material.dart';
import '../components/myappbar.dart';
import '../friends/friend_profile.dart';
// HTTP
import 'package:http/http.dart' as http;
import 'dart:convert';
// Globals
import '../utils/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

class PendingFriends extends StatefulWidget {
  @override
  _PendingFriendsState createState() => _PendingFriendsState();
}

class _PendingFriendsState extends State<PendingFriends> {
  bool _isLoading = true;
  bool _visibleError = false;
  String _message = "";
  List data = List.empty(growable: true);

  //=======================================
  //
  // GET all the friends of the current user
  //
  //=======================================
  Future<String> _getFriends() async {
    String url = globals.baseUrl;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int userId = preferences.getInt('userId') as int;
    String token = preferences.getString('apiToken') as String;
    url = url + "user/$userId/pendingrequests";
    Uri uri = Uri.parse(url);

    await http
        .get(uri, headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        setState(() {
          _visibleError = true;
          _isLoading = false;
          _message = "Hubo un error en el servicio, intenta más tarde";
        });
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          data = result;

          data.forEach((element) {
            print("Frined Pending - ${element['id']}");
          });
          List<String> friendsPendingStrings =
              data.map((i) => i['id'].toString()).toList();
          preferences.setStringList('friendsPending', friendsPendingStrings);
          print(
              "ID de amigos pendientes desde la pantalla de amigos pendientes");
          print(friendsPendingStrings);

          if (data.length == 0) {
            print("No results, no error");
            setState(() {
              _visibleError = true;
              _message = "No tienes solicitudes pendientes por el momento";
            });
          } else {
            print("Erease error");
          }
          _isLoading = false;
          print(data.length);
          print(result);
        });
      }
    });

    return "success";
  }

  @override
  void initState() {
    _getFriends();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(
        title: Text("Solicitudes de Amistad"),
      ),
      body: _isLoading
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(
                    backgroundColor: Colors.white,
                  ),
                  SizedBox(
                    height: 25.0,
                  ),
                  Text('Estamos cargando...'),
                ],
              ),
            )
          : _visibleError
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(_message),
                    ],
                  ),
                )
              : Center(
                  child: Column(
                    children: <Widget>[
                      //===========================
                      // display the data
                      //===========================
                      Flexible(
                        child: ListView.builder(
                          itemCount: data == null ? 0 : data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                              elevation: 8.0,
                              margin: EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 6.0),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                ),
                                child: ListTile(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 20.0, vertical: 0.0),
                                  leading: Container(
                                      padding: EdgeInsets.only(right: 12.0),
                                      decoration: BoxDecoration(
                                          border: Border(
                                              right: BorderSide(
                                                  width: 1.0,
                                                  color: Colors.white24))),
                                      child: Image(
                                        image:
                                            AssetImage('images/avatar_0.png'),
                                      )),
                                  title: Text(
                                    "${data[index]['username']} | ${data[index]['first_name']}",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  subtitle: Row(
                                    children: <Widget>[
                                      Column(
                                        children: [
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Container(
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(9)),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 3.0,
                                                        horizontal: 12.0),
                                                child: Text(
                                                  "${data[index]['global_score']}",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w700),
                                                ),
                                              )),
                                        ],
                                      ),
                                    ],
                                  ),
                                  trailing: Image(
                                    image: AssetImage(
                                        'images/${data[index]['badge']['icon']}'),
                                    width: 35.0,
                                  ),
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                FriendsProfile(
                                                  userId: data[index]['id'],
                                                )));
                                  },
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
    );
  }
}

// HTTP
import 'package:flutter_html/style.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import '../friends/battle_results.dart';
import '../utils/globals.dart' as globals;
import 'package:flutter_html/flutter_html.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BattleQuiz extends StatefulWidget {
  final int userId;
  final String userAvatar;
  final String userName;
  final int contenderId;
  final String contenderAvatar;
  final String contenderName;
  final int questionId;
  final int totalQuestions;
  final score;
  final bool response;
  final int challengeId;

  const BattleQuiz({
    required this.userId,
    required this.userAvatar,
    required this.userName,
    required this.contenderId,
    required this.contenderAvatar,
    required this.contenderName,
    required this.score,
    required this.questionId,
    required this.totalQuestions,
    required this.response,
    required this.challengeId,
  });

  @override
  _BattleQuizState createState() => _BattleQuizState();
}

class _BattleQuizState extends State<BattleQuiz> {
  Map<String, dynamic> data = Map<String, dynamic>();
  List<dynamic> answers = List<dynamic>.empty(growable: true);
  List<dynamic> answers2 = List<dynamic>.empty(growable: true);
  bool _visibleLoading = true;
  bool _visibleError = false;
  String _message = "";
  bool _chosed0 = false;
  bool _chosed1 = false;
  bool _chosed2 = false;
  bool _chosed3 = false;
  bool _end = false;
  bool _correct = false;
  late Timer _timer;
  String _remainingTime = "";
  List<bool> chosed = List<bool>.empty(growable: true);
  ValueNotifier _htmlNotifier = ValueNotifier<Html>(Html(data: ''));
  //=======================================
  //
  // GET all the Questions of the quiz
  //
  //=======================================
  Future<String> _getBattleQuestions() async {
    String url = globals.baseUrl;

    if (widget.response == true) {
      url = globals.baseUrl + "challenge/reply/${widget.challengeId}";
    } else {
      url =
          globals.baseUrl + "challenge/${widget.userId}/${widget.contenderId}";
    }

    print("url : $url");
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString('apiToken') ?? '';

    Uri uri = Uri.parse(url);

    await http
        .get(uri, headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        setState(() {
          print("error doing the call");
          _visibleError = true;
          _visibleLoading = false;
          _message = "Error al conectarse con el servidos";
        });
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          print(result);
          data = result;

          answers = data['questions'];

          _htmlNotifier.value =
              Html(data: answers[widget.questionId]['question'], style: {
            "*": Style(color: Colors.black, fontSize: FontSize(14)),
            "p": Style(color: Colors.black, fontSize: FontSize(14)),
            "table": Style(
              textOverflow: TextOverflow.clip,
            ),
          });

          if (data.length == 0) {
            print("No results, no error");
            _visibleError = true;
            _visibleLoading = false;
            _message = "No hay preguntas para este módulo";
          }
          if (answers.length == 0) {
            print("No questions in the battle");
            _visibleError = true;
            _visibleLoading = false;
            _message =
                "Parece que no hay preguntas para este reto, vuelve pronto";
          } else {
            print("Erease error");
            answers2 = answers[widget.questionId]['answers'];
            chosed = List<bool>.filled(answers2.length, false);
            print("Total Quiz Questions ${answers.length}");
            print("Obteniendo index ${widget.questionId}");
            //print(data[widget.questionId - 1]['question']);
            print("Total De RESPUESTAS = ${answers2.length}");
            print("RESPUESTAS :");
            print(answers2);
          }
          _visibleLoading = false;
        });
      }
    });

    return "success";
  }

  void _startTime() {
    const oneSecond = Duration(seconds: 1);
    _timer = Timer.periodic(oneSecond, (timer) {
      _verifyTime();
    });
  }

  _verifyTime() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    //TODO: This final date must be initialized to something
    var finalDate = DateTime.parse(
        pref.getString('finalBattleDate') ?? DateTime.now().toString());

    var currentDate = DateTime.now();

    if (currentDate.isAfter(finalDate)) {
      //print('Se acabó el tiempo');
      _timer.cancel();
      if (_timer.isActive == false) {
        // ESA PANTALLA SABRÁ DESPUÉS SI ES FINAL DE EXAMEN O NO

        print('DATOS');
        print(
            "ID: ${data['id']} - CONTENDER ${widget.contenderId} - SCORE ${widget.score} - T.Questions ${widget.totalQuestions}");
        return Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => BattleEnd(
                      challengeId: data['id'],
                      contenderId: widget.contenderId,
                      score: widget.score,
                      totalQuestions: widget.totalQuestions,
                      response: widget.response,
                    )));
      }
    } else {
      /*print('Aún tienes tiempo');*/
      Duration dif = finalDate.difference(currentDate);
      //print('Diff te queda(n) ${dif.inMinutes} minuto(s)');
      setState(() {
        //remaining = dif.inMinutes.toString() + ":" + dif.inSeconds.toString();
        _remainingTime =
            dif.toString().substring(0, (dif.toString().length - 7));
      });
    }
  }

  @override
  void initState() {
    _startTime();
    _getBattleQuestions();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return await (showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Container(
                    width: double.infinity,
                    child: Text(
                      '¡Alerta!',
                      style: TextStyle(color: Colors.red),
                    )),
                content: Text(
                  '¿Realmente quires salir del desafio?',
                  style: TextStyle(fontSize: 20.0),
                ),
                actions: [
                  Column(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        child: FlatButton(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 12.5),
                          color: Color.fromRGBO(78, 124, 176, 1),
                          onPressed: () {
                            Navigator.of(context).pop(false);
                          },
                          child: Text('No'.toUpperCase(),
                              style: TextStyle(color: Colors.white)),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        child: FlatButton(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 12.5),
                          color: Color.fromRGBO(78, 124, 176, 1),
                          onPressed: () {
                            Navigator.of(context).pop(true);
                          },
                          child: Text(
                            'Sí'.toUpperCase(),
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )) ??
            false;
      },
      child: Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Material(
                  elevation: 8.0,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.15,
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(56, 90, 123, 1),
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            flex: 3,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(50.0),
                                  child: Image(
                                    image: AssetImage(
                                        'images/${widget.userAvatar}'),
                                    width: 80.0,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                Text("${widget.userName}")
                              ],
                            )),
                        Expanded(
                          flex: 4,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  //Text("${widget.userName}", style: TextStyle( fontWeight: FontWeight.w300),),
                                  Text(
                                    "VS",
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  //Text("${widget.contenderName}", style: TextStyle( fontWeight: FontWeight.w300),),
                                ],
                              ),
                              Text(
                                _remainingTime,
                                style: TextStyle(
                                    fontSize: 13.0,
                                    fontWeight: FontWeight.w300),
                              )
                            ],
                          ),
                        ),
                        Expanded(
                            flex: 3,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(50.0),
                                  child: Image(
                                    image: AssetImage(
                                        'images/${widget.contenderAvatar}'),
                                    width: 80.0,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                Text("${widget.contenderName}")
                              ],
                            )),
                      ],
                    ),
                  ),
                ),
                //TODO: Change for isLoading
                !_visibleLoading
                    ? Material(
                        elevation: 1.0,
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      color: Color.fromRGBO(78, 124, 176, 1),
                                      width: 3))),
                          width: double.infinity,
                          // child: Html(
                          //   data: answers[widget.questionId]['question'],
                          //   style: {
                          //     "p": Style(
                          //         color: Colors.black, fontSize: FontSize(18)),
                          //   },
                          // ),
                          child: _htmlNotifier.value,
                        ),
                      )
                    : Row(),
                SizedBox(
                  height: 20.0,
                ),
                _visibleLoading
                    ? Center(
                        child: Column(children: [
                        CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        Text("Estamos cargando...")
                      ]))
                    : Column(
                        //padding: EdgeInsets.all(20),
                        children: [
                          _buildQuestions(context, answers2),
                          SizedBox(height: 15.0),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.95,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: (widget.totalQuestions ==
                                            (widget.questionId + 1))
                                        ? RaisedButton(
                                            color:
                                                Color.fromRGBO(78, 124, 176, 1),
                                            padding: EdgeInsets.symmetric(
                                                vertical: 15.0,
                                                horizontal: 10.0),
                                            child: Text(
                                              "Finalizar",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            onPressed: () {
                                              nextQuestion();
                                            },
                                          )
                                        : RaisedButton(
                                            color:
                                                Color.fromRGBO(78, 124, 176, 1),
                                            padding: EdgeInsets.symmetric(
                                                vertical: 15.0,
                                                horizontal: 10.0),
                                            child: Text(
                                              "Siguiente",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            onPressed: () {
                                              nextQuestion();
                                            },
                                          ),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildQuestions(BuildContext context, List strings) {
    List<Widget> list = [];

    for (var i = 0; i < strings.length; i++) {
      list.add(
        Row(
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 5),
                child: Material(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)),
                  color: chosed[i] ? Colors.blue[100] : Colors.white,
                  child: InkWell(
                    onTap: () {
                      markQuestion2(i);
                      int isCorrect = -1;
                      strings[i]['is_correct'] is int
                          ? isCorrect = strings[i]['is_correct']
                          : isCorrect = int.parse(strings[i]['is_correct']);
                      //changeQuestion( int.parse(strings[i]['is_correct']));
                      changeQuestion(isCorrect);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 17.5, horizontal: 12.0),
                      child: Text(
                        "${strings[i]['value']}",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14.5,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
    /*Row(
      children: <Widget>[
        Expanded(
          child: Material(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15)
            ),
            color: chosed[i] ? Colors.blue[100] : Colors.white,
            child: InkWell(
              onTap: () {
                markQuestion2(i);
                changeQuestion( int.parse(strings[i]['is_correct']));
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 17.5, horizontal: 12.0),
                child: Text("$i- ${strings[i]['value']}", 
                  style: TextStyle(color: Colors.black, fontSize: 14.5, fontWeight: FontWeight.w500),
                ),
              ),
            ),
          ),
        ),
      ],
    ),*/
    return Column(children: list);
  }

  markQuestion2(int index) {
    setState(() {
      chosed = List<bool>.filled(answers2.length, false);
      chosed[index] = true;
    });
    print(chosed);
  }

  nextQuestion() {
    // add to the score
    int score = widget.score;

    if (widget.totalQuestions == (widget.questionId + 1)) {
      _end = true;
    }
    if (_correct) {
      score = score + 1;
    }

    //make sure to cancell the timer before changin questions
    _timer.cancel();

    if (!_end) {
      return Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => BattleQuiz(
                    contenderId: widget.contenderId,
                    contenderAvatar: widget.contenderAvatar,
                    userId: widget.userId,
                    userAvatar: widget.userAvatar,
                    questionId: widget.questionId +
                        1, // this one is not the ID just the array index of the question
                    score: score,
                    totalQuestions: widget.totalQuestions, // this wont change
                    challengeId: widget.challengeId,
                    response: widget.response,
                    contenderName: widget.contenderName,
                    userName: widget.userName,
                  )));
    } else {
      print('DATOS');
      print(
          "ID: ${data['id']} - CONTENDER ${widget.contenderId} - SCORE $score - T.Questions ${widget.totalQuestions}");
      return Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => BattleEnd(
                    challengeId: data['id'],
                    contenderId: widget.contenderId,
                    score: score,
                    totalQuestions: widget.totalQuestions,
                    response: widget.response,
                  )));
    }
  }

  changeQuestion(int isCorrect) {
    // here will go with the flow and
    // add to the score
    int score = widget.score;

    if (answers.length == (widget.questionId + 1)) {
      // widget.questionId is not the real ID, but the array index
      _end = true;
    }
    if (isCorrect == 1) {
      score = score + 1;
      _correct = true;
    } else {
      _correct = false;
    }

    print("---------");
    print('Question number = ${widget.questionId}');
    print('Is_correct = $isCorrect');
    print('Top Correct = $_correct');
    print('End = $_end');
    print('Score = $score');
    print("---------");
  }

  markQuestion(int index) {
    setState(() {
      switch (index) {
        case 0:
          if (_chosed0 == false) {
            _chosed0 = !_chosed0;
            _chosed1 = false;
            _chosed2 = false;
            _chosed3 = false;
          }
          break;
        case 1:
          if (_chosed1 == false) {
            _chosed0 = false;
            _chosed1 = !_chosed1;
            _chosed2 = false;
            _chosed3 = false;
          }
          break;
        case 2:
          if (_chosed2 == false) {
            _chosed0 = false;
            _chosed1 = false;
            _chosed2 = !_chosed2;
            _chosed3 = false;
          }
          break;
        case 3:
          if (_chosed3 == false) {
            _chosed0 = false;
            _chosed1 = false;
            _chosed2 = false;
            _chosed3 = !_chosed3;
          }
          break;
        default:
      }
    });
  }
}

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// HTTP
import 'package:http/http.dart' as http;
import '../friends/battle_quiz.dart';
import 'dart:async';
import 'dart:convert';
// Globals
import '../utils/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

class BattleInstrucionts extends StatefulWidget {
  final int userId;
  final String userAvatar;
  final String userName;
  final int contenderId;
  final String contenderAvatar;
  final String contenderName;
  final bool response;
  final int challengeId;

  BattleInstrucionts(
      {required this.userId,
      required this.userAvatar,
      required this.contenderId,
      required this.contenderAvatar,
      required this.response,
      required this.challengeId,
      required this.contenderName,
      required this.userName});

  @override
  _BattleInstruciontsState createState() => _BattleInstruciontsState();
}

class _BattleInstruciontsState extends State<BattleInstrucionts> {
  late Map<String, dynamic> data;
  late List intents;
  bool _visibleLoading = true;
  bool _visibleError = false;
  String _message = "";
  bool _isWaiting = false;
  bool _isNoMore = false;

  //=======================================
  // Function
  // GET all the questions and info of
  // the selected quiz
  //=======================================
  Future<String> _getChallengeInfo() async {
    String url;
    print('Response = ${widget.response}');

    if (widget.response == true) {
      url = globals.baseUrl + "challenge/reply/${widget.challengeId}";
    } else {
      url =
          globals.baseUrl + "challenge/${widget.userId}/${widget.contenderId}";
    }

    print("URL = " + url);
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString('apiToken') ?? '';

    Uri uri = Uri.parse(url);

    await http
        .get(uri, headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        setState(() {
          _visibleError = true;
          _visibleLoading = false;
          _message = "Hubo un error en el servicio, intenta más tarde";
        });
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          print("--------------------------");
          print(result);
          if (result == 0) {
            _isWaiting = true;
          } else if (result == -1) {
            _isNoMore = true;
          } else {
            data = result;
            if (data.length == 0) {
              print("No results, no error");
              _visibleError = true;
              _message =
                  "Hubo un error al consultar el examen, intenta más tarde";
            } else {
              _visibleError = false;
            }
          }

          _visibleLoading = false;
          print(result);
        });
      }
    });
    return "success";
  }

  @override
  void initState() {
    _getChallengeInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //appBar: MyCustomAppBar(),
      body: _visibleLoading
          ? Center(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(
                  backgroundColor: Colors.white,
                ),
                SizedBox(
                  height: 25.0,
                ),
                Text('Estamos cargando el reto...'),
              ],
            ))
          : _visibleError
              ? Center(
                  child: Text(_message),
                )
              : SafeArea(
                  child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Material(
                          elevation: 8.0,
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.2,
                            decoration: BoxDecoration(
                              color: Color.fromRGBO(56, 90, 123, 1),
                            ),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                    flex: 4,
                                    child: Center(
                                      child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(50.0),
                                        child: Image(
                                          image: AssetImage(
                                              'images/${widget.userAvatar}'),
                                          width: 120.0,
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    )),
                                Expanded(
                                  flex: 1,
                                  child: Text(
                                    "VS",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                                Expanded(
                                    flex: 4,
                                    child: Center(
                                      child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(50.0),
                                        child: Image(
                                          image: AssetImage(
                                              'images/${widget.contenderAvatar}'),
                                          width: 120.0,
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    )),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        _isWaiting
                            ? FaIcon(
                                FontAwesomeIcons.clock,
                                size: 75.0,
                                color: Colors.white,
                              )
                            : Image(
                                width: 128.0,
                                image: AssetImage('images/game.png'))
                        /*FaIcon(
                          FontAwesomeIcons.gamepad,
                          size: 75.0,
                          color: Colors.white,
                        )*/
                        ,
                        _isWaiting
                            ? Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "¡Espera a que tu amigo responda el reto!",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w200,
                                      fontSize: 18.0,
                                      color: Colors.white),
                                ),
                              )
                            : _isNoMore
                                ? Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "¡No hay más retos!",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 21.0,
                                          color: Colors.white),
                                    ),
                                  )
                                : Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "¡Estás por comenzar un reto!",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 21.0,
                                          color: Colors.white),
                                    ),
                                  ),
                        //Text(data['questions'][0]['question'].toString()),
                        Container(
                          decoration: BoxDecoration(
                              border: Border(
                                  top: BorderSide(color: Colors.white12),
                                  bottom: BorderSide(color: Colors.white12))),
                          padding: EdgeInsets.only(top: 25.0, bottom: 25.0),
                          margin: EdgeInsets.only(right: 10.0, left: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              _isWaiting
                                  ? Expanded(
                                      child: Column(
                                        children: <Widget>[
                                          Text(
                                            "Vuelve pronto",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: 26.0,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  : _isNoMore
                                      ? Expanded(
                                          child: Column(
                                            children: <Widget>[
                                              Text(
                                                "Vuelve más tarde",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  color: Colors.white54,
                                                  fontSize: 26.0,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      : Expanded(
                                          child: Column(
                                            children: <Widget>[
                                              Text(
                                                data['questions']
                                                    .length
                                                    .toString(),
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  fontSize: 30.0,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                              Text(
                                                'Preguntas',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  fontSize: 16.0,
                                                  color: Colors.white60,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                            ],
                          ),
                        ),
                        _isWaiting || _isNoMore
                            ? Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "¡También puedes ir y retar a más amigos!",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 17.0,
                                    color: Colors.white60,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              )
                            : Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "¡Recuerda que una vez iniciado no lo puedes detener!",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 17.0,
                                    color: Colors.white60,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                        /*Text("¡Suerte!", style: TextStyle( fontSize: 20.0, color: Colors.white54,)),*/
                        Column(
                          children: <Widget>[
                            _isWaiting || _isNoMore
                                ? Offstage()
                                : Container(
                                    width: MediaQuery.of(context).size.width,
                                    // color: Theme.of(context).primaryColor,
                                    padding: EdgeInsets.all(10.0),
                                    child: data['questions'].length > 0
                                        ? FlatButton(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 10.0,
                                                vertical: 15.0),
                                            onPressed: () {
                                              if (data['questions'].length >
                                                  0) {
                                                _startBattle();
                                              } else {
                                                print("No questions no battle");
                                              }
                                            },
                                            color:
                                                Color.fromRGBO(78, 124, 176, 1),
                                            child: Text(
                                                "Ir al reto".toUpperCase(),
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 16.0,
                                                    fontWeight:
                                                        FontWeight.w500)),
                                          )
                                        : Text(
                                            'Disculpa, no hay preguntas por ahora',
                                            textAlign: TextAlign.center,
                                          ),
                                  ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              // color: Theme.of(context).primaryColor,
                              padding: EdgeInsets.all(10.0),
                              child: FlatButton(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10.0, vertical: 15.0),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                color: Colors.white,
                                child: Text("Volver".toUpperCase(),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w500)),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30.0,
                        ),
                      ],
                    ),
                  ),
                ),
    );
  }

  _startBattle() async {
    print(
        "LETS START A battle Between user ${widget.userId} and ${widget.contenderId}");

    // Lets get the dates and times for the exams
    var initialDate = DateTime.now(); // the initial date of the battle
    var finalDate = initialDate
        .add(Duration(minutes: 10)); // the date when the date must finish

    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('initialBattleDate', initialDate.toString());
    pref.setString('finalBattleDate', finalDate.toString());

    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => BattleQuiz(
                  userId: widget.userId,
                  userAvatar: widget.userAvatar,
                  userName: widget.userName,
                  contenderId: widget.contenderId,
                  contenderAvatar: widget.contenderAvatar,
                  contenderName: widget.contenderName,
                  score: 0,
                  questionId: 0,
                  totalQuestions: data['questions'].length,
                  response: widget.response,
                  challengeId: widget.challengeId,
                )));
  }
}

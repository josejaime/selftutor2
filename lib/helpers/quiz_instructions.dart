import 'package:flutter/material.dart';
// HTTP
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

// Globals
import '../utils/globals.dart' as globals;

import '../pages/quiz.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QuizInstructions extends StatefulWidget {
  // Constructor
  final lessonId;
  final String nameLesson;
  final int subjectId;
  final String subjectPortrait;
  final String subjectName;

  QuizInstructions(
      {this.lessonId,
      required this.nameLesson,
      required this.subjectId,
      required this.subjectPortrait,
      required this.subjectName});

  @override
  _QuizInstructionsState createState() => _QuizInstructionsState();
}

class _QuizInstructionsState extends State<QuizInstructions> {
  // some variables
  List data = List.empty(growable: true);
  List intents = List.empty(growable: true);
  int _currentIntents = 0;
  bool _visibleLoading = true;
  bool _visibleError = false;
  String _errorMessage = "";
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<String> _getUserQuizIntents() async {
    String url = globals.baseUrl;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString("apiToken") as String;

    int userId = preferences.getInt('userId') as int;

    await http.get(Uri.parse(url + "user/$userId/quizzes"),
        headers: {'api-token': token}).then((http.Response response) {
      int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        setState(() {
          //_errorMessage = "Hubo un error en el servicio, intenta más tarde";
        });
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          intents = result;

          if (intents.length == 0) {
            print("No results, no error");
            setState(() {
              //_visibleError = true;
              //_visibleLoading = false;
              //_errorMessage = "Hubo un error al consultar el quiz, intenta más tarde";
            });
          } else {
            var _map = Map();

            for (int i = 0; i < intents.length; i++) {
              print(intents[i]['id']);
              if (!_map.containsKey(intents[i]['id'])) {
                _map[intents[i]['id']] = 1;
              } else {
                _map[intents[i]['id']] += 1;
              }
            }
            setState(() {
              if (_map[widget.lessonId] != null) {
                _currentIntents = _map[widget.lessonId];
              }
            });
          }
          print(result);
        });
      }
    });
    return "success";
  }

  //=======================================
  // Function
  // GET all the questions and info of
  // the selected quiz
  //=======================================
  Future<String> _getQuizData() async {
    String url = globals.baseUrl;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString("apiToken") as String;

    await http.get(Uri.parse(url + "quiz/${widget.lessonId}/questions"),
        headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        setState(() {
          _visibleError = true;
          _visibleLoading = false;
          _errorMessage = "Hubo un error en el servicio, intenta más tarde";
        });
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          int code = 0;
          try {
            code = result[0];
          } catch (_) {
            code = 1;
          }

          data = result;

          if (data.length == 0) {
            print("No results, no error");
            setState(() {
              _visibleError = true;
              _visibleLoading = false;
              _errorMessage = "Esté Quiz no está disponible intenta más tarde";
            });
          } else {
            if (code != null && code < 0) {
              setState(() {
                print("THERE WAS AN ERROR CODE $code");
                _visibleLoading = false;
                _visibleError = true;
                _errorMessage = result[2];
              });
            } else {
              print("Erease error");
              setState(() {
                _visibleError = false;
              });
            }
          }
          _visibleLoading = false;
          print(data.length);
          print(result);
        });
      }
    });
    return "success";
  }

  @override
  void initState() {
    _getUserQuizIntents();
    _getQuizData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        title: Text("Instrucciones"),
        centerTitle: true,
      ),
      body: _visibleLoading
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(
                    backgroundColor: Colors.white,
                  ),
                  SizedBox(
                    height: 25.0,
                  ),
                  Text("Cargando Quiz"),
                ],
              ),
            )
          : Center(
              child: _visibleError
                  ? Padding(
                      padding: const EdgeInsets.all(25.0),
                      child: Text(_errorMessage),
                    )
                  : Padding(
                      padding: const EdgeInsets.all(25.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          SizedBox(
                            height: 25.0,
                          ),
                          Image(
                            image: AssetImage('images/brain.png'),
                            width: 128.0,
                          ),
                          /*FaIcon(
                FontAwesomeIcons.fileSignature, size: 75.0, color: Colors.white,
              ),*/
                          /*SizedBox(
                height: 25.0,
              ),*/
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "¡Estás por comenzar el Quiz!",
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 21.0,
                                  color: Colors.white),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              widget.nameLesson,
                              style: TextStyle(
                                fontWeight: FontWeight.w200,
                                fontSize: 21.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                border: Border(
                                    top: BorderSide(color: Colors.white12),
                                    bottom: BorderSide(color: Colors.white12))),
                            padding: EdgeInsets.only(top: 25.0, bottom: 25.0),
                            margin: EdgeInsets.only(right: 10.0, left: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Expanded(
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        "15",
                                        style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 24.0,
                                          color: Colors.white,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                      Text(
                                        "Minutos",
                                        style: TextStyle(
                                          fontWeight: FontWeight.w200,
                                          fontSize: 16.0,
                                          color: Colors.white,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        "$_currentIntents/2",
                                        style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 24.0,
                                          color: Colors.white,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                      Text(
                                        "Intentos",
                                        style: TextStyle(
                                          fontWeight: FontWeight.w200,
                                          fontSize: 16.0,
                                          color: Colors.white,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          /*SizedBox(
                height: 25.0,
              ),*/
                          /*SizedBox(
                width: MediaQuery.of(context).size.width / 1.85,
                child: Text(
                  "Tienes 15 minutos y "+data.length.toString()+" preguntas para responder", textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300,),
                )
              ),*/
                          Text("¡Suerte!",
                              style: TextStyle(
                                fontSize: 20.0,
                                color: Colors.white54,
                              )),
                          Column(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width,
                                // color: Theme.of(context).primaryColor,
                                padding: EdgeInsets.all(10.0),
                                child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 0.0),
                                    child: _currentIntents < 2
                                        ? FlatButton(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 10.0,
                                                vertical: 15.0),
                                            onPressed: () {
                                              _startQuiz();
                                            },
                                            //color: Color.fromRGBO(25, 48, 80, 0.7),
                                            color:
                                                Color.fromRGBO(78, 124, 176, 1),
                                            child: Text(
                                                "Empezar Quiz".toUpperCase(),
                                                style: TextStyle(
                                                    color: Colors.white)),
                                          )
                                        : Text(
                                            "Acabaste con tus intentos para este Quiz",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16.0),
                                            textAlign: TextAlign.center,
                                          )),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                // color: Theme.of(context).primaryColor,
                                padding: EdgeInsets.all(10.0),
                                child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 0.0),
                                    child: FlatButton(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.0, vertical: 15.0),
                                      onPressed: () {
                                        /*Navigator.push(context, MaterialPageRoute(builder: (context) => Lesson(
                            id: widget.subjectId,
                            name: widget.subjectName,
                            portrait: widget.subjectPortrait,
                          )));*/
                                        //Navigator.of(context).pushNamedAndRemoveUntil('/lessons', (Route<dynamic> route) => true);
                                        Navigator.popUntil(context,
                                            ModalRoute.withName('/lessons'));
                                      },
                                      color: Colors.white,
                                      child: Text(
                                          "Regresar a lecciones".toUpperCase(),
                                          style:
                                              TextStyle(color: Colors.black)),
                                    )),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 25.0,
                          )
                        ],
                      ),
                    ),
            ),
    );
  }

  //==========================================================
  // Function
  // Start the quiz when the user is ready
  //==========================================================
  _startQuiz() async {
    var initialDate = DateTime.now();
    var finalDate = initialDate.add(Duration(minutes: 15));

    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('initialDate', initialDate.toString());
    pref.setString('finalDate', finalDate.toString());
    pref.setBool('doingQuiz', true);
    pref.remove('doingExamn');
    pref.remove('doingBattle');
    pref.setInt('score', 0);
    pref.setInt('quizId', widget.lessonId);
    pref.setInt('totalQuestions', data.length);
    pref.setBool('start', true);
    pref.setBool('end', false);
    pref.setInt('currentQuestion', 1);

    //var formattedDate = "${initialDate.day}-${initialDate.month}-${initialDate.year}--${initialDate.minute}";

    print("Lets start quiz ${widget.lessonId}");
    print('Inital date : $initialDate');
    print('Final date : $finalDate');

    /*_scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text("Hoello"),
      ),
    );*/
    return Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => QuizQuestion(
                  quizId: widget.lessonId,
                  questionId: 1,
                  start: true,
                  end: false,
                  correct: false,
                  score: 0,
                  totalQuestions: data.length,
                )));
  }
}

import 'package:flutter/material.dart';
import '../helpers/exam_results.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'module_instructions.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

// Globals
import '../utils/globals.dart' as globals;

class ExamMidTerm extends StatefulWidget {
  final int score;
  final int totalQuestions;
  final int examId;
  //NEW STUFF
  final int nextModuleId;
  final int doneModules;
  final int totalModules;
  //final int currentModule;

  ExamMidTerm(
      {required this.score,
      required this.totalQuestions,
      required this.examId,
      required this.nextModuleId,
      required this.doneModules,
      required this.totalModules});

  @override
  _ExamMidTermState createState() => _ExamMidTermState();
}

class _ExamMidTermState extends State<ExamMidTerm> {
  int _finalScore = 0;
  double _valueScore = 0.0;
  int _totalDoneModulesSoFar = 0;

  @override
  void initState() {
    // This one is just a counter
    _totalDoneModulesSoFar = widget.doneModules + 1;
    print(
        "MID TERM - INIT STATE - Modules done so far $_totalDoneModulesSoFar y widget done modules ${widget.doneModules}");
    _saveScores();
    _postScore();
    setState(() {
      _finalScore = (widget.score * 100 / widget.totalQuestions).truncate();
      _valueScore = widget.score * 100 / widget.totalQuestions / 100;
    });
    _setCurrentData();
    super.initState();
  }

  _setCurrentData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setInt('currentQuestion', -1);
    //preferences.setInt('doneModules', (widget.countCurrentModule + 1));
    preferences.setInt('doneModules', _totalDoneModulesSoFar);
    // NEXT index for the array if needed
    List<String> modulesIds =
        preferences.getStringList('modulesIds') as List<String>;
    // the next index of the array is the next module Id we should get
    int nextModuleId = widget.doneModules + 1;
    if (nextModuleId < widget.totalModules) {
      preferences.setInt('currentModule', int.parse(modulesIds[nextModuleId]));
    }
    //preferences.setInt('currentModule', (widget.countCurrentModule + 1));
  }

  //====================================================
  //
  // Lets make the score post for the user
  //
  //====================================================
  Future<bool> _postScore() async {
    String urlPostScore = globals.baseUrl + "module/log";

    SharedPreferences preferences = await SharedPreferences.getInstance();

    int userId = preferences.getInt('userId') as int;
    String token = preferences.getString('apiToken') as String;

    List<String> modulesIds =
        preferences.getStringList('modulesIds') as List<String>;

    var body = new Map<String, dynamic>();
    body['score'] = widget.score.toString();
    body['user_id'] = userId.toString();
    // Here comes the index on the array plus one,
    // so to put the score lets substract 1
    //body['exam_id'] = (widget.nextModuleId - 1).toString();
    body['exam_id'] = modulesIds[widget.doneModules];

    Uri uri = Uri.parse(urlPostScore);
    //headers: {'api-token': token}
    print("SF - MODULO A POSTEAR");
    print(body);
    http
        .post(uri, headers: {'api-token': token}, body: body)
        .then((http.Response response) async {
      final int statusCode = response.statusCode;
      print("Intento de Postear SCORE de Modulo = ${response.body}");
      print("Status del Server $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        //throw new Exception("Error while fetching data");
      }
      if (response.body != "-1") {
        print("Fue true");

        print(response.body);
        setState(() {});
      } else {
        print("Fue falso");
        setState(() {
          //_showMessage = true;
          //_message = "Intento fallido de registro";
        });
      }
    });
    return true;
  }

  _saveScores() async {
    SharedPreferences pref = await SharedPreferences.getInstance();

    List<String> _examModules =
        pref.getStringList('modulesExam') as List<String>;
    List<String> _examIds = pref.getStringList('modulesIds') as List<String>;
    List<String> _examResults =
        pref.getStringList('modulesResults') as List<String>;

    print('From shared :');
    print("Names: " + _examModules.toString());
    print("IDS: " + _examIds.toString());
    print("Results: " + _examResults.toString());

    // save the scores
    // in the index before adding 1 for the next so it stats with zero
    _examResults[widget.doneModules] = widget.score.toString();

    //TODO:: CJECK THSI ONE SDFHSDHFSD
    //TODO:: CJECK THSI ONE SDFHSDHFSD
    //TODO:: CJECK THSI ONE SDFHSDHFSD
    //TODO:: CJECK THSI ONE SDFHSDHFSD

    print('After break');
    print("Names: " + _examModules.toString());
    print("IDS: " + _examIds.toString());
    print("Results: " + _examResults.toString());
    pref.setStringList('modulesExam', _examModules);
    pref.setStringList('modulesIds', _examIds);
    pref.setStringList('modulesResults', _examResults);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return await (showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Container(
                    width: double.infinity,
                    child: Text(
                      '¡Alerta!',
                      style: TextStyle(color: Colors.red),
                    )),
                content: Container(
                  height: 125,
                  child: Column(
                    children: [
                      Text(
                        '¿Realmente quires salir del Módulo?',
                        style: TextStyle(fontSize: 20.0),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                          'Te contará como un intento y no lo podrás recuperar')
                    ],
                  ),
                ),
                actions: [
                  Column(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        child: FlatButton(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 12.5),
                          color: Color.fromRGBO(78, 124, 176, 1),
                          onPressed: () => Navigator.of(context).pop(false),
                          child: Text('Continuar'.toUpperCase(),
                              style: TextStyle(color: Colors.white)),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        child: FlatButton(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 12.5),
                          //color: Color.fromRGBO(78, 124, 176, 1),
                          color: Colors.red,
                          onPressed: () async {
                            SharedPreferences preferences =
                                await SharedPreferences.getInstance();
                            List<String> modulesIds = preferences
                                .getStringList('modulesIds') as List<String>;
                            print(modulesIds);
                            print(widget.doneModules);
                            // the next index of the array is the next module Id we should get
                            int nextModuleId;
                            if ((widget.doneModules + 1) < modulesIds.length) {
                              nextModuleId =
                                  int.parse(modulesIds[widget.doneModules + 1]);
                            } else {
                              nextModuleId = int.parse(modulesIds.last) + 1;
                            }
                            //int nextModuleId = int.parse(modulesIds[widget.doneModules + 1]);
                            await preferences.setInt(
                                'arrayPosition', widget.doneModules);

                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ExamMidTerm(
                                          examId: widget.examId,
                                          score: widget.score,
                                          totalQuestions: widget.totalQuestions,
                                          doneModules: widget.doneModules,
                                          nextModuleId: nextModuleId,
                                          totalModules: widget.totalModules,
                                        )));
                          },
                          child: Text(
                            'Salir'.toUpperCase(),
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )) ??
            false;
      },
      child: Scaffold(
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "¡Completaste el módulo!".toUpperCase(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 21.0,
                        color: Colors.white),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Revisa tu puntaje",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 18.0,
                        color: Colors.white60),
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Container(
                    width: MediaQuery.of(context).size.width * 0.75,
                    padding: EdgeInsets.all(55.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Column(
                      children: <Widget>[
                        Text("Puntos",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 27.0,
                                color: Colors.black)),
                        SizedBox(
                          height: 20.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Stack(
                              children: <Widget>[
                                SizedBox(
                                  width: 115,
                                  height: 115,
                                  child: CircularProgressIndicator(
                                    value: _valueScore,
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Color.fromRGBO(78, 124, 176, 1)),
                                    backgroundColor: Colors.black12,
                                    strokeWidth: 7.5,
                                  ),
                                ),
                                Container(
                                  width: 115,
                                  height: 115,
                                  child: Center(
                                    child: Text(_finalScore.toString(),
                                        style: TextStyle(
                                            fontSize: 37,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w600)),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )
                      ],
                    )),
                SizedBox(
                  height: 15.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.75,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border(
                                right: BorderSide(
                                    color: Colors.white38,
                                    style: BorderStyle.solid),
                              ),
                            ),
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                      _totalDoneModulesSoFar.toString() +
                                          "/${widget.totalModules}",
                                      style: TextStyle(fontSize: 26.0)),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10.0, vertical: 5.0),
                                  child: Text(
                                    "Módulos terminados",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.white70),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Text(
                                    "${widget.score}/${widget.totalQuestions}",
                                    style: TextStyle(fontSize: 26.0)),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10.0, vertical: 5.0),
                                child: Text(
                                  "Aciertos",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white70),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.symmetric(vertical: 15.0),
                    child: Text("Intenta mejorar tu score en cada oportunidad",
                        textAlign: TextAlign.center)),
                SizedBox(
                  height: 15.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.75,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: FlatButton(
                          color: Color.fromRGBO(78, 124, 176, 1),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 15.0),
                          onPressed: () async {
                            print("END");
                            //Navigator.push( context, MaterialPageRoute(builder: (context) => Home()));
                            //Navigator.popUntil(context, ModalRoute.withName('/'));
                            //Navigator.of(context).pop(MaterialPageRoute(builder: (context) => Home()));
                            _nextModule();
                          },
                          child: Text(
                              _totalDoneModulesSoFar >= widget.totalModules
                                  ? "Finalizar Examen"
                                  : "Siguiente Módulo",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white)),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _nextModule() async {
    //int totalDoneModulesSoFar = widget.countCurrentModule + 1; // next index

    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.remove('initialExamDate');
    pref.remove('finalExamDate');
    //pref.setInt('modulesRemaining', widget.totalModules);
    pref.setInt('currentQuestion', -1);
    pref.setInt('doneModules', _totalDoneModulesSoFar); // Este sobra, pero...

    print('SF - MIDTERM - Modules done so far $_totalDoneModulesSoFar');
    print('SF - MIDTERM - widget.totalModules ${widget.totalModules}');
    print(
        "Lets keep with exam ${widget.examId} con el módulo ${widget.nextModuleId} con total de ${widget.totalModules} módulos");

    if (_totalDoneModulesSoFar >= widget.totalModules) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => ExamResults()));
    } else {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ModuleInstriuctions(
                    examId: widget.examId,
                    moduleId: widget.nextModuleId,
                    modulesDone:
                        _totalDoneModulesSoFar, // the last index plus one
                    totalModules: widget.totalModules,
                  )));
    }
  }
}

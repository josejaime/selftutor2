import 'package:flutter/material.dart';

// HTTP
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

// Globals
import '../utils/globals.dart' as globals;

import 'package:shared_preferences/shared_preferences.dart';

class SelectAvatar extends StatefulWidget {
  @override
  _SelectAvatarState createState() => _SelectAvatarState();
}

class _SelectAvatarState extends State<SelectAvatar> {
  bool _visibleLoading = false;
  bool _visibleError = false;
  List data = List.empty(growable: true);
  int current = -1;
  int avatar = 0;
  int userId = 0;
  List<bool> falseValues = List.filled(15, false);
  GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  bool sending = false;

  //=========================================================
  //
  // Update the user avatar, this will be a post
  //
  //=========================================================
  Future<String> _updateUser() async {
    String url = globals.baseUrl;

    await http
        .post(Uri.parse(url + "quiz/questions"))
        .then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        setState(() {
          print("error doing the call");
          _visibleError = true;
          _visibleLoading = false;
        });
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          data = result;

          //answers = data[widget.quizId]['answers'];

          if (data.length == 0) {
            print("No results, no error");
          } else {
            print("Erease error");
          }
          _visibleLoading = false;
          print(data.length);
          print(result);
        });
      }
    });

    return "success";
  }

  @override
  void initState() {
    _getCurrentPreferences();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*appBar: MyCustomAppBar(
        title: Text(''),
      ),*/
      key: _scaffold,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context, current);
            //Navigator.pushReplacement(context, MaterialPageRoute( builder: (context) => Home()));
          },
          icon: Icon(Icons.arrow_back),
        ),
      ),
      body: userId != null && userId != 1
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Text(
                      "Selecciona tu avatar",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.w800, fontSize: 16.0),
                    ),
                  ),
                  //=======================================
                  // Error Column
                  //=======================================
                  _visibleError
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                  "¡Hubo un error en el servicio, intenta más tarde!"),
                            )
                          ],
                        )
                      : Row(),
                  //=======================================
                  // Loading Column
                  //=======================================
                  _visibleLoading
                      ? Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CircularProgressIndicator(),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "Cargando",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: 0.5),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )
                      : Row(),
                  //=======================================
                  // End Loading Column
                  //=======================================
                  Flexible(
                      child: GridView.builder(
                    // external paddings
                    padding: const EdgeInsets.all(10),
                    //itemCount: data == null ? 0: data.length,
                    itemCount: falseValues.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      mainAxisSpacing: 10.0,
                      crossAxisSpacing: 10.0,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        borderRadius: BorderRadius.circular(0.0),
                        onTap: () {
                          print("selected $index");
                          //_changeAvatar(index);
                          setAvatarCurrent(index);
                        },
                        child: Material(
                          elevation: 4.0,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(0.0),
                              //color: values[index] == false ? Color.fromRGBO(205, 205, 205, 0.75) : Color.fromRGBO(78, 124, 176, 1),
                              color: falseValues[index] == false
                                  ? Color.fromRGBO(205, 205, 205, 0.75)
                                  : Color.fromRGBO(78, 124, 176, 1),
                            ),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image(
                                  image: AssetImage('images/$index.png'),
                                ),
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  )),
                ],
              ),
            )
          : Center(
              child: Text(
                  "Debes crear una cuenta para acceder a esta funcionalidad"),
            ),
      bottomNavigationBar: userId != null && userId != 1
          ? Container(
              decoration: BoxDecoration(color: Color.fromRGBO(25, 48, 80, 0.7)),
              child: FlatButton(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(sending ? "Espera..." : "Actualizar",
                      style: TextStyle(color: Colors.white, fontSize: 16.0)),
                ),
                onPressed: () {
                  if (sending) {
                    print("NOT SENDING, BUT WAITING");
                  } else {
                    print("SENDING DATA...");
                    _changeAvatar(current);
                  }
                },
              ),
            )
          : Offstage(),
    );
  }

  void setAvatarCurrent(int index) {
    falseValues = List.filled(15, false);

    setState(() {
      current = index;
      falseValues[index] = true;
    });
  }

  void _changeAvatar(int index) async {
    print("change $index");

    falseValues = List.filled(15, false);

    setState(() {
      sending = true;
      falseValues[index] = true;
    });

    current = index;

    bool send = true;

    SharedPreferences preferences = await SharedPreferences.getInstance();

    int userId = preferences.getInt('userId') as int;
    String token = preferences.getString("apiToken") as String;
    String urlPostUser = globals.baseUrl + "user/$userId";
    print('URL $urlPostUser');
    print("Making the body request...");
    var body = new Map<String, dynamic>();
    //body['id'] = userId.toString();
    body['first_name'] = preferences.getString('name');
    body['_method'] = 'PUT';
    body['last_name'] = preferences.getString('lastName');
    body['prepa_name'] = preferences.getString('prepa');
    body['email'] = preferences.getString('email');
    body['avatar'] = current.toString();
    body['career_id'] = preferences.getInt('careerId').toString();
    body['prepa_score'] = preferences.getInt('promedio').toString();
    print("Body request...");
    print(body);

    if (userId != 1 && send) {
      await http
          .post(Uri.parse(urlPostUser),
              headers: {'api-token': token}, body: body)
          .then((http.Response response) async {
        print("Intento de Postear = ${response.body}");
        final int statusCode = response.statusCode;

        print("Status del Server $statusCode");

        if (statusCode < 200 || statusCode > 400) {
          setState(() {
            sending = false;
            ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text('Hubo un error intenta nuevamente')));
            print(
                "Lo sentimos, hubo un error al procesar tu búsqueda, intenta más tarde STATUS CODE $statusCode");
          });
          throw new Exception("Error while fetching data");
        } else {
          var result = json.decode(response.body);

          if (result.length > 0) {
            if (result[0] != -1) {
              setState(() {
                sending = false;
                //profile = result;
                preferences.setString('avatar', index.toString());
              });
              /*return Navigator.of(context).pushNamedAndRemoveUntil(
                  'home', (Route<dynamic> route) => false);*/
            } else {
              setState(() {
                sending = false;
                //TODO: Review this is working
                ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('Error: ${result[2]}')));
              });
            }
          } else {
            setState(() {
              sending = false;
              //TODO: Review this is working
              ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(content: Text('Hubo un error intenta nuevamente')));
            });
          }

          setState(() {
            sending = false;
          });
        }
      });
    }
    setState(() {
      sending = false;
    });
  }

  Future<void> _getCurrentPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String avatarString = prefs.getString('avatar') as String;

    setState(() {
      userId = prefs.getInt('userId') as int;
      current = int.parse(avatarString);
      falseValues[int.parse(avatarString)] = true;
    });
  }
}

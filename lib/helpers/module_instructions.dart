import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../components/myappbar.dart';
// HTTP
import 'package:http/http.dart' as http;
import '../pages/exam_question.dart';
import 'dart:async';
import 'dart:convert';
import '../utils/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

class ModuleInstriuctions extends StatefulWidget {
  final int examId;
  final int moduleId;
  final int totalModules;
  final int modulesDone;

  const ModuleInstriuctions(
      {required this.examId,
      required this.moduleId,
      required this.totalModules,
      required this.modulesDone});

  @override
  _ModuleInstriuctionsState createState() => _ModuleInstriuctionsState();
}

class _ModuleInstriuctionsState extends State<ModuleInstriuctions> {
  var module;
  late List<String> questions = List<String>.empty(growable: true);
  // late List<Map<String, dynamic>> questions =
  //     List<Map<String, dynamic>>.empty(growable: true);
  // bool _visibleLoading = true;
  ///
  /// Now that we don load any information from the server
  /// Just display the info
  bool _visibleLoading = true;
  bool _visibleError = false;
  String _message = "";

  // Exam questions
  List data = List.empty(growable: true);
  late List<dynamic> answers;

  @override
  void initState() {
    print("SF - Module Instructions - Getting module Info");
    _getModuleInfo();
    _setData();
    super.initState();
  }

  _setData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    // The module we are going
    preferences.setInt('currentModule', widget.moduleId);
    // Current question to 2 inner control
    preferences.setInt('currentQuestion', -2);
    List<String> modulesResults =
        preferences.getStringList('modulesResults') as List<String>;
    print('Results So Far');
    print(modulesResults);
  }

  //=======================================
  // Function
  // GET all the module info of
  // the selected exam module
  //=======================================
  Future<String> _getModuleInfo() async {
    String url = globals.baseUrl;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString('apiToken') as String;

    url += "module/${widget.moduleId}/";
    Uri uri = Uri.parse(url);

    await http.get(uri, headers: {
      'api-token': token,
    }).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("SF - GET - _getModuleInfo - status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        setState(() {
          _visibleError = true;
          _visibleLoading = false;
          _message = "Hubo un error en el servicio, intenta más tarde";
        });
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          module = result;

          if (module.length == 0) {
            print("No results, no error");
            setState(() {
              _visibleError = true;
              _visibleLoading = false;
              _message =
                  "Hubo un error al consultar el examen, intenta más tarde";
            });
          } else {
            // for (int i = 0; i < questions.length; i++) {
            //   debugPrint("Question [$i] = $questions[$i]");
            // }
            //_getExam();
            setState(() {
              _visibleLoading = false;
              _visibleError = false;
            });
          }
          print("ST: _GETMODULEINFO RESULT");
          print(result);
        });
      }
    });
    return "success";
  }

  //=======================================
  //
  // GET all the Questions of the quiz
  //
  //=======================================
  Future<String> _getExam() async {
    String url = globals.baseUrl;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString('apiToken') as String;

    await http.get(Uri.parse(url + "module/${widget.moduleId}/questions"),
        headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        setState(() {
          print("error doing the call");
          _visibleError = true;
          _visibleLoading = false;
          _message = "Error al conectarse con el servidor";
        });
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          data = result;

          // print("Exam Questions");
          // print(data);

          Map<String, dynamic> question = Map<String, dynamic>();

          /// =======================================
          /// Saving the questions on maps
          /// =======================================
          for (int i = 0; i < data.length; i++) {
            question = Map<String, dynamic>();
            question['question'] = data[i]['question'];
            question['answers'] = data[i]['answers'];
            questions.add(json.encode(question));
          }

          print("Questions Maps");
          print(questions.toString());

          preferences.setStringList('examQuestions', questions);

          if (data.length == 0) {
            print("No results, no error");
            _visibleError = true;
            _visibleLoading = false;
            _message = "No hay preguntas para este módulo";
          } else {
            print("Erease error");
          }
          _visibleLoading = false;
          print("SF - Total Questions = ${data.length}");
          //print(result);
        });
      }
    });

    return "success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(),
      body: _visibleLoading
          ? Center(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Estamos cargando el módulo...'),
                SizedBox(
                  height: 25.0,
                ),
                CircularProgressIndicator(
                  backgroundColor: Colors.white,
                )
              ],
            ))
          : _visibleError
              ? Center(
                  child: Text(_message),
                )
              : Center(
                  child: Padding(
                    padding: const EdgeInsets.all(25.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        SizedBox(
                          height: 25.0,
                        ),
                        FaIcon(
                          FontAwesomeIcons.clock,
                          size: 75.0,
                          color: Colors.white,
                        ),
                        Column(
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8.0),
                              child: Text(
                                "¡Estás por comenzar el Módulo!",
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 21.0,
                                    color: Colors.white),
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8.0),
                              child: Text(
                                "${module['module_name'].toString()}",
                                style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 23.0,
                                    color: Colors.white60),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                            width: MediaQuery.of(context).size.width * 0.50,
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              padding: EdgeInsets.all(25.0),
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 8.0),
                                    child: Text(
                                      "Tienes",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.w300,
                                          color: Colors.black),
                                    ),
                                  ),
                                  Text(
                                    "${module['limit_time']}",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 36.0,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 8.0),
                                    child: Text(
                                      "Minutos",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w300,
                                          color: Colors.black),
                                    ),
                                  ),
                                ],
                              ),
                            )),
                        Text("¡Suerte!",
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.white54,
                            )),
                        Column(
                          children: <Widget>[
                            module['questions'].length > 0
                                ? Container(
                                    width: MediaQuery.of(context).size.width,
                                    // color: Theme.of(context).primaryColor,
                                    padding: EdgeInsets.only(
                                        right: 10.0, left: 10.0),
                                    child: FlatButton(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.0, vertical: 15.0),
                                      onPressed: () {
                                        int limitTime = 15;
                                        module['limit_time'] is int
                                            ? limitTime = module['limit_time']
                                            : limitTime = int.parse(
                                                module['limit_time']
                                                    .toString());
                                        //_startExam(data[0]['id'], data.length, int.parse(data[0]['limit_time']));
                                        _startExamModule(limitTime,
                                            module['questions'].length);
                                      },
                                      //color: Color.fromRGBO(25, 48, 80, 0.7),
                                      color: Color.fromRGBO(78, 124, 176, 1),
                                      child: Text("Empezar".toUpperCase(),
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.w500)),
                                    ),
                                  )
                                : Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(
                                        right: 10.0, left: 10.0),
                                    child: Text(
                                      "No se puede acceder a este módulo justo ahora",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16.0),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                          ],
                        ),
                        SizedBox(
                          height: 25.0,
                        )
                      ],
                    ),
                  ),
                ),
    );
  }

  _startExamModule(int limitTime, int totalQuestions) async {
    print('lets start the exam ID = ${widget.examId}');
    print("with the module ID = ${widget.moduleId}");
    print('And time limit of $limitTime and $totalQuestions questions');
    print('Modules Completed So far ${widget.modulesDone}');

    // Lets get the dates and times for the exams
    var initialDate = DateTime.now();
    var finalDate = initialDate.add(Duration(minutes: limitTime));

    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('initialExamDate', initialDate.toString());
    pref.setString('finalExamDate', finalDate.toString());
    // PREFERENCES TO GET BACK TO THIS STATE
    pref.setInt('totalQuestions', totalQuestions);
    pref.setInt('score', 0);
    pref.setBool('doingExam', true);
    pref.remove('doingQuiz');
    pref.remove('doingBattle');

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ExamQuestion(
                correct: false,
                end: false,
                start: true,
                score: 0,
                examId: widget.examId,
                moduleId: widget.moduleId,
                questionId:
                    0, // from here always the first question index od the array
                totalQuestions: totalQuestions,
                totalModules: widget.totalModules,
                doneModules:
                    widget.modulesDone // the amount of modules done so far
                )));
  }
}

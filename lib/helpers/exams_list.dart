import 'package:flutter/material.dart';
// HTTP
import 'package:http/http.dart' as http;
import '../components/myappbar.dart';
import '../helpers/exam_instructions.dart';
import 'dart:async';
import 'dart:convert';

// Globals
import '../utils/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

class ExamsList extends StatefulWidget {
  @override
  _ExamsListState createState() => _ExamsListState();
}

class _ExamsListState extends State<ExamsList> {
  List data = List.empty(growable: true);
  bool _visibleLoading = true;
  bool _showMessage = false;
  String _errorMessage = "";

  @override
  void initState() {
    super.initState();
    _getLessons();
  }

  //=======================================
  //
  // GET all the lessson for the subject
  //
  //=======================================
  Future<String> _getLessons() async {
    String url = globals.baseUrl;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString('apiToken') as String;
    url += "exams";

    Uri uri = Uri.parse(url);

    await http
        .get(uri, headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        setState(() {
          _visibleLoading = false;
          _errorMessage = "Hubo un error en el servicio, intenta más tarde";
        });
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);

          if (result.length > 0) {
            print("There is something in the result");
            if (result[0] != -1) {
              print("There is no server error");
              data = result;

              if (data.length == 0) {
                print("No results, no error");
                setState(() {
                  _showMessage = true;
                  _errorMessage = "No hay exámenes por el momento";
                });
              } else {
                print("Erease error");
              }
              _visibleLoading = false;
              print(data.length);
              print(result);
            } else {
              print("Server -1");
              setState(() {
                _visibleLoading = false;
                _showMessage = true;
                _errorMessage = "${result[2]}";
              });
            }
          } else {
            setState(() {
              _visibleLoading = false;
              _showMessage = true;
              _errorMessage = "No hay exámenes por el momento";
            });
          }
        });
      }
    });

    return "success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.white,

      appBar: MyCustomAppBar(),

      body: _visibleLoading
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(
                    backgroundColor: Colors.white,
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Text('Cargando...')
                ],
              ),
            )
          : Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Material(
                    elevation: 8.0,
                    color: Colors.transparent,
                    child: Stack(
                      children: <Widget>[
                        Container(
                          //height: MediaQuery.of(context).size.height * 0.20,
                          padding: EdgeInsets.all(40.0),
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              //color: Color.fromRGBO(78, 124, 176, 1),
                              color: Color.fromRGBO(25, 48, 80, 1)),
                          child: Center(
                              //child: Text('${widget.name}', style: Theme.of(context).textTheme.headline3,),
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(height: 0.0),
                              Icon(
                                Icons.check_box,
                                color: Colors.white,
                              ),
                              Container(
                                width: 90.0,
                                child: Divider(
                                  color: Colors.white,
                                  height: 10.0,
                                ),
                              ),
                              //SizedBox(height: 00.0),
                              Text(
                                'Exámenes',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 45.0),
                              ),
                              //SizedBox(height: 30.0),
                            ],
                          )),
                        ),
                      ],
                    ),
                  ),
                  //===========================
                  // display the data
                  //===========================
                  SizedBox(
                    height: 15.0,
                  ),
                  _showMessage ? Text(_errorMessage) : Offstage(),
                  Flexible(
                    child: ListView.builder(
                      itemCount: data == null ? 0 : data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                          elevation: 8.0,
                          margin: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 6.0),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Theme.of(context)
                                  .primaryColor
                                  .withOpacity(0.99),
                              //color: Color.fromRGBO(78, 124, 176, 1),
                              //color: Colors.white70
                            ),
                            child: ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 0.0),
                              title: Text(
                                data[index]['name'],
                                style: TextStyle(color: Colors.white),
                              ),
                              subtitle: Row(
                                children: <Widget>[
                                  Expanded(
                                      flex: 1,
                                      child: Container(
                                        //tag: 'hero',
                                        child: Text(
                                          'Módulos ' +
                                              data[index]['modules'].toString(),
                                          style: TextStyle(
                                              color: Colors.white54,
                                              fontSize: 12.0),
                                        ),
                                      )),
                                  Expanded(
                                    flex: 3,
                                    child: Padding(
                                        padding: EdgeInsets.only(left: 10.0),
                                        child: Text('',
                                            style: TextStyle(
                                                color: Colors.white54))),
                                  ),
                                ],
                              ),
                              trailing: Icon(Icons.keyboard_arrow_right,
                                  color: Colors.white, size: 30.0),
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ExamInstructions(
                                              examId: data[index]['id'],
                                            )));
                                /*Navigator.push(context, MaterialPageRoute( builder: (context) => Content(
                            id: data[index]['id'],
                            name: data[index]['name'],
                            content: data[index]['content'],
                            subjectId: widget.id,
                            subjectPortrait: widget.portrait,
                            subjectName: widget.name
                          )));*/
                              },
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}

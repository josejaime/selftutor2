import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ExamResults extends StatefulWidget {
  @override
  _ExamResultsState createState() => _ExamResultsState();
}

class _ExamResultsState extends State<ExamResults> {
  List<String> _scores = List.empty(growable: true);
  List<String> _names = List.empty(growable: true);
  int _total = 0;
  double _part1 = 0.0;
  double _part2 = 0.0;
  double _exp = 0.0;
  double _intent = 0.0;
  double _requiredPoints = 0.0;
  double _prepaScore = 0.0;

  @override
  void initState() {
    _getScores();
    super.initState();
  }

  _getScores() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.remove('doingExam');
    pref.setInt('doneModules', 0);

    setState(() {
      _scores = pref.getStringList('modulesResults') as List<String>;
      _names = pref.getStringList('modulesExam') as List<String>;
      _prepaScore = double.parse(pref.getInt('promedio').toString());
      _requiredPoints = double.parse(pref.getInt('careerAdmission').toString());

      for (int i = 0; i < _scores.length; i++) {
        _total = int.parse(_scores[i]) + _total;

        switch (i) {
          case 0:
            _part1 += double.parse(_scores[i]);
            break;
          case 1:
            _part1 += double.parse(_scores[i]);
            break;
          case 2:
            _part2 += double.parse(_scores[i]);
            break;
          case 3:
            _part2 += double.parse(_scores[i]);
            break;
          default:
        }
      }

      print("Parte 1 - " + _part1.toString());
      print("Parte 2 - " + _part2.toString());
      _part1 = (600 * _part1) / 70 + 200;
      _part2 = (600 * _part2) / 55 + 200;
      _exp = (_part1 + _part2 - 400) / 12;
      //_intent = _exp + 98;
      _intent = _exp + _prepaScore;
      print("Parte 1 -" + _part1.toString());
      print("Parte 2 -" + _part2.toString());
      print('EXP - ' + _exp.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return await (showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Container(
                    width: double.infinity,
                    child: Text(
                      '¡No te olvides de revisar tus resultados!',
                      style: TextStyle(color: Colors.red),
                    )),
                content: Text(
                  'Recuerda revisar hasta el final',
                  style: TextStyle(fontSize: 20.0),
                ),
                actions: [
                  Column(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        child: FlatButton(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 12.5),
                          color: Color.fromRGBO(78, 124, 176, 1),
                          onPressed: () => Navigator.of(context).pop(false),
                          child: Text('Ok'.toUpperCase(),
                              style: TextStyle(color: Colors.white)),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )) ??
            false;
      },
      child: Scaffold(
        body: SafeArea(
          child: Center(
              child: _scores == null
                  ? CircularProgressIndicator(
                      backgroundColor: Colors.white,
                    )
                  : SafeArea(
                      child: ListView(
                        children: <Widget>[
                          SizedBox(
                            height: 20.0,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width * 0.95,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topRight: const Radius.circular(5.0),
                                        topLeft: const Radius.circular(5.0),
                                        bottomLeft: const Radius.circular(5.0),
                                        bottomRight:
                                            const Radius.circular(5.0)),
                                    color: Colors.white),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                              decoration: BoxDecoration(
                                                color: Color.fromRGBO(
                                                    78, 124, 176, 1),
                                                borderRadius: BorderRadius.only(
                                                  topRight:
                                                      const Radius.circular(
                                                          5.0),
                                                  topLeft:
                                                      const Radius.circular(
                                                          5.0),
                                                ),
                                              ),
                                              padding: EdgeInsets.all(20.0),
                                              child: Text("Módulos",
                                                  style: TextStyle(
                                                      fontSize: 18.0))),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                              decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        color: Colors.blueGrey
                                                            .withOpacity(0.2))),
                                              ),
                                              padding: EdgeInsets.all(20.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text(_names[0],
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                  Text(_scores[0],
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                ],
                                              )),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                              decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        color: Colors.blueGrey
                                                            .withOpacity(0.2))),
                                              ),
                                              padding: EdgeInsets.all(20.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text(_names[1],
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                  Text(_scores[1],
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                ],
                                              )),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                              decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        color: Colors.blueGrey
                                                            .withOpacity(0.2))),
                                              ),
                                              padding: EdgeInsets.all(20.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text(_names[2],
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                  Text(_scores[2],
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                ],
                                              )),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                              decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        color: Colors.blueGrey
                                                            .withOpacity(0.2))),
                                              ),
                                              padding: EdgeInsets.all(20.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text(_names[3],
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                  Text(_scores[3],
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                ],
                                              )),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                              decoration: BoxDecoration(
                                                color: Colors.grey
                                                    .withOpacity(0.15),
                                              ),
                                              padding: EdgeInsets.all(20.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text("Total",
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 16.0)),
                                                  Text(_total.toString(),
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 16.5)),
                                                ],
                                              )),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width * 0.95,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topRight: const Radius.circular(5.0),
                                        topLeft: const Radius.circular(5.0),
                                        bottomLeft: const Radius.circular(5.0),
                                        bottomRight:
                                            const Radius.circular(5.0)),
                                    color: Colors.white),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                              decoration: BoxDecoration(
                                                color: Color.fromRGBO(
                                                    78, 124, 176, 1),
                                                borderRadius: BorderRadius.only(
                                                  topRight:
                                                      const Radius.circular(
                                                          5.0),
                                                  topLeft:
                                                      const Radius.circular(
                                                          5.0),
                                                ),
                                              ),
                                              padding: EdgeInsets.all(20.0),
                                              child: Text("Resumen",
                                                  style: TextStyle(
                                                      fontSize: 18.0))),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                              decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        color: Colors.blueGrey
                                                            .withOpacity(0.2))),
                                              ),
                                              padding: EdgeInsets.all(20.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text(
                                                      "Promedio de preparatoria",
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                  Text("$_prepaScore",
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                ],
                                              )),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                              decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        color: Colors.blueGrey
                                                            .withOpacity(0.2))),
                                              ),
                                              padding: EdgeInsets.all(20.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text("Puntos del examen",
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                  Text(_total.toString(),
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                ],
                                              )),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                              decoration: BoxDecoration(
                                                color: Colors.grey
                                                    .withOpacity(0.15),
                                              ),
                                              padding: EdgeInsets.all(20.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text("Puntos Totales",
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 16.0)),
                                                  Text(
                                                      _intent
                                                          .toStringAsFixed(4),
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 16.5)),
                                                ],
                                              )),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width * 0.95,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topRight: const Radius.circular(5.0),
                                        topLeft: const Radius.circular(5.0),
                                        bottomLeft: const Radius.circular(5.0),
                                        bottomRight:
                                            const Radius.circular(5.0)),
                                    color: Colors.white),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                              decoration: BoxDecoration(
                                                color: Color.fromRGBO(
                                                    78, 124, 176, 1),
                                                borderRadius: BorderRadius.only(
                                                  topRight:
                                                      const Radius.circular(
                                                          5.0),
                                                  topLeft:
                                                      const Radius.circular(
                                                          5.0),
                                                ),
                                              ),
                                              padding: EdgeInsets.all(20.0),
                                              child: Text("Carrera",
                                                  style: TextStyle(
                                                      fontSize: 18.0))),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                              decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        color: Colors.blueGrey
                                                            .withOpacity(0.2))),
                                              ),
                                              padding: EdgeInsets.all(20.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text("Puntos Requeridos",
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                  Text("$_requiredPoints",
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                ],
                                              )),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                              decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        color: Colors.blueGrey
                                                            .withOpacity(0.2))),
                                              ),
                                              padding: EdgeInsets.all(20.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text("Tu puntaje",
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                  Text(
                                                      _intent
                                                          .toStringAsFixed(4),
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                ],
                                              )),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                              decoration: BoxDecoration(
                                                color: Colors.grey
                                                    .withOpacity(0.15),
                                              ),
                                              padding: EdgeInsets.all(20.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text("Diferencia",
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 16.0)),
                                                  Text(
                                                      (_intent -
                                                              _requiredPoints)
                                                          .toStringAsFixed(2),
                                                      style: TextStyle(
                                                          color: (_intent -
                                                                      _requiredPoints) >
                                                                  0
                                                              ? Colors.green
                                                              : Colors.red,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 16.5)),
                                                ],
                                              )),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Column(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width * 0.95,
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: FlatButton(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10.0, vertical: 15.0),
                                          color:
                                              Color.fromRGBO(78, 124, 176, 1),
                                          onPressed: () {
                                            Navigator.of(context)
                                                .pushNamedAndRemoveUntil(
                                                    'home',
                                                    (Route<dynamic> route) =>
                                                        false);
                                          },
                                          child: Text(
                                            "Volver al inicio".toUpperCase(),
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.w500),
                                          )),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                        ],
                      ),
                    )),
        ),
      ),
    );
  }
}

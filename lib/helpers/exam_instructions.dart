import 'package:flutter/material.dart';
import '../components/myappbar.dart';
// HTTP
import 'package:http/http.dart' as http;
import '../helpers/module_instructions.dart';
import 'dart:async';
import 'dart:convert';
// Globals
import '../utils/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

class ExamInstructions extends StatefulWidget {
  final int examId;

  ExamInstructions({
    required this.examId,
  });

  @override
  _ExamInstructionsState createState() => _ExamInstructionsState();
}

class _ExamInstructionsState extends State<ExamInstructions> {
  List data = List.empty(growable: true);
  List intents = List.empty(growable: true);
  int _currentIntents = 0;
  bool _visibleLoading = true;
  bool _visibleError = false;
  String _message = "";
  String numIntents = '0';
  bool _loaded = false;

  /// here will be saving the list of list of questions :)
  late List<String> questionsList; // = List<String>.empty(growable: true);

  Future<String> _getUserExamIntents() async {
    String url = globals.baseUrl;
    SharedPreferences preferences = await SharedPreferences.getInstance();

    int userId = preferences.getInt('userId') as int;
    String token = preferences.getString('apiToken') as String;
    url = url + "user/$userId/exams";

    Uri uri = Uri.parse(url);

    await http
        .get(uri, headers: {'api-token': token}).then((http.Response response) {
      int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        setState(() {
          //_errorMessage = "Hubo un error en el servicio, intenta más tarde";
        });
        throw new Exception("Error while fetching data");
      } else {
        setState(() {
          var result = json.decode(response.body);
          intents = result;

          print('intents');
          print('intents - commented');
          //print(intents);

          if (intents.length == 0) {
            print("No results, no error");
            setState(() {
              //_visibleError = true;
              //_visibleLoading = false;
              _message =
                  "Hubo un error al consultar el quiz, intenta más tarde";
            });
          } else {
            var _map = Map();
            //=============================================
            // SUM the full user intents for thiz quizz
            //=============================================
            for (int i = 0; i < intents.length; i++) {
              //print(intents[i]['id']);
              if (!_map.containsKey(intents[i]['id'])) {
                _map[intents[i]['id']] = 1;
              } else {
                _map[intents[i]['id']] += 1;
              }
            }
            setState(() {
              if (_map[widget.examId] != null) {
                _currentIntents = _map[widget.examId];
                print('Method current intents $_currentIntents');
              }
            });
          }
          //print("SF - Exam Result Intents");
          //print(result);
        });
      }
    });
    return "success";
  }

  //=======================================
  // Function
  // GET all the questions and info of
  // the selected quiz
  //=======================================
  Future<String> _getExamInfo() async {
    String url = globals.baseUrl;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString('apiToken') as String;

    url = url + "exam/${widget.examId}/modules";
    Uri uri = Uri.parse(url);

    await http.get(uri, headers: {'api-token': token}).then(
        (http.Response response) async {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");
      //print("status code : ${response.body}");

      if (statusCode < 200 || statusCode > 400) {
        print("error doing the call");
        setState(() {
          _visibleError = true;
          _visibleLoading = false;
          _message = "Hubo un error en el servicio, intenta más tarde";
        });
        throw new Exception("Error while fetching data");
      } else {
        //print(response.body);
        var result = json.decode(response.body);
        print("SF - exam/widget.examId/modules");
        print("SF - Exam Modules ");
        print(result);

        setState(() {
          data = result[1];
          //data = result;
          numIntents = result[0].toString();
          //numIntents = '344';
        });

        if (data.length == 0) {
          print("No results, no error");
          setState(() {
            _visibleError = true;
            _visibleLoading = false;
            _message =
                "Hubo un error al consultar el examen, intenta más tarde";
            _loaded = false;
          });
        } else {
          /// ==========================================
          /// Now we initialyze the list of strings
          /// That would contain the whole sets of questions
          /// in all modules
          /// ==========================================
          questionsList = List<String>.filled(data.length, '');

          for (int i = 0; i < data.length; i++) {
            print(data[i]['module_name'] + " ID : " + data[i]['id'].toString());

            /// ===================================
            /// get the module questions
            /// ===================================
            await _getModuleQuestions(i, data[i]['id']);
          }

          setState(() {
            _visibleLoading = false;
            _visibleError = false;
            _loaded = true;
          });
        }
      }
    });
    return "success";
  }

  //=======================================
  // Function to obtain the questions from
  // a specific module
  //=======================================
  Future<String> _getModuleQuestions(int index, int moduleId) async {
    String debugInfo = "SF - _getModuleQuestions ";
    String url = globals.baseUrl;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString('apiToken') as String;

    /// ===========================================
    /// Retrive the questions on each module
    /// ===========================================
    await http.get(Uri.parse(url + "module/$moduleId/questions"),
        headers: {'api-token': token}).then((http.Response response) {
      final int statusCode = response.statusCode;
      print("status code : $statusCode");

      if (statusCode < 200 || statusCode > 400) {
        setState(() {
          print("error doing the call");
          _visibleError = true;
          _visibleLoading = false;
          _message = "Error al conectarse con el servidor";
        });
        throw new Exception("Error while fetching data");
      } else {
        List dataModules = List.empty(growable: true);

        setState(() {
          var result = json.decode(response.body);
          dataModules = result;

          // print("Exam Questions");
          // print(data);

          Map<String, dynamic> question = Map<String, dynamic>();

          /// ===========================================
          /// To hold on each question for this module
          /// ===========================================
          List<String> questions = List<String>.empty(growable: true);

          /// ===========================================
          /// Saving the questions on maps
          /// ===========================================
          for (int i = 0; i < dataModules.length; i++) {
            question = Map<String, dynamic>();
            question['question'] = dataModules[i]['question'];
            question['answers'] = dataModules[i]['answers'];
            questions.add(json.encode(question));
          }

          print("Questions Maps");
          print(questions.toString());

          /// ===========================================
          /// Saving the questions module on the big list
          /// ===========================================
          //preferences.setStringList('examQuestions', questions);
          questionsList[index] = json.encode(questions);

          if (dataModules.length == 0) {
            debugPrint(debugInfo + "Data Modules length = 0 on cicle $index");
            _visibleError = true;
            _visibleLoading = false;
            _message = "$debugInfo No hay preguntas para este módulo";
          }
          /*else {
            debugPrint(debugInfo + "Data Modules length = 0");
            print("Erease error");
          }*/
          _visibleLoading = false;
          print(debugInfo +
              "Total Questions = ${dataModules.length} on cicle $index");
          //print(result);
        });
      }
    });

    return "success";
  }

  @override
  void initState() {
    _clearExamPreferences();
    _getUserExamIntents();
    _getExamInfo();
    super.initState();
  }

  _clearExamPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    if (preferences.containsKey('modulesExam')) {
      preferences.remove('modulesExam');
    }
    if (preferences.containsKey('modulesIds')) {
      preferences.remove('modulesIds');
    }
    if (preferences.containsKey('modulesResults')) {
      preferences.remove('modulesResults');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(),
      body: _visibleLoading
          ? Center(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Estamos cargando el examen...'),
                SizedBox(
                  height: 25.0,
                ),
                CircularProgressIndicator(
                  backgroundColor: Colors.white,
                )
              ],
            ))
          : _visibleError
              ? Center(
                  child: Text(_message),
                )
              : Center(
                  child: Padding(
                    padding: const EdgeInsets.all(25.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        SizedBox(
                          height: 25.0,
                        ),
                        Image(
                          image: AssetImage('images/brain.png'),
                          width: 128.0,
                        ),
                        /*FaIcon(
                          FontAwesomeIcons.tasks,
                          size: 75.0,
                          color: Colors.white,
                        )*/
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            "¡Estás por comenzar el Examen!",
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 21.0,
                                color: Colors.white),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                              border: Border(
                                  top: BorderSide(color: Colors.white12),
                                  bottom: BorderSide(color: Colors.white12))),
                          padding: EdgeInsets.only(top: 25.0, bottom: 25.0),
                          margin: EdgeInsets.only(right: 10.0, left: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      data.length.toString(),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 24.0,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                    Text(
                                      'Módulos',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.white60,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      //"$numIntents/2",
                                      "2",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 24.0,
                                        color: Colors.white,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    Text(
                                      "Intentos",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w200,
                                        fontSize: 16.0,
                                        color: Colors.white,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            "¡Recuerda que una vez iniciado el examen no lo puedes detener!",
                            style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 17.0,
                              color: Colors.white60,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        /*Text("¡Suerte!", style: TextStyle( fontSize: 20.0, color: Colors.white54,)),*/
                        Column(
                          children: <Widget>[
                            _currentIntents < 2
                                ? _loaded
                                    ? Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        // color: Theme.of(context).primaryColor,
                                        padding: EdgeInsets.all(10.0),
                                        child: FlatButton(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10.0, vertical: 15.0),
                                          onPressed: () {
                                            int limitTime = 15;
                                            data[0]['limit_time'] is int
                                                ? limitTime =
                                                    data[0]['limit_time']
                                                : limitTime = int.parse(data[0]
                                                        ['limit_time']
                                                    .toString());

                                            _startExam(data.length, limitTime);
                                          },
                                          //color: Color.fromRGBO(25, 48, 80, 0.7),
                                          color:
                                              Color.fromRGBO(78, 124, 176, 1),
                                          child: Text(
                                              "Ir al primer módulo"
                                                  .toUpperCase(),
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                      )
                                    : Text("...Cargando Módulos...")
                                : Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.all(10.0),
                                    child: Text(
                                      "Ya no tienes más intentos para este examen",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16.0),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              // color: Theme.of(context).primaryColor,
                              padding: EdgeInsets.all(10.0),
                              child: FlatButton(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10.0, vertical: 15.0),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                color: Colors.white,
                                child: Text("Regresar a Materias".toUpperCase(),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w500)),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 25.0,
                        )
                      ],
                    ),
                  ),
                ),
    );
  }

  _startExam(int totalModules, int limit) async {
    //List<String> modulesExam = List<String>(totalModules);
    List<String> modulesExam =
        List<String>.generate(totalModules, (index) => '');
    List<String> modulesResults = List<String>.filled(totalModules, "0");
    //List<String> modulesIds = List<String>(totalModules); // LATER
    List<String> modulesIds =
        List<String>.generate(totalModules, (index) => ''); // LATER

    List<String> modulesQuestions =
        List<String>.generate(totalModules, (index) => '');

    for (int i = 0; i < data.length; i++) {
      //print(data[i]['module_name']);
      modulesIds[i] = data[i]['id'].toString();
      modulesExam[i] = data[i]['module_name'];
      modulesQuestions[i] = questionsList[i];
    }

    int startModule = int.parse(modulesIds[0]);

    print("SAVED MODULES ID FOR THIS EXAM");
    print(modulesIds);
    print("SAVED MODULES NAMES?");
    print(modulesExam);
    print("SAVED MODULES RESULTS");
    print(modulesResults);

    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setStringList('modulesExam', modulesExam);
    pref.setStringList('modulesIds', modulesIds);
    pref.setStringList('modulesQuestions', modulesQuestions);
    pref.setStringList('modulesResults', modulesResults);
    //===============================================
    pref.setInt('currentExam', widget.examId); // 1
    pref.setInt('totalModules', data.length); // 4
    pref.setInt('currentModule', startModule); // 1
    pref.setInt('arrayPosition', 0);
    pref.setInt('doneModules', 0);
    //var formattedDate = "${initialDate.day}-${initialDate.month}-${initialDate.year}--${initialDate.minute}";

    print("Lets start examen");
    print("Examen ID = ${widget.examId} ");
    print("Con el módulo ID $startModule");
    print("con total de $totalModules módulos y tiempo de $limit minutos");

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ModuleInstriuctions(
                  examId: widget.examId,
                  moduleId: startModule,
                  modulesDone: 0, // this will be an index not an id
                  totalModules: totalModules,
                )));
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import '../helpers/exam_results.dart';
import '../helpers/module_instructions.dart';
import '../loggin/loggin.dart';
import '../loggin/register.dart';
import '../pages/exam_question.dart';
import '../pages/home.dart';
import '../pages/lesson.dart';
import '../pages/quiz.dart';
import '../pages/subjects.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'helpers/quiz_end.dart';
//import 'package:firebase_messaging/firebase_messaging.dart';

void main() => runApp(MyApp());

Map<int, Color> color = {
  50: Color.fromRGBO(25, 48, 80, .3),
  100: Color.fromRGBO(25, 48, 80, .3),
  200: Color.fromRGBO(25, 48, 80, .5),
  300: Color.fromRGBO(25, 48, 80, .5),
  400: Color.fromRGBO(25, 48, 80, .5),
  500: Color.fromRGBO(25, 48, 80, .6),
  600: Color.fromRGBO(25, 48, 80, .7),
  700: Color.fromRGBO(25, 48, 80, .8),
  800: Color.fromRGBO(25, 48, 80, .9),
  900: Color.fromRGBO(25, 48, 80, 1),
};

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final MaterialColor colorSF = MaterialColor(0xff405061, color);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Self Tutor',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: colorSF,
        backgroundColor: colorSF,
        scaffoldBackgroundColor: colorSF,
        appBarTheme: AppBarTheme(
          color: colorSF,
        ),
        textTheme: TextTheme(
            bodyText1: TextStyle(color: Colors.black),
            bodyText2: TextStyle(color: Colors.white),
            headline1:
                TextStyle(color: Colors.white, fontWeight: FontWeight.w700),
            headline2:
                TextStyle(color: Colors.black, fontWeight: FontWeight.w700),
            headline3: TextStyle(
              color: Colors.white,
            )),
        buttonColor: Color(0xff405061),
        buttonTheme: ButtonThemeData(
          buttonColor: Color(0xff405061),
        ),
      ),
      //home: MyHomePage(title: 'Self Tuttor'),
      home: Splash(),
      routes: <String, WidgetBuilder>{
        'home': (BuildContext context) => Home(),
        'subjects': (BuildContext context) => Subjects(),
        'lessons': (BuildContext context) => Lesson(),
        'splash': (BuildContext context) => Splash(),
      },
    );
  }
}

//===================================================
// Splash Screen and get the user data ¿_?
//===================================================
class Splash extends StatefulWidget {
  @override
  SplashState createState() => new SplashState();
}

class SplashState extends State<Splash> {
  bool showTestMessage = false;
  bool _getBackToQuizOr = false;
  bool _getBackToExamOr = false;
  //final _firebaseMessaging = FirebaseMessaging();

  Future checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //bool _seen = (prefs.getBool('seen') ?? false);
    bool _loged = (prefs.getBool('loged') ?? false);
    print("Var Loged before Shared = $_loged");
    //================================
    // if loged == true when
    // in the login page succes
    //================================
    if (_loged == true) {
      //=======================================
      // get the current preference user id and
      // no, it should be setted allready ¿?
      //=======================================
      print("Var Loged after Shared = $_loged");

      if (prefs.containsKey('doingQuiz') &&
          prefs.getBool('doingQuiz') == true) {
        print("Doing Quiz");
        setState(() {
          _getBackToQuizOr = true;
          showTestMessage = true;
        });
        var finalDate = DateTime.parse(prefs.getString('finalDate') as String);

        var currentDate = DateTime.now();

        if (currentDate.isAfter(finalDate)) {
          // print('Se acabó el tiempo');
          return Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => QuizEnd(
                        quizId: prefs.getInt('quizId') as int,
                        totalQuestions: prefs.getInt('totalQuestions') as int,
                        score: prefs.getInt('score') as int,
                        isEnded: false,
                      )));
        }
      } else if (prefs.containsKey('doingExam') &&
          prefs.getBool('doingExam') == true) {
        print("Doing Exam");
        print("Still Doing Exam");
        int examId = prefs.getInt('currentExam') as int;
        int module = prefs.getInt('currentModule') as int;
        int question = prefs.getInt('currentQuestion') as int;
        int totalModules = prefs.getInt('totalModules') as int;
        int myDoneModules = prefs.getInt('doneModules') as int;
        int totalQuestions = prefs.getInt('totalQuestions') as int;
        print("Exam = $examId");
        print("Module = $module");
        print("Question = $question");
        print('Total Modules = $totalModules');
        print("Done Modules = $myDoneModules");
        print('Total Questions = $totalQuestions');

        if (module == totalModules) {
          print('El final del examen');
        } else {
          print('Vas en el $module de $totalModules');
        }

        setState(() {
          _getBackToQuizOr = false;
          showTestMessage = true;
          _getBackToExamOr = true;
        });
      } else {
        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (context) => Home()));
      }
    } else {
      //await prefs.setBool('seen', true);
      await prefs.setBool('loged', false);
      // FROM 2 to 1
      await prefs.setInt('userId', 1);
      //the test token
      await prefs.setString('apiToken', 'lpd5rA0PzRExRU3sUrzCnAh5cGa98fQP');
      String savedToken = prefs.getString('apiToken') as String;
      print("Var Loged after Shared = $_loged");
      //print("Var Saved Token = $savedToken");

      setState(() {
        showTestMessage = true;
      });
      /*Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => Home()));*/
    }
  }

  @override
  void initState() {
    /* _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        setState(() {
          print("Message $message on Message");
          //_message ='$message';
        });
      },
      onLaunch: (message) async {
        setState(() {
          print("Message $message on Launch");
          //_message = '$message';
        });
      },
      onResume: (message) async {
        setState(() {
          print("Message $message on Resume");
          //_message = '$message';
        });
      },
    );
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      setState(() {
        print("The token is $token");
      });
    }); */

    Timer(Duration(milliseconds: 600), () {
      checkFirstSeen();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: showTestMessage == false
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 18.0),
                    child: Image(image: AssetImage('images/logo.png')),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.white,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 18.0),
                    child: Text(
                      'Estamos cargando...',
                      style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            )
          : _getBackToQuizOr
              ? Center(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Column(
                            children: [
                              Container(
                                height: 125,
                                width: 155,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: ExactAssetImage('images/logo.png'),
                                    fit: BoxFit.contain,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 15.0, vertical: 10.0),
                                      child: FlatButton(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10.0, vertical: 15.0),
                                          //color: Color.fromRGBO(78, 124, 176, 1),
                                          color: Color.fromRGBO(90, 56, 123, 1),
                                          onPressed: () async {
                                            SharedPreferences prefs =
                                                await SharedPreferences
                                                    .getInstance();
                                            Navigator.pushReplacement(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        QuizQuestion(
                                                          quizId: prefs
                                                              .getInt('quizId'),
                                                          questionId: prefs.getInt(
                                                              'currentQuestion'),
                                                          start: prefs.getBool(
                                                              'start') as bool,
                                                          end: prefs.getBool(
                                                              'end') as bool,
                                                          correct:
                                                              false, // Upsi
                                                          totalQuestions:
                                                              prefs.getInt(
                                                                      'totalQuestions')
                                                                  as int,
                                                          score: prefs
                                                              .getInt('score'),
                                                        )));
                                          },
                                          child: Text(
                                            'Volver al Quiz'.toUpperCase(),
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16.0),
                                          )),
                                    ),
                                  ),
                                ],
                              ),
                              Text(
                                "Estabas haciendo un quiz",
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ],
                          ),
                        ]),
                  ),
                )
              : _getBackToExamOr
                  ? Center(
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                children: [
                                  Container(
                                    height: 125,
                                    width: 155,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image:
                                            ExactAssetImage('images/logo.png'),
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 15.0,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 15.0, vertical: 10.0),
                                          child: TextButton(
                                              style: TextButton.styleFrom(
                                                primary: Colors.white,
                                                backgroundColor: Color.fromRGBO(
                                                    90, 56, 123, 1),
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 10.0,
                                                    vertical: 15.0),
                                              ),
                                              onPressed: () async {
                                                SharedPreferences prefs =
                                                    await SharedPreferences
                                                        .getInstance();
                                                int examId =
                                                    prefs.getInt('currentExam')
                                                        as int;
                                                int module = prefs.getInt(
                                                    'currentModule') as int;
                                                int question = prefs.getInt(
                                                    'currentQuestion') as int;
                                                int totalModules =
                                                    prefs.getInt('totalModules')
                                                        as int;
                                                int totalQuestions =
                                                    prefs.getInt(
                                                            'totalQuestions')
                                                        as int;
                                                int doneModules = prefs.getInt(
                                                        'doneModules') ??
                                                    0; // if is the first then zero
                                                String debugInfo =
                                                    "SF - Get Back to Exam ";
                                                debugPrint(
                                                    "///=================================///");
                                                debugPrint(debugInfo +
                                                    "Done Modules $doneModules");
                                                debugPrint(debugInfo +
                                                    "Current Module $module");
                                                debugPrint(
                                                    "///=================================///");

                                                if (doneModules ==
                                                    totalModules) {
                                                  // We are in the last module, so no more and not in the last question
                                                  // midterm directly to results
                                                  if (question ==
                                                      totalQuestions) {
                                                    //TODO: HERE you left in the last question and did not get to
                                                    //the midterm so lets call midterm in here
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                ExamResults()));
                                                  } else if (question == -1 ||
                                                      question == -2) {
                                                    //TODO: HERE you left in the last midterm page,
                                                    //the -2 should never occur
                                                    //so lets get to the Exam Results
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                ExamResults()));
                                                  } else {
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                ExamQuestion(
                                                                  correct:
                                                                      false, // falta este?
                                                                  end: false,
                                                                  start: true,
                                                                  score: prefs
                                                                      .getInt(
                                                                          'score'),
                                                                  examId:
                                                                      examId,
                                                                  moduleId:
                                                                      module,
                                                                  questionId:
                                                                      question, // from here always the first question index od the array
                                                                  totalQuestions:
                                                                      totalQuestions, // falta este
                                                                  totalModules:
                                                                      totalModules,
                                                                  doneModules:
                                                                      doneModules, // falta pensar esto
                                                                )));
                                                  }
                                                } else if (question == -2) {
                                                  // The user left in the module instructions,
                                                  // of any given module except the last one
                                                  // as the one does not start the exam until this page is dispose
                                                  // then the instructions of the module one never happens here
                                                  // And as the time starts at click then get back to the instructions of the
                                                  // given module
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              ModuleInstriuctions(
                                                                  examId:
                                                                      examId,
                                                                  moduleId:
                                                                      module,
                                                                  modulesDone:
                                                                      doneModules, //TODO: This should be the count of done modules
                                                                  totalModules:
                                                                      totalModules)));
                                                } else if (question == -1) {
                                                  // The user left in the Mid Term Page,
                                                  // of any given module except the last one
                                                  // so the score has been posted, then get to the next module start
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              ModuleInstriuctions(
                                                                  examId:
                                                                      examId,
                                                                  moduleId:
                                                                      module,

                                                                  /// ESTA ESTA MAL MUY MAL, CUANDO DEBE IR UNO VAN 3
                                                                  //modulesDone: (totalModules - doneModules), //TODO: This should be the count of done modules
                                                                  modulesDone:
                                                                      doneModules,
                                                                  totalModules:
                                                                      totalModules)));
                                                } else {
                                                  //This means still in the exam in any given question i>0 i<n
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              ExamQuestion(
                                                                correct:
                                                                    false, // falta este?
                                                                end: false,
                                                                start: true,
                                                                score: prefs
                                                                    .getInt(
                                                                        'score'),
                                                                examId: examId,
                                                                moduleId:
                                                                    module,
                                                                questionId:
                                                                    question, // from here always the first question index od the array
                                                                totalQuestions:
                                                                    totalQuestions, // falta este
                                                                totalModules:
                                                                    totalModules,
                                                                doneModules:
                                                                    doneModules, // falta pensar esto
                                                              )));
                                                }
                                              },
                                              child: Text(
                                                'Volver al Examen'
                                                    .toUpperCase(),
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 16.0),
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    "Estabas haciendo un Examen",
                                    style: TextStyle(fontSize: 18.0),
                                  ),
                                ],
                              ),
                            ]),
                      ),
                    )
                  : Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: 125,
                            width: 155,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: ExactAssetImage('images/logo.png'),
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width * 0.8,
                              padding: EdgeInsets.all(20.0),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5.0)),
                              child: Column(
                                children: <Widget>[
                                  SizedBox(
                                    height: 25.0,
                                  ),
                                  Text(
                                    "Bienvenido".toUpperCase(),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  SizedBox(
                                    height: 25.0,
                                  ),
                                  Text(
                                    "No estás logueado, aún así puedes probar la app",
                                    style: TextStyle(color: Colors.black),
                                    textAlign: TextAlign.center,
                                  ),
                                  SizedBox(
                                    height: 25.0,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 15.0, vertical: 10.0),
                                          child: FlatButton(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 10.0,
                                                  vertical: 15.0),
                                              color:
                                                  Color.fromRGBO(25, 48, 80, 1),
                                              onPressed: () {
                                                Navigator.of(context)
                                                    .pushReplacement(
                                                        MaterialPageRoute(
                                                            builder:
                                                                (context) =>
                                                                    Home()));
                                              },
                                              child: Text(
                                                '¡Vamos allá!'.toUpperCase(),
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 16.0),
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 15.0, vertical: 10.0),
                                          child: FlatButton(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 10.0,
                                                  vertical: 15.0),
                                              color: Color.fromRGBO(
                                                  78, 124, 176, 1),
                                              onPressed: () {
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            LogginUser()));
                                              },
                                              child: Text(
                                                'Loguéate'.toUpperCase(),
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 16.0),
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 15.0, vertical: 10.0),
                                          child: FlatButton(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 10.0,
                                                  vertical: 15.0),
                                              //color: Color.fromRGBO(78, 124, 176, 1),
                                              color: Color.fromRGBO(
                                                  90, 56, 123, 1),
                                              onPressed: () {
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            RegisterUser()));
                                              },
                                              child: Text(
                                                'Crea una cuenta'.toUpperCase(),
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 16.0),
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                        ],
                      ),
                    ),
    );
  }
}
//===================================================
// End Splash Screen
//===================================================
